
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7a100tfgg484-1
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set adc_clkx [ create_bd_intf_port -mode Master -vlnv bt.local:interface:diff_rtl:1.0 adc_clkx ]

  set adc_dax [ create_bd_intf_port -mode Slave -vlnv bt.local:interface:diff_rtl:1.0 adc_dax ]

  set adc_dbx [ create_bd_intf_port -mode Slave -vlnv bt.local:interface:diff_rtl:1.0 adc_dbx ]

  set adc_dcox [ create_bd_intf_port -mode Slave -vlnv bt.local:interface:diff_rtl:1.0 adc_dcox ]

  set fpga_clkx [ create_bd_intf_port -mode Slave -vlnv bt.local:interface:diff_rtl:1.0 fpga_clkx ]


  # Create ports
  set EN_AMP [ create_bd_port -dir O -from 0 -to 0 EN_AMP ]
  set clk_cs [ create_bd_port -dir O clk_cs ]
  set clk_mosi [ create_bd_port -dir O clk_mosi ]
  set clk_sclk [ create_bd_port -dir O clk_sclk ]
  set clk_sync [ create_bd_port -dir O clk_sync ]
  set sys_clock [ create_bd_port -dir I -type clk sys_clock ]

  # Create instance: ADC_GET_DATA_0, and set properties
  set ADC_GET_DATA_0 [ create_bd_cell -type ip -vlnv bt.local:user:ADC_GET_DATA:1.0 ADC_GET_DATA_0 ]

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKOUT1_JITTER {114.829} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {200.000} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {5.000} \
   CONFIG.USE_LOCKED {false} \
   CONFIG.USE_RESET {false} \
 ] $clk_wiz_0

  # Create instance: ila_0, and set properties
  set ila_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:ila:6.2 ila_0 ]
  set_property -dict [ list \
   CONFIG.C_ENABLE_ILA_AXI_MON {false} \
   CONFIG.C_MONITOR_TYPE {Native} \
   CONFIG.C_NUM_OF_PROBES {4} \
   CONFIG.C_PROBE0_WIDTH {18} \
   CONFIG.C_PROBE3_WIDTH {8} \
 ] $ila_0

  # Create instance: init_clk_0, and set properties
  set init_clk_0 [ create_bd_cell -type ip -vlnv bt.local:user:init_clk:1.0 init_clk_0 ]

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_1

  # Create interface connections
  connect_bd_intf_net -intf_net ADC_GET_DATA_0_adc_clkx [get_bd_intf_ports adc_clkx] [get_bd_intf_pins ADC_GET_DATA_0/adc_clkx]
  connect_bd_intf_net -intf_net adc_dax_0_1 [get_bd_intf_ports adc_dax] [get_bd_intf_pins ADC_GET_DATA_0/adc_dax]
  connect_bd_intf_net -intf_net adc_dbx_0_1 [get_bd_intf_ports adc_dbx] [get_bd_intf_pins ADC_GET_DATA_0/adc_dbx]
  connect_bd_intf_net -intf_net adc_dcox_0_1 [get_bd_intf_ports adc_dcox] [get_bd_intf_pins ADC_GET_DATA_0/adc_dcox]
  connect_bd_intf_net -intf_net fpga_clkx_0_1 [get_bd_intf_ports fpga_clkx] [get_bd_intf_pins ADC_GET_DATA_0/fpga_clkx]

  # Create port connections
  connect_bd_net -net ADC_GET_DATA_0_adc_data [get_bd_pins ADC_GET_DATA_0/adc_data] [get_bd_pins ila_0/probe0]
  connect_bd_net -net ADC_GET_DATA_0_data_ready [get_bd_pins ADC_GET_DATA_0/data_ready] [get_bd_pins ila_0/probe1]
  connect_bd_net -net ADC_GET_DATA_0_data_ready_clk [get_bd_pins ADC_GET_DATA_0/data_ready_clk] [get_bd_pins ila_0/probe2]
  connect_bd_net -net ADC_GET_DATA_0_out_cnt [get_bd_pins ADC_GET_DATA_0/out_cnt] [get_bd_pins ila_0/probe3]
  connect_bd_net -net clk_in1_0_1 [get_bd_ports sys_clock] [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins init_clk_0/clk]
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins ila_0/clk]
  connect_bd_net -net init_clk_0_clk_cs [get_bd_ports clk_cs] [get_bd_pins init_clk_0/clk_cs]
  connect_bd_net -net init_clk_0_clk_mosi [get_bd_ports clk_mosi] [get_bd_pins init_clk_0/clk_mosi]
  connect_bd_net -net init_clk_0_clk_sclk [get_bd_ports clk_sclk] [get_bd_pins init_clk_0/clk_sclk]
  connect_bd_net -net init_clk_0_clk_sync [get_bd_ports clk_sync] [get_bd_pins init_clk_0/clk_sync]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins init_clk_0/start_ext] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_ports EN_AMP] [get_bd_pins xlconstant_1/dout]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


