//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Tue Jun 23 12:32:44 2020
//Host        : zl-04 running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (EN_AMP,
    adc_clkx_n,
    adc_clkx_p,
    adc_dax_n,
    adc_dax_p,
    adc_dbx_n,
    adc_dbx_p,
    adc_dcox_n,
    adc_dcox_p,
    clk_cs,
    clk_mosi,
    clk_sclk,
    clk_sync,
    fpga_clkx_n,
    fpga_clkx_p,
    sys_clock);
  output [0:0]EN_AMP;
  output adc_clkx_n;
  output adc_clkx_p;
  input adc_dax_n;
  input adc_dax_p;
  input adc_dbx_n;
  input adc_dbx_p;
  input adc_dcox_n;
  input adc_dcox_p;
  output clk_cs;
  output clk_mosi;
  output clk_sclk;
  output clk_sync;
  input fpga_clkx_n;
  input fpga_clkx_p;
  input sys_clock;

  wire [0:0]EN_AMP;
  wire adc_clkx_n;
  wire adc_clkx_p;
  wire adc_dax_n;
  wire adc_dax_p;
  wire adc_dbx_n;
  wire adc_dbx_p;
  wire adc_dcox_n;
  wire adc_dcox_p;
  wire clk_cs;
  wire clk_mosi;
  wire clk_sclk;
  wire clk_sync;
  wire fpga_clkx_n;
  wire fpga_clkx_p;
  wire sys_clock;

  design_1 design_1_i
       (.EN_AMP(EN_AMP),
        .adc_clkx_n(adc_clkx_n),
        .adc_clkx_p(adc_clkx_p),
        .adc_dax_n(adc_dax_n),
        .adc_dax_p(adc_dax_p),
        .adc_dbx_n(adc_dbx_n),
        .adc_dbx_p(adc_dbx_p),
        .adc_dcox_n(adc_dcox_n),
        .adc_dcox_p(adc_dcox_p),
        .clk_cs(clk_cs),
        .clk_mosi(clk_mosi),
        .clk_sclk(clk_sclk),
        .clk_sync(clk_sync),
        .fpga_clkx_n(fpga_clkx_n),
        .fpga_clkx_p(fpga_clkx_p),
        .sys_clock(sys_clock));
endmodule
