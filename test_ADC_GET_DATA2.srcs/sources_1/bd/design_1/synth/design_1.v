//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Tue Jun 23 12:32:44 2020
//Host        : zl-04 running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=6,numReposBlks=6,numNonXlnxBlks=2,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (EN_AMP,
    adc_clkx_n,
    adc_clkx_p,
    adc_dax_n,
    adc_dax_p,
    adc_dbx_n,
    adc_dbx_p,
    adc_dcox_n,
    adc_dcox_p,
    clk_cs,
    clk_mosi,
    clk_sclk,
    clk_sync,
    fpga_clkx_n,
    fpga_clkx_p,
    sys_clock);
  output [0:0]EN_AMP;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx n" *) output adc_clkx_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx p" *) output adc_clkx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax n" *) input adc_dax_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax p" *) input adc_dax_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx n" *) input adc_dbx_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx p" *) input adc_dbx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox n" *) input adc_dcox_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox p" *) input adc_dcox_p;
  output clk_cs;
  output clk_mosi;
  output clk_sclk;
  output clk_sync;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 fpga_clkx n" *) input fpga_clkx_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 fpga_clkx p" *) input fpga_clkx_p;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.SYS_CLOCK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.SYS_CLOCK, CLK_DOMAIN design_1_clk_in1_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input sys_clock;

  wire ADC_GET_DATA_0_adc_clkx_n;
  wire ADC_GET_DATA_0_adc_clkx_p;
  wire [17:0]ADC_GET_DATA_0_adc_data;
  wire ADC_GET_DATA_0_data_ready;
  wire ADC_GET_DATA_0_data_ready_clk;
  wire [7:0]ADC_GET_DATA_0_out_cnt;
  wire adc_dax_0_1_n;
  wire adc_dax_0_1_p;
  wire adc_dbx_0_1_n;
  wire adc_dbx_0_1_p;
  wire adc_dcox_0_1_n;
  wire adc_dcox_0_1_p;
  wire clk_in1_0_1;
  wire clk_wiz_0_clk_out1;
  wire fpga_clkx_0_1_n;
  wire fpga_clkx_0_1_p;
  wire init_clk_0_clk_cs;
  wire init_clk_0_clk_mosi;
  wire init_clk_0_clk_sclk;
  wire init_clk_0_clk_sync;
  wire [0:0]xlconstant_0_dout;
  wire [0:0]xlconstant_1_dout;

  assign EN_AMP[0] = xlconstant_1_dout;
  assign adc_clkx_n = ADC_GET_DATA_0_adc_clkx_n;
  assign adc_clkx_p = ADC_GET_DATA_0_adc_clkx_p;
  assign adc_dax_0_1_n = adc_dax_n;
  assign adc_dax_0_1_p = adc_dax_p;
  assign adc_dbx_0_1_n = adc_dbx_n;
  assign adc_dbx_0_1_p = adc_dbx_p;
  assign adc_dcox_0_1_n = adc_dcox_n;
  assign adc_dcox_0_1_p = adc_dcox_p;
  assign clk_cs = init_clk_0_clk_cs;
  assign clk_in1_0_1 = sys_clock;
  assign clk_mosi = init_clk_0_clk_mosi;
  assign clk_sclk = init_clk_0_clk_sclk;
  assign clk_sync = init_clk_0_clk_sync;
  assign fpga_clkx_0_1_n = fpga_clkx_n;
  assign fpga_clkx_0_1_p = fpga_clkx_p;
  design_1_ADC_GET_DATA_0_0 ADC_GET_DATA_0
       (.adc_clkx_n(ADC_GET_DATA_0_adc_clkx_n),
        .adc_clkx_p(ADC_GET_DATA_0_adc_clkx_p),
        .adc_data(ADC_GET_DATA_0_adc_data),
        .adc_dax_n(adc_dax_0_1_n),
        .adc_dax_p(adc_dax_0_1_p),
        .adc_dbx_n(adc_dbx_0_1_n),
        .adc_dbx_p(adc_dbx_0_1_p),
        .adc_dcox_n(adc_dcox_0_1_n),
        .adc_dcox_p(adc_dcox_0_1_p),
        .data_ready(ADC_GET_DATA_0_data_ready),
        .data_ready_clk(ADC_GET_DATA_0_data_ready_clk),
        .fpga_clkx_n(fpga_clkx_0_1_n),
        .fpga_clkx_p(fpga_clkx_0_1_p),
        .out_cnt(ADC_GET_DATA_0_out_cnt));
  design_1_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(clk_in1_0_1),
        .clk_out1(clk_wiz_0_clk_out1));
  design_1_ila_0_0 ila_0
       (.clk(clk_wiz_0_clk_out1),
        .probe0(ADC_GET_DATA_0_adc_data),
        .probe1(ADC_GET_DATA_0_data_ready),
        .probe2(ADC_GET_DATA_0_data_ready_clk),
        .probe3(ADC_GET_DATA_0_out_cnt));
  design_1_init_clk_0_0 init_clk_0
       (.clk(clk_in1_0_1),
        .clk_cs(init_clk_0_clk_cs),
        .clk_mosi(init_clk_0_clk_mosi),
        .clk_sclk(init_clk_0_clk_sclk),
        .clk_sync(init_clk_0_clk_sync),
        .start_ext(xlconstant_0_dout));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
  design_1_xlconstant_1_0 xlconstant_1
       (.dout(xlconstant_1_dout));
endmodule
