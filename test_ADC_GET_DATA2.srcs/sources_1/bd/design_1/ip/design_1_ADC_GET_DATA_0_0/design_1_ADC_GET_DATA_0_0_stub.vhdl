-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun 23 12:33:24 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/project/test_ADC_GET_DATA2/test_ADC_GET_DATA2.srcs/sources_1/bd/design_1/ip/design_1_ADC_GET_DATA_0_0/design_1_ADC_GET_DATA_0_0_stub.vhdl
-- Design      : design_1_ADC_GET_DATA_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_ADC_GET_DATA_0_0 is
  Port ( 
    adc_data : out STD_LOGIC_VECTOR ( 17 downto 0 );
    data_ready : out STD_LOGIC;
    data_ready_clk : out STD_LOGIC;
    out_cnt : out STD_LOGIC_VECTOR ( 7 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    fpga_clkx_p : in STD_LOGIC;
    fpga_clkx_n : in STD_LOGIC;
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC
  );

end design_1_ADC_GET_DATA_0_0;

architecture stub of design_1_ADC_GET_DATA_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "adc_data[17:0],data_ready,data_ready_clk,out_cnt[7:0],adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,adc_dcox_p,adc_dcox_n,fpga_clkx_p,fpga_clkx_n,adc_clkx_p,adc_clkx_n";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "adc_get_data,Vivado 2019.1";
begin
end;
