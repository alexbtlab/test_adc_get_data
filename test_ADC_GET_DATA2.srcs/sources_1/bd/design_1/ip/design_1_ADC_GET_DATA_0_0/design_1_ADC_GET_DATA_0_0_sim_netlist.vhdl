-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun 23 12:33:24 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               C:/project/test_ADC_GET_DATA2/test_ADC_GET_DATA2.srcs/sources_1/bd/design_1/ip/design_1_ADC_GET_DATA_0_0/design_1_ADC_GET_DATA_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_GET_DATA_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ADC_GET_DATA_0_0_clk_wiz_1_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ADC_GET_DATA_0_0_clk_wiz_1_clk_wiz : entity is "clk_wiz_1_clk_wiz";
end design_1_ADC_GET_DATA_0_0_clk_wiz_1_clk_wiz;

architecture STRUCTURE of design_1_ADC_GET_DATA_0_0_clk_wiz_1_clk_wiz is
  signal clk_out1_clk_wiz_1 : STD_LOGIC;
  signal clk_out2_clk_wiz_1 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_1 : STD_LOGIC;
  signal clkfbout_clk_wiz_1 : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of mmcm_adv_inst : label is "PRIMITIVE";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of mmcm_adv_inst : label is "MLO";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_1,
      O => clkfbout_buf_clk_wiz_1
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_1,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_1,
      O => clk_out2
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 9.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 9.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 9,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 160.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_1,
      CLKFBOUT => clkfbout_clk_wiz_1,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => clk_in1,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_1,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ADC_GET_DATA_0_0_clk_wiz_1 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ADC_GET_DATA_0_0_clk_wiz_1 : entity is "clk_wiz_1";
end design_1_ADC_GET_DATA_0_0_clk_wiz_1;

architecture STRUCTURE of design_1_ADC_GET_DATA_0_0_clk_wiz_1 is
begin
inst: entity work.design_1_ADC_GET_DATA_0_0_clk_wiz_1_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      locked => locked
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ADC_GET_DATA_0_0_adc_get_data is
  port (
    out_cnt : out STD_LOGIC_VECTOR ( 7 downto 0 );
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC;
    adc_data : out STD_LOGIC_VECTOR ( 17 downto 0 );
    data_ready : out STD_LOGIC;
    data_ready_clk : out STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    fpga_clkx_p : in STD_LOGIC;
    fpga_clkx_n : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_ADC_GET_DATA_0_0_adc_get_data : entity is "adc_get_data";
end design_1_ADC_GET_DATA_0_0_adc_get_data;

architecture STRUCTURE of design_1_ADC_GET_DATA_0_0_adc_get_data is
  signal adc_clk : STD_LOGIC;
  signal \adc_current_clk_num_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \adc_current_clk_num_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \adc_current_clk_num_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \adc_current_clk_num_reg_reg_n_0_[0]\ : STD_LOGIC;
  signal \adc_current_clk_num_reg_reg_n_0_[1]\ : STD_LOGIC;
  signal \adc_current_clk_num_reg_reg_n_0_[2]\ : STD_LOGIC;
  signal \adc_current_strobe_num_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \adc_current_strobe_num_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal adc_da : STD_LOGIC;
  signal \^adc_data\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal adc_data_neg1 : STD_LOGIC_VECTOR ( 31 downto 3 );
  signal adc_data_neg2 : STD_LOGIC_VECTOR ( 31 downto 3 );
  signal \adc_data_neg[10]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_neg[10]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_neg[11]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_10_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_13_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_14_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_15_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_16_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_17_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_18_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_19_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_20_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_21_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_22_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_27_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_28_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_29_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_30_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_31_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_32_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_33_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_34_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_35_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_36_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_37_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_38_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_39_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_3_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_40_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_41_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_42_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_43_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_44_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_45_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_46_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_47_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_4_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_5_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_8_n_0\ : STD_LOGIC;
  signal \adc_data_neg[14]_i_9_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_11_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_12_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_13_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_14_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_15_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_16_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_17_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_18_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_19_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_20_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_21_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_22_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_23_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_24_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_25_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_26_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_27_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_28_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_29_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_34_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_35_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_36_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_37_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_38_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_39_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_3_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_40_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_41_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_42_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_43_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_44_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_45_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_46_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_47_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_4_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_8_n_0\ : STD_LOGIC;
  signal \adc_data_neg[15]_i_9_n_0\ : STD_LOGIC;
  signal \adc_data_neg[2]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_neg[2]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_neg[3]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_neg[6]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_neg[6]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_neg[7]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_11_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_11_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_11_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_11_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_12_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_12_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_12_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_12_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_23_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_23_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_23_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_23_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_24_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_24_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_24_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_24_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_25_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_25_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_25_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_25_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_6_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_6_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_6_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_6_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_7_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_7_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_7_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[14]_i_7_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_10_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_10_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_10_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_10_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_30_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_30_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_30_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_30_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_31_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_31_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_31_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_31_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_32_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_32_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_32_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_32_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_5_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_5_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_5_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_5_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_6_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_6_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_6_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_6_n_3\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_7_n_0\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_7_n_1\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_7_n_2\ : STD_LOGIC;
  signal \adc_data_neg_reg[15]_i_7_n_3\ : STD_LOGIC;
  signal adc_data_pos0 : STD_LOGIC;
  signal \adc_data_pos[0]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[0]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_pos[12]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[12]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_pos[13]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[13]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_pos[16]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[16]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_pos[16]_i_3_n_0\ : STD_LOGIC;
  signal \adc_data_pos[17]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[17]_i_3_n_0\ : STD_LOGIC;
  signal \adc_data_pos[1]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[1]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_pos[4]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[4]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_pos[5]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[5]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_pos[8]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[8]_i_2_n_0\ : STD_LOGIC;
  signal \adc_data_pos[9]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_pos[9]_i_2_n_0\ : STD_LOGIC;
  signal adc_db : STD_LOGIC;
  signal adc_reading_allowed_neg_reg_i_1_n_0 : STD_LOGIC;
  signal adc_reading_allowed_neg_reg_reg_n_0 : STD_LOGIC;
  signal \adc_reading_allowed_reg1__1\ : STD_LOGIC;
  signal adc_reading_allowed_reg_i_1_n_0 : STD_LOGIC;
  signal adc_reading_allowed_reg_reg_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal clk_adc : STD_LOGIC;
  signal clk_adc_delay : STD_LOGIC;
  signal clk_locked : STD_LOGIC;
  signal \^data_ready\ : STD_LOGIC;
  signal data_ready_clk_INST_0_i_1_n_0 : STD_LOGIC;
  signal fpga_clk : STD_LOGIC;
  signal \fpga_clk_counter[2]_i_1_n_0\ : STD_LOGIC;
  signal fpga_clk_counter_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal is_data_ready_i_1_n_0 : STD_LOGIC;
  signal is_it_first_strobe : STD_LOGIC;
  signal is_it_first_strobe_i_1_n_0 : STD_LOGIC;
  signal is_strobe_clk_active : STD_LOGIC;
  signal is_strobe_clk_active_neg : STD_LOGIC;
  signal \^out_cnt\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal tfirstclk_passed : STD_LOGIC;
  signal tfirstclk_passed_i_1_n_0 : STD_LOGIC;
  signal tfirstclk_passed_prev : STD_LOGIC;
  signal tfirstclk_passed_prev0 : STD_LOGIC;
  signal NLW_IBUFDS_adc_dco_O_UNCONNECTED : STD_LOGIC;
  signal \NLW_adc_data_neg_reg[14]_i_26_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_adc_data_neg_reg[14]_i_26_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_adc_data_neg_reg[15]_i_33_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_adc_data_neg_reg[15]_i_33_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_adc_data_neg_reg[15]_i_6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of IBUFDS_adc_da : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of IBUFDS_adc_da : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_da : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of IBUFDS_adc_da : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_db : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_db : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_db : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_db : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_dco : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_dco : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_dco : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_dco : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_fpga_clk : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_fpga_clk : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_fpga_clk : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_fpga_clk : label is "AUTO";
  attribute BOX_TYPE of OBUFDS_adc_clk : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_adc_clk : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of OBUFDS_adc_clk : label is "OBUFDS";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \adc_current_clk_num_reg[1]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \adc_current_clk_num_reg[2]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \adc_current_strobe_num_reg[1]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \adc_current_strobe_num_reg[2]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \adc_current_strobe_num_reg[3]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \adc_current_strobe_num_reg[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \adc_current_strobe_num_reg[6]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \adc_current_strobe_num_reg[7]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \adc_data_neg[14]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \adc_data_pos[0]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \adc_data_pos[12]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \adc_data_pos[13]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \adc_data_pos[13]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \adc_data_pos[16]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \adc_data_pos[16]_i_3\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \adc_data_pos[17]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \adc_data_pos[17]_i_3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \adc_data_pos[1]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \adc_data_pos[4]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \adc_data_pos[5]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \adc_data_pos[5]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \adc_data_pos[8]_i_2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \adc_data_pos[9]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \adc_data_pos[9]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of adc_reading_allowed_neg_reg_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of data_ready_clk_INST_0 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \fpga_clk_counter[0]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \fpga_clk_counter[1]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \fpga_clk_counter[2]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \fpga_clk_counter[3]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of is_data_ready_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of is_it_first_strobe_i_2 : label is "soft_lutpair5";
begin
  adc_data(17 downto 0) <= \^adc_data\(17 downto 0);
  data_ready <= \^data_ready\;
  out_cnt(7 downto 0) <= \^out_cnt\(7 downto 0);
IBUFDS_adc_da: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dax_p,
      IB => adc_dax_n,
      O => adc_da
    );
IBUFDS_adc_db: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dbx_p,
      IB => adc_dbx_n,
      O => adc_db
    );
IBUFDS_adc_dco: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dcox_p,
      IB => adc_dcox_n,
      O => NLW_IBUFDS_adc_dco_O_UNCONNECTED
    );
IBUFDS_fpga_clk: unisim.vcomponents.IBUFDS
     port map (
      I => fpga_clkx_p,
      IB => fpga_clkx_n,
      O => fpga_clk
    );
OBUFDS_adc_clk: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => adc_clk,
      O => adc_clkx_p,
      OB => adc_clkx_n
    );
OBUFDS_adc_clk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => adc_reading_allowed_reg_reg_n_0,
      I1 => clk_adc,
      I2 => adc_reading_allowed_neg_reg_reg_n_0,
      O => adc_clk
    );
\adc_current_clk_num_reg[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B5AA"
    )
        port map (
      I0 => \adc_current_clk_num_reg_reg_n_0_[0]\,
      I1 => \adc_current_clk_num_reg_reg_n_0_[1]\,
      I2 => \adc_current_clk_num_reg_reg_n_0_[2]\,
      I3 => adc_reading_allowed_reg_reg_n_0,
      O => \adc_current_clk_num_reg[0]_i_1_n_0\
    );
\adc_current_clk_num_reg[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C6CC"
    )
        port map (
      I0 => \adc_current_clk_num_reg_reg_n_0_[0]\,
      I1 => \adc_current_clk_num_reg_reg_n_0_[1]\,
      I2 => \adc_current_clk_num_reg_reg_n_0_[2]\,
      I3 => adc_reading_allowed_reg_reg_n_0,
      O => \adc_current_clk_num_reg[1]_i_1_n_0\
    );
\adc_current_clk_num_reg[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D8F0"
    )
        port map (
      I0 => \adc_current_clk_num_reg_reg_n_0_[0]\,
      I1 => \adc_current_clk_num_reg_reg_n_0_[1]\,
      I2 => \adc_current_clk_num_reg_reg_n_0_[2]\,
      I3 => adc_reading_allowed_reg_reg_n_0,
      O => \adc_current_clk_num_reg[2]_i_1_n_0\
    );
\adc_current_clk_num_reg[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_adc,
      I1 => clk_locked,
      O => tfirstclk_passed_prev0
    );
\adc_current_clk_num_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => tfirstclk_passed_prev0,
      CE => '1',
      D => \adc_current_clk_num_reg[0]_i_1_n_0\,
      Q => \adc_current_clk_num_reg_reg_n_0_[0]\,
      R => '0'
    );
\adc_current_clk_num_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tfirstclk_passed_prev0,
      CE => '1',
      D => \adc_current_clk_num_reg[1]_i_1_n_0\,
      Q => \adc_current_clk_num_reg_reg_n_0_[1]\,
      R => '0'
    );
\adc_current_clk_num_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tfirstclk_passed_prev0,
      CE => '1',
      D => \adc_current_clk_num_reg[2]_i_1_n_0\,
      Q => \adc_current_clk_num_reg_reg_n_0_[2]\,
      R => '0'
    );
\adc_current_strobe_num_reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^out_cnt\(0),
      O => \adc_current_strobe_num_reg[0]_i_1_n_0\
    );
\adc_current_strobe_num_reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \^out_cnt\(1),
      O => adc_data_neg2(3)
    );
\adc_current_strobe_num_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(2),
      O => p_0_in(2)
    );
\adc_current_strobe_num_reg[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^out_cnt\(1),
      I1 => \^out_cnt\(2),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(3),
      O => p_0_in(3)
    );
\adc_current_strobe_num_reg[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \^out_cnt\(3),
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(2),
      I3 => \^out_cnt\(1),
      I4 => \^out_cnt\(4),
      O => p_0_in(4)
    );
\adc_current_strobe_num_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \^out_cnt\(1),
      I1 => \^out_cnt\(2),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(5),
      O => p_0_in(5)
    );
\adc_current_strobe_num_reg[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \adc_current_strobe_num_reg[7]_i_2_n_0\,
      I1 => \^out_cnt\(6),
      O => p_0_in(6)
    );
\adc_current_strobe_num_reg[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \^out_cnt\(6),
      I1 => \adc_current_strobe_num_reg[7]_i_2_n_0\,
      I2 => \^out_cnt\(7),
      O => p_0_in(7)
    );
\adc_current_strobe_num_reg[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^out_cnt\(1),
      I1 => \^out_cnt\(2),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(5),
      O => \adc_current_strobe_num_reg[7]_i_2_n_0\
    );
\adc_current_strobe_num_reg_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_current_strobe_num_reg[0]_i_1_n_0\,
      Q => \^out_cnt\(0),
      S => is_it_first_strobe
    );
\adc_current_strobe_num_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => adc_data_neg2(3),
      Q => \^out_cnt\(1),
      R => is_it_first_strobe
    );
\adc_current_strobe_num_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => p_0_in(2),
      Q => \^out_cnt\(2),
      R => is_it_first_strobe
    );
\adc_current_strobe_num_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => p_0_in(3),
      Q => \^out_cnt\(3),
      R => is_it_first_strobe
    );
\adc_current_strobe_num_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => p_0_in(4),
      Q => \^out_cnt\(4),
      R => is_it_first_strobe
    );
\adc_current_strobe_num_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => p_0_in(5),
      Q => \^out_cnt\(5),
      R => is_it_first_strobe
    );
\adc_current_strobe_num_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => p_0_in(6),
      Q => \^out_cnt\(6),
      R => is_it_first_strobe
    );
\adc_current_strobe_num_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => p_0_in(7),
      Q => \^out_cnt\(7),
      R => is_it_first_strobe
    );
\adc_data_neg[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_neg[14]_i_2_n_0\,
      I2 => \adc_data_neg[10]_i_2_n_0\,
      I3 => \^adc_data\(10),
      O => \adc_data_neg[10]_i_1_n_0\
    );
\adc_data_neg[10]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDFF"
    )
        port map (
      I0 => \adc_data_neg[14]_i_4_n_0\,
      I1 => \adc_data_neg[14]_i_5_n_0\,
      I2 => \^out_cnt\(0),
      I3 => adc_data_neg1(3),
      O => \adc_data_neg[10]_i_2_n_0\
    );
\adc_data_neg[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF2000"
    )
        port map (
      I0 => adc_da,
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(1),
      I3 => \adc_data_neg[15]_i_2_n_0\,
      I4 => \^adc_data\(11),
      O => \adc_data_neg[11]_i_1_n_0\
    );
\adc_data_neg[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_neg[14]_i_2_n_0\,
      I2 => \adc_data_neg[14]_i_3_n_0\,
      I3 => \^adc_data\(14),
      O => \adc_data_neg[14]_i_1_n_0\
    );
\adc_data_neg[14]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => adc_data_neg1(26),
      I1 => adc_data_neg1(27),
      I2 => adc_data_neg1(28),
      I3 => adc_data_neg1(29),
      I4 => adc_data_neg1(31),
      I5 => adc_data_neg1(30),
      O => \adc_data_neg[14]_i_10_n_0\
    );
\adc_data_neg[14]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => adc_data_neg1(8),
      I1 => adc_data_neg1(11),
      I2 => adc_data_neg1(5),
      I3 => adc_data_neg1(9),
      O => \adc_data_neg[14]_i_13_n_0\
    );
\adc_data_neg[14]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => adc_data_neg1(12),
      I1 => adc_data_neg1(15),
      I2 => adc_data_neg1(10),
      I3 => adc_data_neg1(13),
      O => \adc_data_neg[14]_i_14_n_0\
    );
\adc_data_neg[14]_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFE"
    )
        port map (
      I0 => \^out_cnt\(3),
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(1),
      I3 => \^out_cnt\(2),
      I4 => \^out_cnt\(4),
      O => \adc_data_neg[14]_i_15_n_0\
    );
\adc_data_neg[14]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"01FE"
    )
        port map (
      I0 => \^out_cnt\(2),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(3),
      O => \adc_data_neg[14]_i_16_n_0\
    );
\adc_data_neg[14]_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \^out_cnt\(2),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(0),
      O => \adc_data_neg[14]_i_17_n_0\
    );
\adc_data_neg[14]_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \^out_cnt\(1),
      O => \adc_data_neg[14]_i_18_n_0\
    );
\adc_data_neg[14]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_19_n_0\
    );
\adc_data_neg[14]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"001F"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(2),
      I3 => data_ready_clk_INST_0_i_1_n_0,
      O => \adc_data_neg[14]_i_2_n_0\
    );
\adc_data_neg[14]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_20_n_0\
    );
\adc_data_neg[14]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_21_n_0\
    );
\adc_data_neg[14]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_22_n_0\
    );
\adc_data_neg[14]_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_27_n_0\
    );
\adc_data_neg[14]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFE"
    )
        port map (
      I0 => \^out_cnt\(5),
      I1 => \^out_cnt\(3),
      I2 => \^out_cnt\(4),
      I3 => \^out_cnt\(6),
      I4 => \adc_data_neg[15]_i_34_n_0\,
      I5 => \^out_cnt\(7),
      O => \adc_data_neg[14]_i_28_n_0\
    );
\adc_data_neg[14]_i_29\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFE"
    )
        port map (
      I0 => \^out_cnt\(4),
      I1 => \^out_cnt\(3),
      I2 => \^out_cnt\(5),
      I3 => \adc_data_neg[15]_i_34_n_0\,
      I4 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_29_n_0\
    );
\adc_data_neg[14]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7FF"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \adc_data_neg[14]_i_4_n_0\,
      I2 => \adc_data_neg[14]_i_5_n_0\,
      I3 => adc_data_neg1(3),
      O => \adc_data_neg[14]_i_3_n_0\
    );
\adc_data_neg[14]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFE"
    )
        port map (
      I0 => \^out_cnt\(3),
      I1 => \^out_cnt\(4),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(1),
      I4 => \^out_cnt\(2),
      I5 => \^out_cnt\(5),
      O => \adc_data_neg[14]_i_30_n_0\
    );
\adc_data_neg[14]_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_31_n_0\
    );
\adc_data_neg[14]_i_32\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_32_n_0\
    );
\adc_data_neg[14]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_33_n_0\
    );
\adc_data_neg[14]_i_34\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_34_n_0\
    );
\adc_data_neg[14]_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_35_n_0\
    );
\adc_data_neg[14]_i_36\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_36_n_0\
    );
\adc_data_neg[14]_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_37_n_0\
    );
\adc_data_neg[14]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_38_n_0\
    );
\adc_data_neg[14]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_39_n_0\
    );
\adc_data_neg[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => adc_data_neg1(17),
      I1 => adc_data_neg1(16),
      I2 => adc_data_neg1(4),
      I3 => \adc_data_neg[14]_i_8_n_0\,
      I4 => \adc_data_neg[14]_i_9_n_0\,
      I5 => \adc_data_neg[14]_i_10_n_0\,
      O => \adc_data_neg[14]_i_4_n_0\
    );
\adc_data_neg[14]_i_40\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_40_n_0\
    );
\adc_data_neg[14]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_41_n_0\
    );
\adc_data_neg[14]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_42_n_0\
    );
\adc_data_neg[14]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_43_n_0\
    );
\adc_data_neg[14]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_44_n_0\
    );
\adc_data_neg[14]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_45_n_0\
    );
\adc_data_neg[14]_i_46\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_46_n_0\
    );
\adc_data_neg[14]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[14]_i_47_n_0\
    );
\adc_data_neg[14]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => adc_data_neg1(7),
      I1 => adc_data_neg1(6),
      I2 => adc_data_neg1(14),
      I3 => \adc_data_neg[14]_i_13_n_0\,
      I4 => \adc_data_neg[14]_i_14_n_0\,
      O => \adc_data_neg[14]_i_5_n_0\
    );
\adc_data_neg[14]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => adc_data_neg1(21),
      I1 => adc_data_neg1(20),
      I2 => adc_data_neg1(19),
      I3 => adc_data_neg1(18),
      O => \adc_data_neg[14]_i_8_n_0\
    );
\adc_data_neg[14]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => adc_data_neg1(25),
      I1 => adc_data_neg1(24),
      I2 => adc_data_neg1(23),
      I3 => adc_data_neg1(22),
      O => \adc_data_neg[14]_i_9_n_0\
    );
\adc_data_neg[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFF0800"
    )
        port map (
      I0 => adc_da,
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(1),
      I3 => \adc_data_neg[15]_i_2_n_0\,
      I4 => \^adc_data\(15),
      O => \adc_data_neg[15]_i_1_n_0\
    );
\adc_data_neg[15]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => adc_data_neg2(21),
      I1 => adc_data_neg2(20),
      I2 => adc_data_neg2(19),
      I3 => adc_data_neg2(18),
      O => \adc_data_neg[15]_i_11_n_0\
    );
\adc_data_neg[15]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => adc_data_neg2(25),
      I1 => adc_data_neg2(24),
      I2 => adc_data_neg2(23),
      I3 => adc_data_neg2(22),
      O => \adc_data_neg[15]_i_12_n_0\
    );
\adc_data_neg[15]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => adc_data_neg2(26),
      I1 => adc_data_neg2(27),
      I2 => adc_data_neg2(28),
      I3 => adc_data_neg2(29),
      I4 => adc_data_neg2(31),
      I5 => adc_data_neg2(30),
      O => \adc_data_neg[15]_i_13_n_0\
    );
\adc_data_neg[15]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_14_n_0\
    );
\adc_data_neg[15]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFE"
    )
        port map (
      I0 => \^out_cnt\(5),
      I1 => \^out_cnt\(3),
      I2 => \^out_cnt\(4),
      I3 => \^out_cnt\(6),
      I4 => \adc_data_neg[15]_i_34_n_0\,
      I5 => \^out_cnt\(7),
      O => \adc_data_neg[15]_i_15_n_0\
    );
\adc_data_neg[15]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFE"
    )
        port map (
      I0 => \^out_cnt\(4),
      I1 => \^out_cnt\(3),
      I2 => \^out_cnt\(5),
      I3 => \adc_data_neg[15]_i_34_n_0\,
      I4 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_16_n_0\
    );
\adc_data_neg[15]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFE"
    )
        port map (
      I0 => \^out_cnt\(3),
      I1 => \^out_cnt\(4),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(1),
      I4 => \^out_cnt\(2),
      I5 => \^out_cnt\(5),
      O => \adc_data_neg[15]_i_17_n_0\
    );
\adc_data_neg[15]_i_18\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFE"
    )
        port map (
      I0 => \^out_cnt\(3),
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(1),
      I3 => \^out_cnt\(2),
      I4 => \^out_cnt\(4),
      O => \adc_data_neg[15]_i_18_n_0\
    );
\adc_data_neg[15]_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"01FE"
    )
        port map (
      I0 => \^out_cnt\(2),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(3),
      O => \adc_data_neg[15]_i_19_n_0\
    );
\adc_data_neg[15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \adc_data_neg[14]_i_2_n_0\,
      I1 => \adc_data_neg[15]_i_3_n_0\,
      I2 => \adc_data_neg[15]_i_4_n_0\,
      O => \adc_data_neg[15]_i_2_n_0\
    );
\adc_data_neg[15]_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \^out_cnt\(2),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(0),
      O => \adc_data_neg[15]_i_20_n_0\
    );
\adc_data_neg[15]_i_21\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \^out_cnt\(1),
      O => \adc_data_neg[15]_i_21_n_0\
    );
\adc_data_neg[15]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_22_n_0\
    );
\adc_data_neg[15]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_23_n_0\
    );
\adc_data_neg[15]_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_24_n_0\
    );
\adc_data_neg[15]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_25_n_0\
    );
\adc_data_neg[15]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_26_n_0\
    );
\adc_data_neg[15]_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_27_n_0\
    );
\adc_data_neg[15]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_28_n_0\
    );
\adc_data_neg[15]_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_29_n_0\
    );
\adc_data_neg[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => adc_data_neg2(7),
      I1 => adc_data_neg2(6),
      I2 => adc_data_neg2(14),
      I3 => \adc_data_neg[15]_i_8_n_0\,
      I4 => \adc_data_neg[15]_i_9_n_0\,
      O => \adc_data_neg[15]_i_3_n_0\
    );
\adc_data_neg[15]_i_34\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(2),
      O => \adc_data_neg[15]_i_34_n_0\
    );
\adc_data_neg[15]_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_35_n_0\
    );
\adc_data_neg[15]_i_36\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_36_n_0\
    );
\adc_data_neg[15]_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_37_n_0\
    );
\adc_data_neg[15]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_38_n_0\
    );
\adc_data_neg[15]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_39_n_0\
    );
\adc_data_neg[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => adc_data_neg2(17),
      I1 => adc_data_neg2(16),
      I2 => adc_data_neg2(4),
      I3 => \adc_data_neg[15]_i_11_n_0\,
      I4 => \adc_data_neg[15]_i_12_n_0\,
      I5 => \adc_data_neg[15]_i_13_n_0\,
      O => \adc_data_neg[15]_i_4_n_0\
    );
\adc_data_neg[15]_i_40\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_40_n_0\
    );
\adc_data_neg[15]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_41_n_0\
    );
\adc_data_neg[15]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_42_n_0\
    );
\adc_data_neg[15]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_43_n_0\
    );
\adc_data_neg[15]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_44_n_0\
    );
\adc_data_neg[15]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_45_n_0\
    );
\adc_data_neg[15]_i_46\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_46_n_0\
    );
\adc_data_neg[15]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \adc_data_neg[15]_i_34_n_0\,
      I1 => \^out_cnt\(7),
      I2 => \^out_cnt\(5),
      I3 => \^out_cnt\(3),
      I4 => \^out_cnt\(4),
      I5 => \^out_cnt\(6),
      O => \adc_data_neg[15]_i_47_n_0\
    );
\adc_data_neg[15]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => adc_data_neg2(8),
      I1 => adc_data_neg2(11),
      I2 => adc_data_neg2(5),
      I3 => adc_data_neg2(9),
      O => \adc_data_neg[15]_i_8_n_0\
    );
\adc_data_neg[15]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => adc_data_neg2(12),
      I1 => adc_data_neg2(15),
      I2 => adc_data_neg2(10),
      I3 => adc_data_neg2(13),
      O => \adc_data_neg[15]_i_9_n_0\
    );
\adc_data_neg[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_neg[14]_i_2_n_0\,
      I2 => \adc_data_neg[2]_i_2_n_0\,
      I3 => \^adc_data\(2),
      O => \adc_data_neg[2]_i_1_n_0\
    );
\adc_data_neg[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \adc_data_neg[14]_i_4_n_0\,
      I1 => \adc_data_neg[14]_i_5_n_0\,
      I2 => \^out_cnt\(0),
      I3 => adc_data_neg1(3),
      O => \adc_data_neg[2]_i_2_n_0\
    );
\adc_data_neg[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0200"
    )
        port map (
      I0 => adc_da,
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(0),
      I3 => \adc_data_neg[15]_i_2_n_0\,
      I4 => \^adc_data\(3),
      O => \adc_data_neg[3]_i_1_n_0\
    );
\adc_data_neg[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_neg[14]_i_2_n_0\,
      I2 => \adc_data_neg[6]_i_2_n_0\,
      I3 => \^adc_data\(6),
      O => \adc_data_neg[6]_i_1_n_0\
    );
\adc_data_neg[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF7"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \adc_data_neg[14]_i_4_n_0\,
      I2 => \adc_data_neg[14]_i_5_n_0\,
      I3 => adc_data_neg1(3),
      O => \adc_data_neg[6]_i_2_n_0\
    );
\adc_data_neg[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFF8000"
    )
        port map (
      I0 => adc_da,
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(1),
      I3 => \adc_data_neg[15]_i_2_n_0\,
      I4 => \^adc_data\(7),
      O => \adc_data_neg[7]_i_1_n_0\
    );
\adc_data_neg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_neg[10]_i_1_n_0\,
      Q => \^adc_data\(10),
      R => '0'
    );
\adc_data_neg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_neg[11]_i_1_n_0\,
      Q => \^adc_data\(11),
      R => '0'
    );
\adc_data_neg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_neg[14]_i_1_n_0\,
      Q => \^adc_data\(14),
      R => '0'
    );
\adc_data_neg_reg[14]_i_11\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[14]_i_6_n_0\,
      CO(3) => \adc_data_neg_reg[14]_i_11_n_0\,
      CO(2) => \adc_data_neg_reg[14]_i_11_n_1\,
      CO(1) => \adc_data_neg_reg[14]_i_11_n_2\,
      CO(0) => \adc_data_neg_reg[14]_i_11_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg1(10 downto 7),
      S(3) => \adc_data_neg[14]_i_27_n_0\,
      S(2) => \adc_data_neg[14]_i_28_n_0\,
      S(1) => \adc_data_neg[14]_i_29_n_0\,
      S(0) => \adc_data_neg[14]_i_30_n_0\
    );
\adc_data_neg_reg[14]_i_12\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[14]_i_11_n_0\,
      CO(3) => \adc_data_neg_reg[14]_i_12_n_0\,
      CO(2) => \adc_data_neg_reg[14]_i_12_n_1\,
      CO(1) => \adc_data_neg_reg[14]_i_12_n_2\,
      CO(0) => \adc_data_neg_reg[14]_i_12_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg1(14 downto 11),
      S(3) => \adc_data_neg[14]_i_31_n_0\,
      S(2) => \adc_data_neg[14]_i_32_n_0\,
      S(1) => \adc_data_neg[14]_i_33_n_0\,
      S(0) => \adc_data_neg[14]_i_34_n_0\
    );
\adc_data_neg_reg[14]_i_23\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[14]_i_7_n_0\,
      CO(3) => \adc_data_neg_reg[14]_i_23_n_0\,
      CO(2) => \adc_data_neg_reg[14]_i_23_n_1\,
      CO(1) => \adc_data_neg_reg[14]_i_23_n_2\,
      CO(0) => \adc_data_neg_reg[14]_i_23_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg1(22 downto 19),
      S(3) => \adc_data_neg[14]_i_35_n_0\,
      S(2) => \adc_data_neg[14]_i_36_n_0\,
      S(1) => \adc_data_neg[14]_i_37_n_0\,
      S(0) => \adc_data_neg[14]_i_38_n_0\
    );
\adc_data_neg_reg[14]_i_24\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[14]_i_23_n_0\,
      CO(3) => \adc_data_neg_reg[14]_i_24_n_0\,
      CO(2) => \adc_data_neg_reg[14]_i_24_n_1\,
      CO(1) => \adc_data_neg_reg[14]_i_24_n_2\,
      CO(0) => \adc_data_neg_reg[14]_i_24_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg1(26 downto 23),
      S(3) => \adc_data_neg[14]_i_39_n_0\,
      S(2) => \adc_data_neg[14]_i_40_n_0\,
      S(1) => \adc_data_neg[14]_i_41_n_0\,
      S(0) => \adc_data_neg[14]_i_42_n_0\
    );
\adc_data_neg_reg[14]_i_25\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[14]_i_24_n_0\,
      CO(3) => \adc_data_neg_reg[14]_i_25_n_0\,
      CO(2) => \adc_data_neg_reg[14]_i_25_n_1\,
      CO(1) => \adc_data_neg_reg[14]_i_25_n_2\,
      CO(0) => \adc_data_neg_reg[14]_i_25_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg1(30 downto 27),
      S(3) => \adc_data_neg[14]_i_43_n_0\,
      S(2) => \adc_data_neg[14]_i_44_n_0\,
      S(1) => \adc_data_neg[14]_i_45_n_0\,
      S(0) => \adc_data_neg[14]_i_46_n_0\
    );
\adc_data_neg_reg[14]_i_26\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[14]_i_25_n_0\,
      CO(3 downto 0) => \NLW_adc_data_neg_reg[14]_i_26_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_adc_data_neg_reg[14]_i_26_O_UNCONNECTED\(3 downto 1),
      O(0) => adc_data_neg1(31),
      S(3 downto 1) => B"000",
      S(0) => \adc_data_neg[14]_i_47_n_0\
    );
\adc_data_neg_reg[14]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \adc_data_neg_reg[14]_i_6_n_0\,
      CO(2) => \adc_data_neg_reg[14]_i_6_n_1\,
      CO(1) => \adc_data_neg_reg[14]_i_6_n_2\,
      CO(0) => \adc_data_neg_reg[14]_i_6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3 downto 0) => adc_data_neg1(6 downto 3),
      S(3) => \adc_data_neg[14]_i_15_n_0\,
      S(2) => \adc_data_neg[14]_i_16_n_0\,
      S(1) => \adc_data_neg[14]_i_17_n_0\,
      S(0) => \adc_data_neg[14]_i_18_n_0\
    );
\adc_data_neg_reg[14]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[14]_i_12_n_0\,
      CO(3) => \adc_data_neg_reg[14]_i_7_n_0\,
      CO(2) => \adc_data_neg_reg[14]_i_7_n_1\,
      CO(1) => \adc_data_neg_reg[14]_i_7_n_2\,
      CO(0) => \adc_data_neg_reg[14]_i_7_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg1(18 downto 15),
      S(3) => \adc_data_neg[14]_i_19_n_0\,
      S(2) => \adc_data_neg[14]_i_20_n_0\,
      S(1) => \adc_data_neg[14]_i_21_n_0\,
      S(0) => \adc_data_neg[14]_i_22_n_0\
    );
\adc_data_neg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_neg[15]_i_1_n_0\,
      Q => \^adc_data\(15),
      R => '0'
    );
\adc_data_neg_reg[15]_i_10\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[15]_i_7_n_0\,
      CO(3) => \adc_data_neg_reg[15]_i_10_n_0\,
      CO(2) => \adc_data_neg_reg[15]_i_10_n_1\,
      CO(1) => \adc_data_neg_reg[15]_i_10_n_2\,
      CO(0) => \adc_data_neg_reg[15]_i_10_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg2(18 downto 15),
      S(3) => \adc_data_neg[15]_i_26_n_0\,
      S(2) => \adc_data_neg[15]_i_27_n_0\,
      S(1) => \adc_data_neg[15]_i_28_n_0\,
      S(0) => \adc_data_neg[15]_i_29_n_0\
    );
\adc_data_neg_reg[15]_i_30\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[15]_i_10_n_0\,
      CO(3) => \adc_data_neg_reg[15]_i_30_n_0\,
      CO(2) => \adc_data_neg_reg[15]_i_30_n_1\,
      CO(1) => \adc_data_neg_reg[15]_i_30_n_2\,
      CO(0) => \adc_data_neg_reg[15]_i_30_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg2(22 downto 19),
      S(3) => \adc_data_neg[15]_i_35_n_0\,
      S(2) => \adc_data_neg[15]_i_36_n_0\,
      S(1) => \adc_data_neg[15]_i_37_n_0\,
      S(0) => \adc_data_neg[15]_i_38_n_0\
    );
\adc_data_neg_reg[15]_i_31\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[15]_i_30_n_0\,
      CO(3) => \adc_data_neg_reg[15]_i_31_n_0\,
      CO(2) => \adc_data_neg_reg[15]_i_31_n_1\,
      CO(1) => \adc_data_neg_reg[15]_i_31_n_2\,
      CO(0) => \adc_data_neg_reg[15]_i_31_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg2(26 downto 23),
      S(3) => \adc_data_neg[15]_i_39_n_0\,
      S(2) => \adc_data_neg[15]_i_40_n_0\,
      S(1) => \adc_data_neg[15]_i_41_n_0\,
      S(0) => \adc_data_neg[15]_i_42_n_0\
    );
\adc_data_neg_reg[15]_i_32\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[15]_i_31_n_0\,
      CO(3) => \adc_data_neg_reg[15]_i_32_n_0\,
      CO(2) => \adc_data_neg_reg[15]_i_32_n_1\,
      CO(1) => \adc_data_neg_reg[15]_i_32_n_2\,
      CO(0) => \adc_data_neg_reg[15]_i_32_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg2(30 downto 27),
      S(3) => \adc_data_neg[15]_i_43_n_0\,
      S(2) => \adc_data_neg[15]_i_44_n_0\,
      S(1) => \adc_data_neg[15]_i_45_n_0\,
      S(0) => \adc_data_neg[15]_i_46_n_0\
    );
\adc_data_neg_reg[15]_i_33\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[15]_i_32_n_0\,
      CO(3 downto 0) => \NLW_adc_data_neg_reg[15]_i_33_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_adc_data_neg_reg[15]_i_33_O_UNCONNECTED\(3 downto 1),
      O(0) => adc_data_neg2(31),
      S(3 downto 1) => B"000",
      S(0) => \adc_data_neg[15]_i_47_n_0\
    );
\adc_data_neg_reg[15]_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[15]_i_6_n_0\,
      CO(3) => \adc_data_neg_reg[15]_i_5_n_0\,
      CO(2) => \adc_data_neg_reg[15]_i_5_n_1\,
      CO(1) => \adc_data_neg_reg[15]_i_5_n_2\,
      CO(0) => \adc_data_neg_reg[15]_i_5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg2(10 downto 7),
      S(3) => \adc_data_neg[15]_i_14_n_0\,
      S(2) => \adc_data_neg[15]_i_15_n_0\,
      S(1) => \adc_data_neg[15]_i_16_n_0\,
      S(0) => \adc_data_neg[15]_i_17_n_0\
    );
\adc_data_neg_reg[15]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \adc_data_neg_reg[15]_i_6_n_0\,
      CO(2) => \adc_data_neg_reg[15]_i_6_n_1\,
      CO(1) => \adc_data_neg_reg[15]_i_6_n_2\,
      CO(0) => \adc_data_neg_reg[15]_i_6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0010",
      O(3 downto 1) => adc_data_neg2(6 downto 4),
      O(0) => \NLW_adc_data_neg_reg[15]_i_6_O_UNCONNECTED\(0),
      S(3) => \adc_data_neg[15]_i_18_n_0\,
      S(2) => \adc_data_neg[15]_i_19_n_0\,
      S(1) => \adc_data_neg[15]_i_20_n_0\,
      S(0) => \adc_data_neg[15]_i_21_n_0\
    );
\adc_data_neg_reg[15]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \adc_data_neg_reg[15]_i_5_n_0\,
      CO(3) => \adc_data_neg_reg[15]_i_7_n_0\,
      CO(2) => \adc_data_neg_reg[15]_i_7_n_1\,
      CO(1) => \adc_data_neg_reg[15]_i_7_n_2\,
      CO(0) => \adc_data_neg_reg[15]_i_7_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => adc_data_neg2(14 downto 11),
      S(3) => \adc_data_neg[15]_i_22_n_0\,
      S(2) => \adc_data_neg[15]_i_23_n_0\,
      S(1) => \adc_data_neg[15]_i_24_n_0\,
      S(0) => \adc_data_neg[15]_i_25_n_0\
    );
\adc_data_neg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_neg[2]_i_1_n_0\,
      Q => \^adc_data\(2),
      R => '0'
    );
\adc_data_neg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_neg[3]_i_1_n_0\,
      Q => \^adc_data\(3),
      R => '0'
    );
\adc_data_neg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_neg[6]_i_1_n_0\,
      Q => \^adc_data\(6),
      R => '0'
    );
\adc_data_neg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_neg[7]_i_1_n_0\,
      Q => \^adc_data\(7),
      R => '0'
    );
\adc_data_pos[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_pos[16]_i_2_n_0\,
      I2 => is_it_first_strobe,
      I3 => \adc_data_pos[0]_i_2_n_0\,
      I4 => \^adc_data\(0),
      O => \adc_data_pos[0]_i_1_n_0\
    );
\adc_data_pos[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(2),
      I3 => \adc_data_pos[16]_i_2_n_0\,
      O => \adc_data_pos[0]_i_2_n_0\
    );
\adc_data_pos[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_pos[16]_i_2_n_0\,
      I2 => is_it_first_strobe,
      I3 => \adc_data_pos[12]_i_2_n_0\,
      I4 => \^adc_data\(12),
      O => \adc_data_pos[12]_i_1_n_0\
    );
\adc_data_pos[12]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \adc_data_pos[16]_i_2_n_0\,
      I1 => \^out_cnt\(2),
      I2 => \^out_cnt\(1),
      I3 => \^out_cnt\(0),
      O => \adc_data_pos[12]_i_2_n_0\
    );
\adc_data_pos[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_da,
      I1 => \adc_data_pos[13]_i_2_n_0\,
      I2 => \^adc_data\(13),
      O => \adc_data_pos[13]_i_1_n_0\
    );
\adc_data_pos[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000004"
    )
        port map (
      I0 => is_it_first_strobe,
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(1),
      I3 => \^out_cnt\(2),
      I4 => \adc_data_pos[16]_i_2_n_0\,
      O => \adc_data_pos[13]_i_2_n_0\
    );
\adc_data_pos[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAFEAA02"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_pos[16]_i_2_n_0\,
      I2 => \adc_data_pos[16]_i_3_n_0\,
      I3 => is_it_first_strobe,
      I4 => \^adc_data\(16),
      O => \adc_data_pos[16]_i_1_n_0\
    );
\adc_data_pos[16]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EEEA"
    )
        port map (
      I0 => data_ready_clk_INST_0_i_1_n_0,
      I1 => \^out_cnt\(2),
      I2 => \^out_cnt\(1),
      I3 => \^out_cnt\(0),
      O => \adc_data_pos[16]_i_2_n_0\
    );
\adc_data_pos[16]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \adc_data_pos[16]_i_2_n_0\,
      I1 => \^out_cnt\(2),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(1),
      O => \adc_data_pos[16]_i_3_n_0\
    );
\adc_data_pos[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_da,
      I1 => \adc_data_pos[17]_i_3_n_0\,
      I2 => \^adc_data\(17),
      O => \adc_data_pos[17]_i_1_n_0\
    );
\adc_data_pos[17]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => is_strobe_clk_active_neg,
      I1 => clk_adc_delay,
      I2 => clk_locked,
      I3 => is_strobe_clk_active,
      O => adc_data_pos0
    );
\adc_data_pos[17]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \adc_data_pos[16]_i_2_n_0\,
      I1 => \^out_cnt\(2),
      I2 => \^out_cnt\(0),
      I3 => \^out_cnt\(1),
      I4 => is_it_first_strobe,
      O => \adc_data_pos[17]_i_3_n_0\
    );
\adc_data_pos[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_da,
      I1 => \adc_data_pos[1]_i_2_n_0\,
      I2 => \^adc_data\(1),
      O => \adc_data_pos[1]_i_1_n_0\
    );
\adc_data_pos[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => is_it_first_strobe,
      I1 => \adc_data_pos[16]_i_2_n_0\,
      I2 => \^out_cnt\(2),
      I3 => \^out_cnt\(1),
      I4 => \^out_cnt\(0),
      O => \adc_data_pos[1]_i_2_n_0\
    );
\adc_data_pos[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_pos[16]_i_2_n_0\,
      I2 => is_it_first_strobe,
      I3 => \adc_data_pos[4]_i_2_n_0\,
      I4 => \^adc_data\(4),
      O => \adc_data_pos[4]_i_1_n_0\
    );
\adc_data_pos[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFFF"
    )
        port map (
      I0 => \adc_data_pos[16]_i_2_n_0\,
      I1 => \^out_cnt\(2),
      I2 => \^out_cnt\(1),
      I3 => \^out_cnt\(0),
      O => \adc_data_pos[4]_i_2_n_0\
    );
\adc_data_pos[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_da,
      I1 => \adc_data_pos[5]_i_2_n_0\,
      I2 => \^adc_data\(5),
      O => \adc_data_pos[5]_i_1_n_0\
    );
\adc_data_pos[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000040"
    )
        port map (
      I0 => is_it_first_strobe,
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(1),
      I3 => \^out_cnt\(2),
      I4 => \adc_data_pos[16]_i_2_n_0\,
      O => \adc_data_pos[5]_i_2_n_0\
    );
\adc_data_pos[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => adc_db,
      I1 => \adc_data_pos[16]_i_2_n_0\,
      I2 => is_it_first_strobe,
      I3 => \adc_data_pos[8]_i_2_n_0\,
      I4 => \^adc_data\(8),
      O => \adc_data_pos[8]_i_1_n_0\
    );
\adc_data_pos[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFB"
    )
        port map (
      I0 => \^out_cnt\(0),
      I1 => \^out_cnt\(1),
      I2 => \^out_cnt\(2),
      I3 => \adc_data_pos[16]_i_2_n_0\,
      O => \adc_data_pos[8]_i_2_n_0\
    );
\adc_data_pos[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_da,
      I1 => \adc_data_pos[9]_i_2_n_0\,
      I2 => \^adc_data\(9),
      O => \adc_data_pos[9]_i_1_n_0\
    );
\adc_data_pos[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => is_it_first_strobe,
      I1 => \adc_data_pos[16]_i_2_n_0\,
      I2 => \^out_cnt\(2),
      I3 => \^out_cnt\(1),
      I4 => \^out_cnt\(0),
      O => \adc_data_pos[9]_i_2_n_0\
    );
\adc_data_pos_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[0]_i_1_n_0\,
      Q => \^adc_data\(0),
      R => '0'
    );
\adc_data_pos_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[12]_i_1_n_0\,
      Q => \^adc_data\(12),
      R => '0'
    );
\adc_data_pos_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[13]_i_1_n_0\,
      Q => \^adc_data\(13),
      R => '0'
    );
\adc_data_pos_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[16]_i_1_n_0\,
      Q => \^adc_data\(16),
      R => '0'
    );
\adc_data_pos_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[17]_i_1_n_0\,
      Q => \^adc_data\(17),
      R => '0'
    );
\adc_data_pos_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[1]_i_1_n_0\,
      Q => \^adc_data\(1),
      R => '0'
    );
\adc_data_pos_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[4]_i_1_n_0\,
      Q => \^adc_data\(4),
      R => '0'
    );
\adc_data_pos_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[5]_i_1_n_0\,
      Q => \^adc_data\(5),
      R => '0'
    );
\adc_data_pos_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[8]_i_1_n_0\,
      Q => \^adc_data\(8),
      R => '0'
    );
\adc_data_pos_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => \adc_data_pos[9]_i_1_n_0\,
      Q => \^adc_data\(9),
      R => '0'
    );
adc_reading_allowed_neg_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A2AE"
    )
        port map (
      I0 => adc_reading_allowed_neg_reg_reg_n_0,
      I1 => \adc_current_clk_num_reg_reg_n_0_[0]\,
      I2 => \adc_current_clk_num_reg_reg_n_0_[1]\,
      I3 => \adc_current_clk_num_reg_reg_n_0_[2]\,
      O => adc_reading_allowed_neg_reg_i_1_n_0
    );
adc_reading_allowed_neg_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => tfirstclk_passed_prev0,
      CE => '1',
      D => adc_reading_allowed_neg_reg_i_1_n_0,
      Q => adc_reading_allowed_neg_reg_reg_n_0,
      R => '0'
    );
adc_reading_allowed_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF00DFFFDF00DF00"
    )
        port map (
      I0 => \adc_current_clk_num_reg_reg_n_0_[0]\,
      I1 => \adc_current_clk_num_reg_reg_n_0_[1]\,
      I2 => \adc_current_clk_num_reg_reg_n_0_[2]\,
      I3 => adc_reading_allowed_reg_reg_n_0,
      I4 => tfirstclk_passed_prev,
      I5 => tfirstclk_passed,
      O => adc_reading_allowed_reg_i_1_n_0
    );
adc_reading_allowed_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tfirstclk_passed_prev0,
      CE => '1',
      D => adc_reading_allowed_reg_i_1_n_0,
      Q => adc_reading_allowed_reg_reg_n_0,
      R => '0'
    );
clk_wizard_adc: entity work.design_1_ADC_GET_DATA_0_0_clk_wiz_1
     port map (
      clk_in1 => fpga_clk,
      clk_out1 => clk_adc,
      clk_out2 => clk_adc_delay,
      locked => clk_locked
    );
data_ready_clk_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => data_ready_clk_INST_0_i_1_n_0,
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(2),
      I3 => \^out_cnt\(1),
      O => data_ready_clk
    );
data_ready_clk_INST_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^out_cnt\(6),
      I1 => \^out_cnt\(4),
      I2 => \^out_cnt\(3),
      I3 => \^out_cnt\(5),
      I4 => \^out_cnt\(7),
      O => data_ready_clk_INST_0_i_1_n_0
    );
\fpga_clk_counter[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => fpga_clk_counter_reg(0),
      O => \p_0_in__0\(0)
    );
\fpga_clk_counter[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => fpga_clk_counter_reg(0),
      I1 => fpga_clk_counter_reg(1),
      O => \p_0_in__0\(1)
    );
\fpga_clk_counter[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => fpga_clk_counter_reg(0),
      I1 => fpga_clk_counter_reg(1),
      I2 => fpga_clk_counter_reg(2),
      O => \fpga_clk_counter[2]_i_1_n_0\
    );
\fpga_clk_counter[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => fpga_clk_counter_reg(1),
      I1 => fpga_clk_counter_reg(0),
      I2 => fpga_clk_counter_reg(2),
      I3 => fpga_clk_counter_reg(3),
      O => \p_0_in__0\(3)
    );
\fpga_clk_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => \p_0_in__0\(0),
      Q => fpga_clk_counter_reg(0),
      R => clear
    );
\fpga_clk_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => \p_0_in__0\(1),
      Q => fpga_clk_counter_reg(1),
      R => clear
    );
\fpga_clk_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => \fpga_clk_counter[2]_i_1_n_0\,
      Q => fpga_clk_counter_reg(2),
      R => clear
    );
\fpga_clk_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => \p_0_in__0\(3),
      Q => fpga_clk_counter_reg(3),
      R => clear
    );
\inst/\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA8"
    )
        port map (
      I0 => fpga_clk_counter_reg(3),
      I1 => fpga_clk_counter_reg(0),
      I2 => fpga_clk_counter_reg(1),
      I3 => fpga_clk_counter_reg(2),
      O => clear
    );
is_data_ready_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAA3AA"
    )
        port map (
      I0 => \^data_ready\,
      I1 => \^out_cnt\(0),
      I2 => \^out_cnt\(1),
      I3 => \^out_cnt\(2),
      I4 => data_ready_clk_INST_0_i_1_n_0,
      O => is_data_ready_i_1_n_0
    );
is_data_ready_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_data_pos0,
      CE => '1',
      D => is_data_ready_i_1_n_0,
      Q => \^data_ready\,
      R => '0'
    );
is_it_first_strobe_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"70707770"
    )
        port map (
      I0 => \adc_reading_allowed_reg1__1\,
      I1 => adc_reading_allowed_reg_reg_n_0,
      I2 => is_it_first_strobe,
      I3 => tfirstclk_passed,
      I4 => tfirstclk_passed_prev,
      O => is_it_first_strobe_i_1_n_0
    );
is_it_first_strobe_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1F"
    )
        port map (
      I0 => \adc_current_clk_num_reg_reg_n_0_[0]\,
      I1 => \adc_current_clk_num_reg_reg_n_0_[1]\,
      I2 => \adc_current_clk_num_reg_reg_n_0_[2]\,
      O => \adc_reading_allowed_reg1__1\
    );
is_it_first_strobe_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tfirstclk_passed_prev0,
      CE => '1',
      D => is_it_first_strobe_i_1_n_0,
      Q => is_it_first_strobe,
      R => '0'
    );
is_strobe_clk_active_neg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_adc_delay,
      CE => '1',
      D => adc_reading_allowed_neg_reg_reg_n_0,
      Q => is_strobe_clk_active_neg,
      R => '0'
    );
is_strobe_clk_active_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_adc_delay,
      CE => '1',
      D => adc_reading_allowed_reg_reg_n_0,
      Q => is_strobe_clk_active,
      R => '0'
    );
tfirstclk_passed_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA8AAA"
    )
        port map (
      I0 => tfirstclk_passed,
      I1 => fpga_clk_counter_reg(1),
      I2 => fpga_clk_counter_reg(0),
      I3 => fpga_clk_counter_reg(3),
      I4 => fpga_clk_counter_reg(2),
      O => tfirstclk_passed_i_1_n_0
    );
tfirstclk_passed_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => tfirstclk_passed_prev0,
      CE => '1',
      D => tfirstclk_passed,
      Q => tfirstclk_passed_prev,
      R => '0'
    );
tfirstclk_passed_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => tfirstclk_passed_i_1_n_0,
      Q => tfirstclk_passed,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_ADC_GET_DATA_0_0 is
  port (
    adc_data : out STD_LOGIC_VECTOR ( 17 downto 0 );
    data_ready : out STD_LOGIC;
    data_ready_clk : out STD_LOGIC;
    out_cnt : out STD_LOGIC_VECTOR ( 7 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    fpga_clkx_p : in STD_LOGIC;
    fpga_clkx_n : in STD_LOGIC;
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_ADC_GET_DATA_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_ADC_GET_DATA_0_0 : entity is "design_1_ADC_GET_DATA_0_0,adc_get_data,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_ADC_GET_DATA_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_ADC_GET_DATA_0_0 : entity is "adc_get_data,Vivado 2019.1";
end design_1_ADC_GET_DATA_0_0;

architecture STRUCTURE of design_1_ADC_GET_DATA_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of adc_clkx_n : signal is "bt.local:interface:diff:1.0 adc_clkx n";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of adc_clkx_n : signal is "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_clkx_p : signal is "bt.local:interface:diff:1.0 adc_clkx p";
  attribute X_INTERFACE_INFO of adc_dax_n : signal is "bt.local:interface:diff:1.0 adc_dax n";
  attribute X_INTERFACE_PARAMETER of adc_dax_n : signal is "XIL_INTERFACENAME adc_dax, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dax_p : signal is "bt.local:interface:diff:1.0 adc_dax p";
  attribute X_INTERFACE_INFO of adc_dbx_n : signal is "bt.local:interface:diff:1.0 adc_dbx n";
  attribute X_INTERFACE_PARAMETER of adc_dbx_n : signal is "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dbx_p : signal is "bt.local:interface:diff:1.0 adc_dbx p";
  attribute X_INTERFACE_INFO of adc_dcox_n : signal is "bt.local:interface:diff:1.0 adc_dcox n";
  attribute X_INTERFACE_PARAMETER of adc_dcox_n : signal is "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dcox_p : signal is "bt.local:interface:diff:1.0 adc_dcox p";
  attribute X_INTERFACE_INFO of fpga_clkx_n : signal is "bt.local:interface:diff:1.0 fpga_clkx n";
  attribute X_INTERFACE_PARAMETER of fpga_clkx_n : signal is "XIL_INTERFACENAME fpga_clkx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of fpga_clkx_p : signal is "bt.local:interface:diff:1.0 fpga_clkx p";
begin
inst: entity work.design_1_ADC_GET_DATA_0_0_adc_get_data
     port map (
      adc_clkx_n => adc_clkx_n,
      adc_clkx_p => adc_clkx_p,
      adc_data(17 downto 0) => adc_data(17 downto 0),
      adc_dax_n => adc_dax_n,
      adc_dax_p => adc_dax_p,
      adc_dbx_n => adc_dbx_n,
      adc_dbx_p => adc_dbx_p,
      adc_dcox_n => adc_dcox_n,
      adc_dcox_p => adc_dcox_p,
      data_ready => data_ready,
      data_ready_clk => data_ready_clk,
      fpga_clkx_n => fpga_clkx_n,
      fpga_clkx_p => fpga_clkx_p,
      out_cnt(7 downto 0) => out_cnt(7 downto 0)
    );
end STRUCTURE;
