-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun 23 11:10:01 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               c:/project/test_ADC_GET_DATA2/test_ADC_GET_DATA2.srcs/sources_1/bd/design_1/ip/design_1_init_clk_0_0/design_1_init_clk_0_0_sim_netlist.vhdl
-- Design      : design_1_init_clk_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_init_clk_0_0_clk_wiz_0_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    clk_out4 : out STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_init_clk_0_0_clk_wiz_0_clk_wiz : entity is "clk_wiz_0_clk_wiz";
end design_1_init_clk_0_0_clk_wiz_0_clk_wiz;

architecture STRUCTURE of design_1_init_clk_0_0_clk_wiz_0_clk_wiz is
  signal clk_in1_clk_wiz_0 : STD_LOGIC;
  signal clk_out1_clk_wiz_0 : STD_LOGIC;
  signal clk_out2_clk_wiz_0 : STD_LOGIC;
  signal clk_out3_clk_wiz_0 : STD_LOGIC;
  signal clk_out4_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_clk_wiz_0 : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkin1_ibufg : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufg : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufg : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufg : label is "AUTO";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout3_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout4_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of mmcm_adv_inst : label is "PRIMITIVE";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_0,
      O => clkfbout_buf_clk_wiz_0
    );
clkin1_ibufg: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in1,
      O => clk_in1_clk_wiz_0
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_0,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_0,
      O => clk_out2
    );
clkout3_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out3_clk_wiz_0,
      O => clk_out3
    );
clkout4_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out4_clk_wiz_0,
      O => clk_out4
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 10.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 125,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 50,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 100,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_0,
      CLKFBOUT => clkfbout_clk_wiz_0,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => clk_in1_clk_wiz_0,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_0,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => clk_out3_clk_wiz_0,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => clk_out4_clk_wiz_0,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_init_clk_0_0_clk_wiz_0 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    clk_out4 : out STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_init_clk_0_0_clk_wiz_0 : entity is "clk_wiz_0";
end design_1_init_clk_0_0_clk_wiz_0;

architecture STRUCTURE of design_1_init_clk_0_0_clk_wiz_0 is
begin
inst: entity work.design_1_init_clk_0_0_clk_wiz_0_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      clk_out3 => clk_out3,
      clk_out4 => clk_out4,
      locked => locked
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_init_clk_0_0_init_clk is
  port (
    clk_start_init_reg_0 : out STD_LOGIC;
    clk_cs : out STD_LOGIC;
    clk_sclk : out STD_LOGIC;
    clk_mosi : out STD_LOGIC;
    start_ext : in STD_LOGIC;
    clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_init_clk_0_0_init_clk : entity is "init_clk";
end design_1_init_clk_0_0_init_clk;

architecture STRUCTURE of design_1_init_clk_0_0_init_clk is
  signal \bits_left[6]_i_1_n_0\ : STD_LOGIC;
  signal \bits_left[6]_i_2_n_0\ : STD_LOGIC;
  signal \bits_left[6]_i_4_n_0\ : STD_LOGIC;
  signal bits_left_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal clk_8MHz : STD_LOGIC;
  signal \^clk_cs\ : STD_LOGIC;
  signal clk_cs_reg_i_1_n_0 : STD_LOGIC;
  signal clk_cs_reg_i_2_n_0 : STD_LOGIC;
  signal clk_cs_reg_i_3_n_0 : STD_LOGIC;
  signal clk_cs_reg_i_4_n_0 : STD_LOGIC;
  signal \^clk_mosi\ : STD_LOGIC;
  signal clk_mosi_reg_i_1_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_2_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_3_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_4_n_0 : STD_LOGIC;
  signal clk_sclk_INST_0_i_1_n_0 : STD_LOGIC;
  signal clk_start_init_i_10_n_0 : STD_LOGIC;
  signal clk_start_init_i_11_n_0 : STD_LOGIC;
  signal clk_start_init_i_12_n_0 : STD_LOGIC;
  signal clk_start_init_i_13_n_0 : STD_LOGIC;
  signal clk_start_init_i_14_n_0 : STD_LOGIC;
  signal clk_start_init_i_15_n_0 : STD_LOGIC;
  signal clk_start_init_i_16_n_0 : STD_LOGIC;
  signal clk_start_init_i_17_n_0 : STD_LOGIC;
  signal clk_start_init_i_18_n_0 : STD_LOGIC;
  signal clk_start_init_i_1_n_0 : STD_LOGIC;
  signal clk_start_init_i_3_n_0 : STD_LOGIC;
  signal clk_start_init_i_4_n_0 : STD_LOGIC;
  signal clk_start_init_i_5_n_0 : STD_LOGIC;
  signal clk_start_init_i_6_n_0 : STD_LOGIC;
  signal clk_start_init_i_7_n_0 : STD_LOGIC;
  signal clk_start_init_i_8_n_0 : STD_LOGIC;
  signal clk_start_init_i_9_n_0 : STD_LOGIC;
  signal \^clk_start_init_reg_0\ : STD_LOGIC;
  signal clk_sync_counter_reg : STD_LOGIC;
  signal clk_sync_counter_reg0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal clk_sync_counter_reg0_0 : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__2_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__2_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__2_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__3_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__3_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__3_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__4_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__4_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__4_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__5_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__5_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__5_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__6_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry__6_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_10_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_11_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_12_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_13_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_8_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_9_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_10_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_8_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_9_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_10_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_11_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_12_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_13_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_14_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_15_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_16_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_17_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_18_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_8_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_9_n_0\ : STD_LOGIC;
  signal clk_sync_counter_reg_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \clk_sync_counter_reg_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal is_clk_initialized_reg : STD_LOGIC;
  signal is_clk_initialized_reg_i_1_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal start_prev : STD_LOGIC;
  signal start_prev_i_1_n_0 : STD_LOGIC;
  signal sys_clk_locked : STD_LOGIC;
  signal \NLW_clk_sync_counter_reg0_inferred__0/i__carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_clk_sync_counter_reg0_inferred__0/i__carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_clk_wizard_main_clk_out1_UNCONNECTED : STD_LOGIC;
  signal NLW_clk_wizard_main_clk_out3_UNCONNECTED : STD_LOGIC;
  signal NLW_clk_wizard_main_clk_out4_UNCONNECTED : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \bits_left[0]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \bits_left[1]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \bits_left[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \bits_left[3]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \bits_left[6]_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of clk_cs_reg_i_4 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of clk_start_init_i_15 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of clk_start_init_i_16 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_11\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_13\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[4]_i_10\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[4]_i_9\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[8]_i_10\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[8]_i_13\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[8]_i_16\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[8]_i_9\ : label is "soft_lutpair1";
begin
  clk_cs <= \^clk_cs\;
  clk_mosi <= \^clk_mosi\;
  clk_start_init_reg_0 <= \^clk_start_init_reg_0\;
\bits_left[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sclk_INST_0_i_1_n_0,
      I1 => bits_left_reg(0),
      O => p_0_in(0)
    );
\bits_left[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => clk_sclk_INST_0_i_1_n_0,
      I1 => bits_left_reg(1),
      I2 => bits_left_reg(0),
      O => p_0_in(1)
    );
\bits_left[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A802"
    )
        port map (
      I0 => clk_sclk_INST_0_i_1_n_0,
      I1 => bits_left_reg(0),
      I2 => bits_left_reg(1),
      I3 => bits_left_reg(2),
      O => p_0_in(2)
    );
\bits_left[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA80002"
    )
        port map (
      I0 => clk_sclk_INST_0_i_1_n_0,
      I1 => bits_left_reg(1),
      I2 => bits_left_reg(0),
      I3 => bits_left_reg(2),
      I4 => bits_left_reg(3),
      O => p_0_in(3)
    );
\bits_left[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000002"
    )
        port map (
      I0 => clk_sclk_INST_0_i_1_n_0,
      I1 => bits_left_reg(1),
      I2 => bits_left_reg(0),
      I3 => bits_left_reg(3),
      I4 => bits_left_reg(2),
      I5 => bits_left_reg(4),
      O => p_0_in(4)
    );
\bits_left[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFC00000002"
    )
        port map (
      I0 => bits_left_reg(6),
      I1 => \bits_left[6]_i_4_n_0\,
      I2 => bits_left_reg(0),
      I3 => bits_left_reg(1),
      I4 => bits_left_reg(4),
      I5 => bits_left_reg(5),
      O => p_0_in(5)
    );
\bits_left[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => start_prev,
      I1 => \^clk_start_init_reg_0\,
      I2 => start_ext,
      O => \bits_left[6]_i_1_n_0\
    );
\bits_left[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8880"
    )
        port map (
      I0 => \^clk_start_init_reg_0\,
      I1 => start_ext,
      I2 => clk_sclk_INST_0_i_1_n_0,
      I3 => bits_left_reg(0),
      O => \bits_left[6]_i_2_n_0\
    );
\bits_left[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA8"
    )
        port map (
      I0 => bits_left_reg(6),
      I1 => bits_left_reg(5),
      I2 => \bits_left[6]_i_4_n_0\,
      I3 => bits_left_reg(0),
      I4 => bits_left_reg(1),
      I5 => bits_left_reg(4),
      O => p_0_in(6)
    );
\bits_left[6]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => bits_left_reg(2),
      I1 => bits_left_reg(3),
      O => \bits_left[6]_i_4_n_0\
    );
\bits_left_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(0),
      Q => bits_left_reg(0),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(1),
      Q => bits_left_reg(1),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(2),
      Q => bits_left_reg(2),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(3),
      Q => bits_left_reg(3),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(4),
      Q => bits_left_reg(4),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(5),
      Q => bits_left_reg(5),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(6),
      Q => bits_left_reg(6),
      S => \bits_left[6]_i_1_n_0\
    );
clk_cs_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAA00AAAAAA"
    )
        port map (
      I0 => \^clk_cs\,
      I1 => clk_cs_reg_i_2_n_0,
      I2 => clk_cs_reg_i_3_n_0,
      I3 => start_ext,
      I4 => \^clk_start_init_reg_0\,
      I5 => start_prev,
      O => clk_cs_reg_i_1_n_0
    );
clk_cs_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => bits_left_reg(4),
      I1 => bits_left_reg(1),
      I2 => bits_left_reg(0),
      I3 => bits_left_reg(3),
      I4 => bits_left_reg(2),
      I5 => bits_left_reg(5),
      O => clk_cs_reg_i_2_n_0
    );
clk_cs_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEEEAAAA"
    )
        port map (
      I0 => bits_left_reg(6),
      I1 => bits_left_reg(2),
      I2 => bits_left_reg(1),
      I3 => bits_left_reg(0),
      I4 => clk_cs_reg_i_4_n_0,
      O => clk_cs_reg_i_3_n_0
    );
clk_cs_reg_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => bits_left_reg(3),
      I1 => bits_left_reg(4),
      I2 => bits_left_reg(5),
      O => clk_cs_reg_i_4_n_0
    );
clk_cs_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => clk_cs_reg_i_1_n_0,
      Q => \^clk_cs\,
      R => '0'
    );
clk_mosi_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFFBFAAAA0080"
    )
        port map (
      I0 => clk_mosi_reg_i_2_n_0,
      I1 => start_ext,
      I2 => \^clk_start_init_reg_0\,
      I3 => start_prev,
      I4 => \bits_left[6]_i_2_n_0\,
      I5 => \^clk_mosi\,
      O => clk_mosi_reg_i_1_n_0
    );
clk_mosi_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000DFFFFFF"
    )
        port map (
      I0 => bits_left_reg(0),
      I1 => clk_sclk_INST_0_i_1_n_0,
      I2 => clk_mosi_reg_i_3_n_0,
      I3 => start_ext,
      I4 => \^clk_start_init_reg_0\,
      I5 => clk_mosi_reg_i_4_n_0,
      O => clk_mosi_reg_i_2_n_0
    );
clk_mosi_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFBFBBB"
    )
        port map (
      I0 => bits_left_reg(6),
      I1 => start_prev,
      I2 => clk_cs_reg_i_4_n_0,
      I3 => bits_left_reg(1),
      I4 => bits_left_reg(2),
      O => clk_mosi_reg_i_3_n_0
    );
clk_mosi_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF74FCF1FF70FFFD"
    )
        port map (
      I0 => bits_left_reg(5),
      I1 => bits_left_reg(1),
      I2 => bits_left_reg(3),
      I3 => bits_left_reg(2),
      I4 => bits_left_reg(4),
      I5 => bits_left_reg(0),
      O => clk_mosi_reg_i_4_n_0
    );
clk_mosi_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => clk_mosi_reg_i_1_n_0,
      Q => \^clk_mosi\,
      R => '0'
    );
clk_sclk_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => clk,
      I1 => clk_sclk_INST_0_i_1_n_0,
      I2 => bits_left_reg(0),
      O => clk_sclk
    );
clk_sclk_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => bits_left_reg(6),
      I1 => bits_left_reg(4),
      I2 => bits_left_reg(2),
      I3 => bits_left_reg(3),
      I4 => bits_left_reg(1),
      I5 => bits_left_reg(5),
      O => clk_sclk_INST_0_i_1_n_0
    );
clk_start_init_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCCCC8FCCCCCC"
    )
        port map (
      I0 => clk_start_init_i_3_n_0,
      I1 => \^clk_start_init_reg_0\,
      I2 => clk_start_init_i_4_n_0,
      I3 => clk_start_init_i_5_n_0,
      I4 => start_ext,
      I5 => is_clk_initialized_reg,
      O => clk_start_init_i_1_n_0
    );
clk_start_init_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(17),
      I1 => clk_sync_counter_reg_reg(16),
      I2 => clk_start_init_i_18_n_0,
      I3 => clk_sync_counter_reg_reg(6),
      I4 => clk_sync_counter_reg_reg(4),
      I5 => clk_start_init_i_16_n_0,
      O => clk_start_init_i_10_n_0
    );
clk_start_init_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(21),
      I1 => clk_sync_counter_reg_reg(22),
      I2 => clk_sync_counter_reg_reg(19),
      I3 => clk_sync_counter_reg_reg(20),
      I4 => clk_sync_counter_reg_reg(18),
      I5 => clk_sync_counter_reg_reg(23),
      O => clk_start_init_i_11_n_0
    );
clk_start_init_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg_reg(12),
      I3 => clk_sync_counter_reg_reg(11),
      O => clk_start_init_i_12_n_0
    );
clk_start_init_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FAF8000000000000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(7),
      I1 => clk_sync_counter_reg_reg(6),
      I2 => clk_sync_counter_reg_reg(8),
      I3 => clk_sync_counter_reg_reg(5),
      I4 => clk_sync_counter_reg_reg(9),
      I5 => clk_sync_counter_reg_reg(10),
      O => clk_start_init_i_13_n_0
    );
clk_start_init_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(29),
      I1 => clk_sync_counter_reg_reg(31),
      I2 => clk_sync_counter_reg_reg(26),
      I3 => clk_sync_counter_reg_reg(30),
      O => clk_start_init_i_14_n_0
    );
clk_start_init_i_15: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(6),
      I1 => clk_sync_counter_reg_reg(5),
      I2 => clk_sync_counter_reg_reg(7),
      O => clk_start_init_i_15_n_0
    );
clk_start_init_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(10),
      I1 => clk_sync_counter_reg_reg(9),
      O => clk_start_init_i_16_n_0
    );
clk_start_init_i_17: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(1),
      I1 => clk_sync_counter_reg_reg(0),
      I2 => clk_sync_counter_reg_reg(2),
      I3 => clk_sync_counter_reg_reg(3),
      O => clk_start_init_i_17_n_0
    );
clk_start_init_i_18: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(7),
      I1 => clk_sync_counter_reg_reg(5),
      O => clk_start_init_i_18_n_0
    );
clk_start_init_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_8MHz,
      I1 => sys_clk_locked,
      O => clk_sync_counter_reg0_0
    );
clk_start_init_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"222A222A222A22AA"
    )
        port map (
      I0 => clk_start_init_i_6_n_0,
      I1 => clk_sync_counter_reg_reg(17),
      I2 => clk_sync_counter_reg_reg(15),
      I3 => clk_sync_counter_reg_reg(16),
      I4 => clk_start_init_i_7_n_0,
      I5 => clk_sync_counter_reg_reg(14),
      O => clk_start_init_i_3_n_0
    );
clk_start_init_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => clk_start_init_i_8_n_0,
      I1 => clk_start_init_i_9_n_0,
      I2 => clk_start_init_i_10_n_0,
      I3 => clk_start_init_i_11_n_0,
      O => clk_start_init_i_4_n_0
    );
clk_start_init_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDDDDDDDDDDDDD"
    )
        port map (
      I0 => clk_start_init_i_6_n_0,
      I1 => clk_sync_counter_reg_reg(17),
      I2 => clk_start_init_i_12_n_0,
      I3 => clk_start_init_i_13_n_0,
      I4 => clk_sync_counter_reg_reg(16),
      I5 => clk_sync_counter_reg_reg(15),
      O => clk_start_init_i_5_n_0
    );
clk_start_init_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => clk_start_init_i_11_n_0,
      I1 => clk_sync_counter_reg_reg(28),
      I2 => clk_sync_counter_reg_reg(24),
      I3 => clk_sync_counter_reg_reg(25),
      I4 => clk_start_init_i_14_n_0,
      I5 => clk_sync_counter_reg_reg(27),
      O => clk_start_init_i_6_n_0
    );
clk_start_init_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => clk_start_init_i_15_n_0,
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(13),
      I3 => clk_sync_counter_reg_reg(8),
      I4 => clk_sync_counter_reg_reg(11),
      I5 => clk_start_init_i_16_n_0,
      O => clk_start_init_i_7_n_0
    );
clk_start_init_i_8: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => clk_start_init_i_17_n_0,
      I1 => clk_sync_counter_reg_reg(8),
      I2 => clk_sync_counter_reg_reg(15),
      I3 => clk_sync_counter_reg_reg(27),
      I4 => clk_start_init_i_12_n_0,
      O => clk_start_init_i_8_n_0
    );
clk_start_init_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(28),
      I1 => clk_sync_counter_reg_reg(24),
      I2 => clk_sync_counter_reg_reg(25),
      I3 => clk_start_init_i_14_n_0,
      O => clk_start_init_i_9_n_0
    );
clk_start_init_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => '1',
      D => clk_start_init_i_1_n_0,
      Q => \^clk_start_init_reg_0\,
      R => '0'
    );
\clk_sync_counter_reg0_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \clk_sync_counter_reg0_inferred__0/i__carry_n_0\,
      CO(2) => \clk_sync_counter_reg0_inferred__0/i__carry_n_1\,
      CO(1) => \clk_sync_counter_reg0_inferred__0/i__carry_n_2\,
      CO(0) => \clk_sync_counter_reg0_inferred__0/i__carry_n_3\,
      CYINIT => clk_sync_counter_reg_reg(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(4 downto 1),
      S(3 downto 0) => clk_sync_counter_reg_reg(4 downto 1)
    );
\clk_sync_counter_reg0_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_inferred__0/i__carry_n_0\,
      CO(3) => \clk_sync_counter_reg0_inferred__0/i__carry__0_n_0\,
      CO(2) => \clk_sync_counter_reg0_inferred__0/i__carry__0_n_1\,
      CO(1) => \clk_sync_counter_reg0_inferred__0/i__carry__0_n_2\,
      CO(0) => \clk_sync_counter_reg0_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(8 downto 5),
      S(3 downto 0) => clk_sync_counter_reg_reg(8 downto 5)
    );
\clk_sync_counter_reg0_inferred__0/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_inferred__0/i__carry__0_n_0\,
      CO(3) => \clk_sync_counter_reg0_inferred__0/i__carry__1_n_0\,
      CO(2) => \clk_sync_counter_reg0_inferred__0/i__carry__1_n_1\,
      CO(1) => \clk_sync_counter_reg0_inferred__0/i__carry__1_n_2\,
      CO(0) => \clk_sync_counter_reg0_inferred__0/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(12 downto 9),
      S(3 downto 0) => clk_sync_counter_reg_reg(12 downto 9)
    );
\clk_sync_counter_reg0_inferred__0/i__carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_inferred__0/i__carry__1_n_0\,
      CO(3) => \clk_sync_counter_reg0_inferred__0/i__carry__2_n_0\,
      CO(2) => \clk_sync_counter_reg0_inferred__0/i__carry__2_n_1\,
      CO(1) => \clk_sync_counter_reg0_inferred__0/i__carry__2_n_2\,
      CO(0) => \clk_sync_counter_reg0_inferred__0/i__carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(16 downto 13),
      S(3 downto 0) => clk_sync_counter_reg_reg(16 downto 13)
    );
\clk_sync_counter_reg0_inferred__0/i__carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_inferred__0/i__carry__2_n_0\,
      CO(3) => \clk_sync_counter_reg0_inferred__0/i__carry__3_n_0\,
      CO(2) => \clk_sync_counter_reg0_inferred__0/i__carry__3_n_1\,
      CO(1) => \clk_sync_counter_reg0_inferred__0/i__carry__3_n_2\,
      CO(0) => \clk_sync_counter_reg0_inferred__0/i__carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(20 downto 17),
      S(3 downto 0) => clk_sync_counter_reg_reg(20 downto 17)
    );
\clk_sync_counter_reg0_inferred__0/i__carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_inferred__0/i__carry__3_n_0\,
      CO(3) => \clk_sync_counter_reg0_inferred__0/i__carry__4_n_0\,
      CO(2) => \clk_sync_counter_reg0_inferred__0/i__carry__4_n_1\,
      CO(1) => \clk_sync_counter_reg0_inferred__0/i__carry__4_n_2\,
      CO(0) => \clk_sync_counter_reg0_inferred__0/i__carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(24 downto 21),
      S(3 downto 0) => clk_sync_counter_reg_reg(24 downto 21)
    );
\clk_sync_counter_reg0_inferred__0/i__carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_inferred__0/i__carry__4_n_0\,
      CO(3) => \clk_sync_counter_reg0_inferred__0/i__carry__5_n_0\,
      CO(2) => \clk_sync_counter_reg0_inferred__0/i__carry__5_n_1\,
      CO(1) => \clk_sync_counter_reg0_inferred__0/i__carry__5_n_2\,
      CO(0) => \clk_sync_counter_reg0_inferred__0/i__carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(28 downto 25),
      S(3 downto 0) => clk_sync_counter_reg_reg(28 downto 25)
    );
\clk_sync_counter_reg0_inferred__0/i__carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_inferred__0/i__carry__5_n_0\,
      CO(3 downto 2) => \NLW_clk_sync_counter_reg0_inferred__0/i__carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \clk_sync_counter_reg0_inferred__0/i__carry__6_n_2\,
      CO(0) => \clk_sync_counter_reg0_inferred__0/i__carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_clk_sync_counter_reg0_inferred__0/i__carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => clk_sync_counter_reg0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => clk_sync_counter_reg_reg(31 downto 29)
    );
\clk_sync_counter_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => clk_start_init_i_3_n_0,
      I1 => start_ext,
      I2 => is_clk_initialized_reg,
      O => clk_sync_counter_reg
    );
\clk_sync_counter_reg[0]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFEB"
    )
        port map (
      I0 => \clk_sync_counter_reg[8]_i_10_n_0\,
      I1 => clk_sync_counter_reg_reg(16),
      I2 => clk_sync_counter_reg_reg(17),
      I3 => clk_sync_counter_reg_reg(11),
      I4 => clk_sync_counter_reg_reg(4),
      I5 => clk_sync_counter_reg_reg(6),
      O => \clk_sync_counter_reg[0]_i_10_n_0\
    );
\clk_sync_counter_reg[0]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF9FF9"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(10),
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(16),
      I3 => clk_sync_counter_reg_reg(15),
      I4 => \clk_sync_counter_reg[0]_i_13_n_0\,
      O => \clk_sync_counter_reg[0]_i_11_n_0\
    );
\clk_sync_counter_reg[0]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"777F"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(15),
      I1 => clk_sync_counter_reg_reg(16),
      I2 => clk_start_init_i_13_n_0,
      I3 => clk_start_init_i_12_n_0,
      O => \clk_sync_counter_reg[0]_i_12_n_0\
    );
\clk_sync_counter_reg[0]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BFFD"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(8),
      I1 => clk_sync_counter_reg_reg(9),
      I2 => clk_sync_counter_reg_reg(7),
      I3 => clk_sync_counter_reg_reg(5),
      O => \clk_sync_counter_reg[0]_i_13_n_0\
    );
\clk_sync_counter_reg[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      O => \clk_sync_counter_reg[0]_i_3_n_0\
    );
\clk_sync_counter_reg[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(3),
      I2 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I3 => clk_sync_counter_reg0(3),
      O => \clk_sync_counter_reg[0]_i_4_n_0\
    );
\clk_sync_counter_reg[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(2),
      I2 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I3 => clk_sync_counter_reg0(2),
      O => \clk_sync_counter_reg[0]_i_5_n_0\
    );
\clk_sync_counter_reg[0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(1),
      I2 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I3 => clk_sync_counter_reg0(1),
      O => \clk_sync_counter_reg[0]_i_6_n_0\
    );
\clk_sync_counter_reg[0]_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(0),
      O => \clk_sync_counter_reg[0]_i_7_n_0\
    );
\clk_sync_counter_reg[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0101FF0100000000"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_9_n_0\,
      I1 => \clk_sync_counter_reg[0]_i_10_n_0\,
      I2 => \clk_sync_counter_reg[0]_i_11_n_0\,
      I3 => \clk_sync_counter_reg[0]_i_12_n_0\,
      I4 => clk_sync_counter_reg_reg(17),
      I5 => clk_start_init_i_6_n_0,
      O => \clk_sync_counter_reg[0]_i_8_n_0\
    );
\clk_sync_counter_reg[0]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEBF"
    )
        port map (
      I0 => clk_start_init_i_17_n_0,
      I1 => clk_sync_counter_reg_reg(13),
      I2 => clk_sync_counter_reg_reg(14),
      I3 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[0]_i_9_n_0\
    );
\clk_sync_counter_reg[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF44F4"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I1 => clk_sync_counter_reg0(15),
      I2 => clk_sync_counter_reg_reg(15),
      I3 => clk_start_init_i_5_n_0,
      I4 => \clk_sync_counter_reg[4]_i_6_n_0\,
      O => \clk_sync_counter_reg[12]_i_2_n_0\
    );
\clk_sync_counter_reg[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF44444"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg0(14),
      I3 => \clk_sync_counter_reg[8]_i_6_n_0\,
      I4 => \clk_sync_counter_reg[8]_i_7_n_0\,
      O => \clk_sync_counter_reg[12]_i_3_n_0\
    );
\clk_sync_counter_reg[12]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF44444"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(13),
      I2 => clk_sync_counter_reg0(13),
      I3 => \clk_sync_counter_reg[8]_i_6_n_0\,
      I4 => \clk_sync_counter_reg[8]_i_7_n_0\,
      O => \clk_sync_counter_reg[12]_i_4_n_0\
    );
\clk_sync_counter_reg[12]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF44444"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg0(12),
      I3 => \clk_sync_counter_reg[8]_i_6_n_0\,
      I4 => \clk_sync_counter_reg[8]_i_7_n_0\,
      O => \clk_sync_counter_reg[12]_i_5_n_0\
    );
\clk_sync_counter_reg[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(19),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[16]_i_2_n_0\
    );
\clk_sync_counter_reg[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(18),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[16]_i_3_n_0\
    );
\clk_sync_counter_reg[16]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => \clk_sync_counter_reg[8]_i_7_n_0\,
      I1 => \clk_sync_counter_reg[8]_i_6_n_0\,
      I2 => clk_sync_counter_reg0(17),
      O => \clk_sync_counter_reg[16]_i_4_n_0\
    );
\clk_sync_counter_reg[16]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF44F4"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I1 => clk_sync_counter_reg0(16),
      I2 => clk_sync_counter_reg_reg(16),
      I3 => clk_start_init_i_5_n_0,
      I4 => \clk_sync_counter_reg[4]_i_6_n_0\,
      O => \clk_sync_counter_reg[16]_i_5_n_0\
    );
\clk_sync_counter_reg[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(23),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[20]_i_2_n_0\
    );
\clk_sync_counter_reg[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(22),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[20]_i_3_n_0\
    );
\clk_sync_counter_reg[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(21),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[20]_i_4_n_0\
    );
\clk_sync_counter_reg[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(20),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[20]_i_5_n_0\
    );
\clk_sync_counter_reg[24]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(27),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[24]_i_2_n_0\
    );
\clk_sync_counter_reg[24]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(26),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[24]_i_3_n_0\
    );
\clk_sync_counter_reg[24]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(25),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[24]_i_4_n_0\
    );
\clk_sync_counter_reg[24]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(24),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[24]_i_5_n_0\
    );
\clk_sync_counter_reg[28]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(31),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[28]_i_2_n_0\
    );
\clk_sync_counter_reg[28]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(30),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[28]_i_3_n_0\
    );
\clk_sync_counter_reg[28]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(29),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[28]_i_4_n_0\
    );
\clk_sync_counter_reg[28]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clk_sync_counter_reg0(28),
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[28]_i_5_n_0\
    );
\clk_sync_counter_reg[4]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(11),
      I1 => clk_sync_counter_reg_reg(4),
      I2 => clk_sync_counter_reg_reg(6),
      O => \clk_sync_counter_reg[4]_i_10_n_0\
    );
\clk_sync_counter_reg[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4F4FFF4"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I1 => clk_sync_counter_reg0(7),
      I2 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I3 => clk_sync_counter_reg_reg(7),
      I4 => clk_start_init_i_5_n_0,
      O => \clk_sync_counter_reg[4]_i_2_n_0\
    );
\clk_sync_counter_reg[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(6),
      I2 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I3 => clk_sync_counter_reg0(6),
      O => \clk_sync_counter_reg[4]_i_3_n_0\
    );
\clk_sync_counter_reg[4]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF44F4"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I1 => clk_sync_counter_reg0(5),
      I2 => clk_sync_counter_reg_reg(5),
      I3 => clk_start_init_i_5_n_0,
      I4 => \clk_sync_counter_reg[4]_i_6_n_0\,
      O => \clk_sync_counter_reg[4]_i_4_n_0\
    );
\clk_sync_counter_reg[4]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(4),
      I2 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I3 => clk_sync_counter_reg0(4),
      O => \clk_sync_counter_reg[4]_i_5_n_0\
    );
\clk_sync_counter_reg[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100010000000100"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_9_n_0\,
      I1 => \clk_sync_counter_reg[0]_i_10_n_0\,
      I2 => \clk_sync_counter_reg[0]_i_11_n_0\,
      I3 => clk_start_init_i_6_n_0,
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I5 => \clk_sync_counter_reg[4]_i_8_n_0\,
      O => \clk_sync_counter_reg[4]_i_6_n_0\
    );
\clk_sync_counter_reg[4]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000010000000000"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_9_n_0\,
      I1 => clk_sync_counter_reg_reg(15),
      I2 => clk_sync_counter_reg_reg(16),
      I3 => clk_sync_counter_reg_reg(17),
      I4 => clk_sync_counter_reg_reg(10),
      I5 => clk_sync_counter_reg_reg(12),
      O => \clk_sync_counter_reg[4]_i_7_n_0\
    );
\clk_sync_counter_reg[4]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_10_n_0\,
      I1 => clk_sync_counter_reg_reg(5),
      I2 => clk_sync_counter_reg_reg(7),
      I3 => clk_sync_counter_reg_reg(8),
      I4 => clk_sync_counter_reg_reg(9),
      I5 => clk_start_init_i_17_n_0,
      O => \clk_sync_counter_reg[4]_i_8_n_0\
    );
\clk_sync_counter_reg[4]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(14),
      I1 => clk_sync_counter_reg_reg(13),
      O => \clk_sync_counter_reg[4]_i_9_n_0\
    );
\clk_sync_counter_reg[8]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6FF6"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(12),
      I1 => clk_sync_counter_reg_reg(13),
      I2 => clk_sync_counter_reg_reg(9),
      I3 => clk_sync_counter_reg_reg(10),
      O => \clk_sync_counter_reg[8]_i_10_n_0\
    );
\clk_sync_counter_reg[8]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF6FF6"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(15),
      I1 => clk_sync_counter_reg_reg(16),
      I2 => clk_sync_counter_reg_reg(13),
      I3 => clk_sync_counter_reg_reg(14),
      I4 => \clk_sync_counter_reg[4]_i_10_n_0\,
      I5 => \clk_sync_counter_reg[8]_i_15_n_0\,
      O => \clk_sync_counter_reg[8]_i_11_n_0\
    );
\clk_sync_counter_reg[8]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \clk_sync_counter_reg[8]_i_16_n_0\,
      I1 => \clk_sync_counter_reg[4]_i_9_n_0\,
      I2 => clk_sync_counter_reg_reg(16),
      I3 => clk_sync_counter_reg_reg(15),
      I4 => \clk_sync_counter_reg[8]_i_17_n_0\,
      I5 => \clk_sync_counter_reg[8]_i_18_n_0\,
      O => \clk_sync_counter_reg[8]_i_12_n_0\
    );
\clk_sync_counter_reg[8]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(10),
      I1 => clk_sync_counter_reg_reg(12),
      O => \clk_sync_counter_reg[8]_i_13_n_0\
    );
\clk_sync_counter_reg[8]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F99F"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(17),
      I1 => clk_sync_counter_reg_reg(16),
      I2 => clk_sync_counter_reg_reg(14),
      I3 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[8]_i_14_n_0\
    );
\clk_sync_counter_reg[8]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(10),
      I1 => clk_sync_counter_reg_reg(9),
      I2 => clk_sync_counter_reg_reg(13),
      I3 => clk_sync_counter_reg_reg(12),
      I4 => clk_sync_counter_reg_reg(5),
      I5 => clk_sync_counter_reg_reg(7),
      O => \clk_sync_counter_reg[8]_i_15_n_0\
    );
\clk_sync_counter_reg[8]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(4),
      I1 => clk_sync_counter_reg_reg(3),
      I2 => clk_sync_counter_reg_reg(10),
      I3 => clk_sync_counter_reg_reg(11),
      O => \clk_sync_counter_reg[8]_i_16_n_0\
    );
\clk_sync_counter_reg[8]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(8),
      I1 => clk_sync_counter_reg_reg(9),
      I2 => clk_sync_counter_reg_reg(12),
      I3 => clk_sync_counter_reg_reg(17),
      O => \clk_sync_counter_reg[8]_i_17_n_0\
    );
\clk_sync_counter_reg[8]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(7),
      I1 => clk_sync_counter_reg_reg(5),
      I2 => clk_sync_counter_reg_reg(6),
      I3 => clk_sync_counter_reg_reg(0),
      I4 => clk_sync_counter_reg_reg(1),
      I5 => clk_sync_counter_reg_reg(2),
      O => \clk_sync_counter_reg[8]_i_18_n_0\
    );
\clk_sync_counter_reg[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(11),
      I2 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I3 => clk_sync_counter_reg0(11),
      O => \clk_sync_counter_reg[8]_i_2_n_0\
    );
\clk_sync_counter_reg[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF44F4"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I1 => clk_sync_counter_reg0(10),
      I2 => clk_sync_counter_reg_reg(10),
      I3 => clk_start_init_i_5_n_0,
      I4 => \clk_sync_counter_reg[4]_i_6_n_0\,
      O => \clk_sync_counter_reg[8]_i_3_n_0\
    );
\clk_sync_counter_reg[8]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF44F4"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I1 => clk_sync_counter_reg0(9),
      I2 => clk_sync_counter_reg_reg(9),
      I3 => clk_start_init_i_5_n_0,
      I4 => \clk_sync_counter_reg[4]_i_6_n_0\,
      O => \clk_sync_counter_reg[8]_i_4_n_0\
    );
\clk_sync_counter_reg[8]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF44444"
    )
        port map (
      I0 => clk_start_init_i_5_n_0,
      I1 => clk_sync_counter_reg_reg(8),
      I2 => clk_sync_counter_reg0(8),
      I3 => \clk_sync_counter_reg[8]_i_6_n_0\,
      I4 => \clk_sync_counter_reg[8]_i_7_n_0\,
      O => \clk_sync_counter_reg[8]_i_5_n_0\
    );
\clk_sync_counter_reg[8]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000002000002"
    )
        port map (
      I0 => clk_start_init_i_6_n_0,
      I1 => \clk_sync_counter_reg[8]_i_8_n_0\,
      I2 => \clk_sync_counter_reg[8]_i_9_n_0\,
      I3 => clk_sync_counter_reg_reg(7),
      I4 => clk_sync_counter_reg_reg(5),
      I5 => \clk_sync_counter_reg[8]_i_10_n_0\,
      O => \clk_sync_counter_reg[8]_i_6_n_0\
    );
\clk_sync_counter_reg[8]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFDFDF0FDFDFDFDF"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_12_n_0\,
      I1 => clk_sync_counter_reg_reg(17),
      I2 => clk_start_init_i_6_n_0,
      I3 => \clk_sync_counter_reg[8]_i_8_n_0\,
      I4 => \clk_sync_counter_reg[8]_i_11_n_0\,
      I5 => \clk_sync_counter_reg[8]_i_12_n_0\,
      O => \clk_sync_counter_reg[8]_i_7_n_0\
    );
\clk_sync_counter_reg[8]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFDFEFFF"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(9),
      I1 => clk_start_init_i_17_n_0,
      I2 => \clk_sync_counter_reg[8]_i_13_n_0\,
      I3 => clk_sync_counter_reg_reg(8),
      I4 => clk_sync_counter_reg_reg(7),
      I5 => \clk_sync_counter_reg[8]_i_14_n_0\,
      O => \clk_sync_counter_reg[8]_i_8_n_0\
    );
\clk_sync_counter_reg[8]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BEFFFFBE"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_10_n_0\,
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg_reg(13),
      I3 => clk_sync_counter_reg_reg(16),
      I4 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[8]_i_9_n_0\
    );
\clk_sync_counter_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_7\,
      Q => clk_sync_counter_reg_reg(0),
      R => '0'
    );
\clk_sync_counter_reg_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \clk_sync_counter_reg_reg[0]_i_2_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[0]_i_2_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[0]_i_2_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \clk_sync_counter_reg[0]_i_3_n_0\,
      O(3) => \clk_sync_counter_reg_reg[0]_i_2_n_4\,
      O(2) => \clk_sync_counter_reg_reg[0]_i_2_n_5\,
      O(1) => \clk_sync_counter_reg_reg[0]_i_2_n_6\,
      O(0) => \clk_sync_counter_reg_reg[0]_i_2_n_7\,
      S(3) => \clk_sync_counter_reg[0]_i_4_n_0\,
      S(2) => \clk_sync_counter_reg[0]_i_5_n_0\,
      S(1) => \clk_sync_counter_reg[0]_i_6_n_0\,
      S(0) => \clk_sync_counter_reg[0]_i_7_n_0\
    );
\clk_sync_counter_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(10),
      R => '0'
    );
\clk_sync_counter_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(11),
      R => '0'
    );
\clk_sync_counter_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(12),
      R => '0'
    );
\clk_sync_counter_reg_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[8]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[12]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[12]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[12]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[12]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[12]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[12]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[12]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[12]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[12]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[12]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[12]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(13),
      R => '0'
    );
\clk_sync_counter_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(14),
      R => '0'
    );
\clk_sync_counter_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(15),
      R => '0'
    );
\clk_sync_counter_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(16),
      R => '0'
    );
\clk_sync_counter_reg_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[12]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[16]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[16]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[16]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[16]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[16]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[16]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[16]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[16]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[16]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[16]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[16]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(17),
      R => '0'
    );
\clk_sync_counter_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(18),
      R => '0'
    );
\clk_sync_counter_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(19),
      R => '0'
    );
\clk_sync_counter_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_6\,
      Q => clk_sync_counter_reg_reg(1),
      R => '0'
    );
\clk_sync_counter_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(20),
      R => '0'
    );
\clk_sync_counter_reg_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[16]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[20]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[20]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[20]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[20]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[20]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[20]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[20]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[20]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[20]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[20]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[20]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(21),
      R => '0'
    );
\clk_sync_counter_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(22),
      R => '0'
    );
\clk_sync_counter_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(23),
      R => '0'
    );
\clk_sync_counter_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(24),
      R => '0'
    );
\clk_sync_counter_reg_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[20]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[24]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[24]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[24]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[24]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[24]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[24]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[24]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[24]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[24]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[24]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[24]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(25),
      R => '0'
    );
\clk_sync_counter_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(26),
      R => '0'
    );
\clk_sync_counter_reg_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(27),
      R => '0'
    );
\clk_sync_counter_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(28),
      R => '0'
    );
\clk_sync_counter_reg_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[24]_i_1_n_0\,
      CO(3) => \NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \clk_sync_counter_reg_reg[28]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[28]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[28]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[28]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[28]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[28]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[28]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[28]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[28]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[28]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(29),
      R => '0'
    );
\clk_sync_counter_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_5\,
      Q => clk_sync_counter_reg_reg(2),
      R => '0'
    );
\clk_sync_counter_reg_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(30),
      R => '0'
    );
\clk_sync_counter_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(31),
      R => '0'
    );
\clk_sync_counter_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_4\,
      Q => clk_sync_counter_reg_reg(3),
      R => '0'
    );
\clk_sync_counter_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(4),
      R => '0'
    );
\clk_sync_counter_reg_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[0]_i_2_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[4]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[4]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[4]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[4]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[4]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[4]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[4]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[4]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[4]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[4]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[4]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(5),
      R => '0'
    );
\clk_sync_counter_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(6),
      R => '0'
    );
\clk_sync_counter_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(7),
      R => '0'
    );
\clk_sync_counter_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(8),
      R => '0'
    );
\clk_sync_counter_reg_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[4]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[8]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[8]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[8]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[8]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[8]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[8]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[8]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[8]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[8]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[8]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[8]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(9),
      R => '0'
    );
clk_wizard_main: entity work.design_1_init_clk_0_0_clk_wiz_0
     port map (
      clk_in1 => clk,
      clk_out1 => NLW_clk_wizard_main_clk_out1_UNCONNECTED,
      clk_out2 => clk_8MHz,
      clk_out3 => NLW_clk_wizard_main_clk_out3_UNCONNECTED,
      clk_out4 => NLW_clk_wizard_main_clk_out4_UNCONNECTED,
      locked => sys_clk_locked
    );
is_clk_initialized_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAEA"
    )
        port map (
      I0 => is_clk_initialized_reg,
      I1 => start_ext,
      I2 => clk_start_init_i_5_n_0,
      I3 => clk_start_init_i_3_n_0,
      O => is_clk_initialized_reg_i_1_n_0
    );
is_clk_initialized_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_sync_counter_reg0_0,
      CE => '1',
      D => is_clk_initialized_reg_i_1_n_0,
      Q => is_clk_initialized_reg,
      R => '0'
    );
start_prev_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^clk_start_init_reg_0\,
      I1 => start_ext,
      I2 => start_prev,
      O => start_prev_i_1_n_0
    );
start_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => start_prev_i_1_n_0,
      Q => start_prev,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_init_clk_0_0 is
  port (
    clk : in STD_LOGIC;
    start_ext : in STD_LOGIC;
    clk_sclk : out STD_LOGIC;
    clk_mosi : out STD_LOGIC;
    clk_cs : out STD_LOGIC;
    clk_sync : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_init_clk_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_init_clk_0_0 : entity is "design_1_init_clk_0_0,init_clk,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_init_clk_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_init_clk_0_0 : entity is "init_clk,Vivado 2019.1";
end design_1_init_clk_0_0;

architecture STRUCTURE of design_1_init_clk_0_0 is
begin
inst: entity work.design_1_init_clk_0_0_init_clk
     port map (
      clk => clk,
      clk_cs => clk_cs,
      clk_mosi => clk_mosi,
      clk_sclk => clk_sclk,
      clk_start_init_reg_0 => clk_sync,
      start_ext => start_ext
    );
end STRUCTURE;
