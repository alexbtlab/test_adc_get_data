// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun 23 11:10:01 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/project/test_ADC_GET_DATA2/test_ADC_GET_DATA2.srcs/sources_1/bd/design_1/ip/design_1_init_clk_0_0/design_1_init_clk_0_0_stub.v
// Design      : design_1_init_clk_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "init_clk,Vivado 2019.1" *)
module design_1_init_clk_0_0(clk, start_ext, clk_sclk, clk_mosi, clk_cs, 
  clk_sync)
/* synthesis syn_black_box black_box_pad_pin="clk,start_ext,clk_sclk,clk_mosi,clk_cs,clk_sync" */;
  input clk;
  input start_ext;
  output clk_sclk;
  output clk_mosi;
  output clk_cs;
  output clk_sync;
endmodule
