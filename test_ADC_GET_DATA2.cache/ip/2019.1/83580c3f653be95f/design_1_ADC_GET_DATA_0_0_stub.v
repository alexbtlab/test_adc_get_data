// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun 23 12:24:37 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_GET_DATA_0_0_stub.v
// Design      : design_1_ADC_GET_DATA_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "adc_get_data,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(adc_data, data_ready, data_ready_clk, out_cnt, 
  adc_dax_p, adc_dax_n, adc_dbx_p, adc_dbx_n, adc_dcox_p, adc_dcox_n, fpga_clkx_p, fpga_clkx_n, 
  adc_clkx_p, adc_clkx_n)
/* synthesis syn_black_box black_box_pad_pin="adc_data[17:0],data_ready,data_ready_clk,out_cnt[7:0],adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,adc_dcox_p,adc_dcox_n,fpga_clkx_p,fpga_clkx_n,adc_clkx_p,adc_clkx_n" */;
  output [17:0]adc_data;
  output data_ready;
  output data_ready_clk;
  output [7:0]out_cnt;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input adc_dcox_p;
  input adc_dcox_n;
  input fpga_clkx_p;
  input fpga_clkx_n;
  output adc_clkx_p;
  output adc_clkx_n;
endmodule
