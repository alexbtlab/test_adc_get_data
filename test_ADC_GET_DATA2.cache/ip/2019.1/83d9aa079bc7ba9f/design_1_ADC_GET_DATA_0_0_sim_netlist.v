// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun 23 12:33:24 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_GET_DATA_0_0_sim_netlist.v
// Design      : design_1_ADC_GET_DATA_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data
   (out_cnt,
    adc_clkx_p,
    adc_clkx_n,
    adc_data,
    data_ready,
    data_ready_clk,
    adc_dcox_p,
    adc_dcox_n,
    adc_dax_p,
    adc_dax_n,
    adc_dbx_p,
    adc_dbx_n,
    fpga_clkx_p,
    fpga_clkx_n);
  output [7:0]out_cnt;
  output adc_clkx_p;
  output adc_clkx_n;
  output [17:0]adc_data;
  output data_ready;
  output data_ready_clk;
  input adc_dcox_p;
  input adc_dcox_n;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input fpga_clkx_p;
  input fpga_clkx_n;

  wire adc_clk;
  wire adc_clkx_n;
  wire adc_clkx_p;
  wire \adc_current_clk_num_reg[0]_i_1_n_0 ;
  wire \adc_current_clk_num_reg[1]_i_1_n_0 ;
  wire \adc_current_clk_num_reg[2]_i_1_n_0 ;
  wire \adc_current_clk_num_reg_reg_n_0_[0] ;
  wire \adc_current_clk_num_reg_reg_n_0_[1] ;
  wire \adc_current_clk_num_reg_reg_n_0_[2] ;
  wire \adc_current_strobe_num_reg[0]_i_1_n_0 ;
  wire \adc_current_strobe_num_reg[7]_i_2_n_0 ;
  wire adc_da;
  wire [17:0]adc_data;
  wire [31:3]adc_data_neg1;
  wire [31:3]adc_data_neg2;
  wire \adc_data_neg[10]_i_1_n_0 ;
  wire \adc_data_neg[10]_i_2_n_0 ;
  wire \adc_data_neg[11]_i_1_n_0 ;
  wire \adc_data_neg[14]_i_10_n_0 ;
  wire \adc_data_neg[14]_i_13_n_0 ;
  wire \adc_data_neg[14]_i_14_n_0 ;
  wire \adc_data_neg[14]_i_15_n_0 ;
  wire \adc_data_neg[14]_i_16_n_0 ;
  wire \adc_data_neg[14]_i_17_n_0 ;
  wire \adc_data_neg[14]_i_18_n_0 ;
  wire \adc_data_neg[14]_i_19_n_0 ;
  wire \adc_data_neg[14]_i_1_n_0 ;
  wire \adc_data_neg[14]_i_20_n_0 ;
  wire \adc_data_neg[14]_i_21_n_0 ;
  wire \adc_data_neg[14]_i_22_n_0 ;
  wire \adc_data_neg[14]_i_27_n_0 ;
  wire \adc_data_neg[14]_i_28_n_0 ;
  wire \adc_data_neg[14]_i_29_n_0 ;
  wire \adc_data_neg[14]_i_2_n_0 ;
  wire \adc_data_neg[14]_i_30_n_0 ;
  wire \adc_data_neg[14]_i_31_n_0 ;
  wire \adc_data_neg[14]_i_32_n_0 ;
  wire \adc_data_neg[14]_i_33_n_0 ;
  wire \adc_data_neg[14]_i_34_n_0 ;
  wire \adc_data_neg[14]_i_35_n_0 ;
  wire \adc_data_neg[14]_i_36_n_0 ;
  wire \adc_data_neg[14]_i_37_n_0 ;
  wire \adc_data_neg[14]_i_38_n_0 ;
  wire \adc_data_neg[14]_i_39_n_0 ;
  wire \adc_data_neg[14]_i_3_n_0 ;
  wire \adc_data_neg[14]_i_40_n_0 ;
  wire \adc_data_neg[14]_i_41_n_0 ;
  wire \adc_data_neg[14]_i_42_n_0 ;
  wire \adc_data_neg[14]_i_43_n_0 ;
  wire \adc_data_neg[14]_i_44_n_0 ;
  wire \adc_data_neg[14]_i_45_n_0 ;
  wire \adc_data_neg[14]_i_46_n_0 ;
  wire \adc_data_neg[14]_i_47_n_0 ;
  wire \adc_data_neg[14]_i_4_n_0 ;
  wire \adc_data_neg[14]_i_5_n_0 ;
  wire \adc_data_neg[14]_i_8_n_0 ;
  wire \adc_data_neg[14]_i_9_n_0 ;
  wire \adc_data_neg[15]_i_11_n_0 ;
  wire \adc_data_neg[15]_i_12_n_0 ;
  wire \adc_data_neg[15]_i_13_n_0 ;
  wire \adc_data_neg[15]_i_14_n_0 ;
  wire \adc_data_neg[15]_i_15_n_0 ;
  wire \adc_data_neg[15]_i_16_n_0 ;
  wire \adc_data_neg[15]_i_17_n_0 ;
  wire \adc_data_neg[15]_i_18_n_0 ;
  wire \adc_data_neg[15]_i_19_n_0 ;
  wire \adc_data_neg[15]_i_1_n_0 ;
  wire \adc_data_neg[15]_i_20_n_0 ;
  wire \adc_data_neg[15]_i_21_n_0 ;
  wire \adc_data_neg[15]_i_22_n_0 ;
  wire \adc_data_neg[15]_i_23_n_0 ;
  wire \adc_data_neg[15]_i_24_n_0 ;
  wire \adc_data_neg[15]_i_25_n_0 ;
  wire \adc_data_neg[15]_i_26_n_0 ;
  wire \adc_data_neg[15]_i_27_n_0 ;
  wire \adc_data_neg[15]_i_28_n_0 ;
  wire \adc_data_neg[15]_i_29_n_0 ;
  wire \adc_data_neg[15]_i_2_n_0 ;
  wire \adc_data_neg[15]_i_34_n_0 ;
  wire \adc_data_neg[15]_i_35_n_0 ;
  wire \adc_data_neg[15]_i_36_n_0 ;
  wire \adc_data_neg[15]_i_37_n_0 ;
  wire \adc_data_neg[15]_i_38_n_0 ;
  wire \adc_data_neg[15]_i_39_n_0 ;
  wire \adc_data_neg[15]_i_3_n_0 ;
  wire \adc_data_neg[15]_i_40_n_0 ;
  wire \adc_data_neg[15]_i_41_n_0 ;
  wire \adc_data_neg[15]_i_42_n_0 ;
  wire \adc_data_neg[15]_i_43_n_0 ;
  wire \adc_data_neg[15]_i_44_n_0 ;
  wire \adc_data_neg[15]_i_45_n_0 ;
  wire \adc_data_neg[15]_i_46_n_0 ;
  wire \adc_data_neg[15]_i_47_n_0 ;
  wire \adc_data_neg[15]_i_4_n_0 ;
  wire \adc_data_neg[15]_i_8_n_0 ;
  wire \adc_data_neg[15]_i_9_n_0 ;
  wire \adc_data_neg[2]_i_1_n_0 ;
  wire \adc_data_neg[2]_i_2_n_0 ;
  wire \adc_data_neg[3]_i_1_n_0 ;
  wire \adc_data_neg[6]_i_1_n_0 ;
  wire \adc_data_neg[6]_i_2_n_0 ;
  wire \adc_data_neg[7]_i_1_n_0 ;
  wire \adc_data_neg_reg[14]_i_11_n_0 ;
  wire \adc_data_neg_reg[14]_i_11_n_1 ;
  wire \adc_data_neg_reg[14]_i_11_n_2 ;
  wire \adc_data_neg_reg[14]_i_11_n_3 ;
  wire \adc_data_neg_reg[14]_i_12_n_0 ;
  wire \adc_data_neg_reg[14]_i_12_n_1 ;
  wire \adc_data_neg_reg[14]_i_12_n_2 ;
  wire \adc_data_neg_reg[14]_i_12_n_3 ;
  wire \adc_data_neg_reg[14]_i_23_n_0 ;
  wire \adc_data_neg_reg[14]_i_23_n_1 ;
  wire \adc_data_neg_reg[14]_i_23_n_2 ;
  wire \adc_data_neg_reg[14]_i_23_n_3 ;
  wire \adc_data_neg_reg[14]_i_24_n_0 ;
  wire \adc_data_neg_reg[14]_i_24_n_1 ;
  wire \adc_data_neg_reg[14]_i_24_n_2 ;
  wire \adc_data_neg_reg[14]_i_24_n_3 ;
  wire \adc_data_neg_reg[14]_i_25_n_0 ;
  wire \adc_data_neg_reg[14]_i_25_n_1 ;
  wire \adc_data_neg_reg[14]_i_25_n_2 ;
  wire \adc_data_neg_reg[14]_i_25_n_3 ;
  wire \adc_data_neg_reg[14]_i_6_n_0 ;
  wire \adc_data_neg_reg[14]_i_6_n_1 ;
  wire \adc_data_neg_reg[14]_i_6_n_2 ;
  wire \adc_data_neg_reg[14]_i_6_n_3 ;
  wire \adc_data_neg_reg[14]_i_7_n_0 ;
  wire \adc_data_neg_reg[14]_i_7_n_1 ;
  wire \adc_data_neg_reg[14]_i_7_n_2 ;
  wire \adc_data_neg_reg[14]_i_7_n_3 ;
  wire \adc_data_neg_reg[15]_i_10_n_0 ;
  wire \adc_data_neg_reg[15]_i_10_n_1 ;
  wire \adc_data_neg_reg[15]_i_10_n_2 ;
  wire \adc_data_neg_reg[15]_i_10_n_3 ;
  wire \adc_data_neg_reg[15]_i_30_n_0 ;
  wire \adc_data_neg_reg[15]_i_30_n_1 ;
  wire \adc_data_neg_reg[15]_i_30_n_2 ;
  wire \adc_data_neg_reg[15]_i_30_n_3 ;
  wire \adc_data_neg_reg[15]_i_31_n_0 ;
  wire \adc_data_neg_reg[15]_i_31_n_1 ;
  wire \adc_data_neg_reg[15]_i_31_n_2 ;
  wire \adc_data_neg_reg[15]_i_31_n_3 ;
  wire \adc_data_neg_reg[15]_i_32_n_0 ;
  wire \adc_data_neg_reg[15]_i_32_n_1 ;
  wire \adc_data_neg_reg[15]_i_32_n_2 ;
  wire \adc_data_neg_reg[15]_i_32_n_3 ;
  wire \adc_data_neg_reg[15]_i_5_n_0 ;
  wire \adc_data_neg_reg[15]_i_5_n_1 ;
  wire \adc_data_neg_reg[15]_i_5_n_2 ;
  wire \adc_data_neg_reg[15]_i_5_n_3 ;
  wire \adc_data_neg_reg[15]_i_6_n_0 ;
  wire \adc_data_neg_reg[15]_i_6_n_1 ;
  wire \adc_data_neg_reg[15]_i_6_n_2 ;
  wire \adc_data_neg_reg[15]_i_6_n_3 ;
  wire \adc_data_neg_reg[15]_i_7_n_0 ;
  wire \adc_data_neg_reg[15]_i_7_n_1 ;
  wire \adc_data_neg_reg[15]_i_7_n_2 ;
  wire \adc_data_neg_reg[15]_i_7_n_3 ;
  wire adc_data_pos0;
  wire \adc_data_pos[0]_i_1_n_0 ;
  wire \adc_data_pos[0]_i_2_n_0 ;
  wire \adc_data_pos[12]_i_1_n_0 ;
  wire \adc_data_pos[12]_i_2_n_0 ;
  wire \adc_data_pos[13]_i_1_n_0 ;
  wire \adc_data_pos[13]_i_2_n_0 ;
  wire \adc_data_pos[16]_i_1_n_0 ;
  wire \adc_data_pos[16]_i_2_n_0 ;
  wire \adc_data_pos[16]_i_3_n_0 ;
  wire \adc_data_pos[17]_i_1_n_0 ;
  wire \adc_data_pos[17]_i_3_n_0 ;
  wire \adc_data_pos[1]_i_1_n_0 ;
  wire \adc_data_pos[1]_i_2_n_0 ;
  wire \adc_data_pos[4]_i_1_n_0 ;
  wire \adc_data_pos[4]_i_2_n_0 ;
  wire \adc_data_pos[5]_i_1_n_0 ;
  wire \adc_data_pos[5]_i_2_n_0 ;
  wire \adc_data_pos[8]_i_1_n_0 ;
  wire \adc_data_pos[8]_i_2_n_0 ;
  wire \adc_data_pos[9]_i_1_n_0 ;
  wire \adc_data_pos[9]_i_2_n_0 ;
  wire adc_dax_n;
  wire adc_dax_p;
  wire adc_db;
  wire adc_dbx_n;
  wire adc_dbx_p;
  wire adc_dcox_n;
  wire adc_dcox_p;
  wire adc_reading_allowed_neg_reg_i_1_n_0;
  wire adc_reading_allowed_neg_reg_reg_n_0;
  wire adc_reading_allowed_reg1__1;
  wire adc_reading_allowed_reg_i_1_n_0;
  wire adc_reading_allowed_reg_reg_n_0;
  wire clear;
  wire clk_adc;
  wire clk_adc_delay;
  wire clk_locked;
  wire data_ready;
  wire data_ready_clk;
  wire data_ready_clk_INST_0_i_1_n_0;
  wire fpga_clk;
  wire \fpga_clk_counter[2]_i_1_n_0 ;
  wire [3:0]fpga_clk_counter_reg;
  wire fpga_clkx_n;
  wire fpga_clkx_p;
  wire is_data_ready_i_1_n_0;
  wire is_it_first_strobe;
  wire is_it_first_strobe_i_1_n_0;
  wire is_strobe_clk_active;
  wire is_strobe_clk_active_neg;
  wire [7:0]out_cnt;
  wire [7:2]p_0_in;
  wire [3:0]p_0_in__0;
  wire tfirstclk_passed;
  wire tfirstclk_passed_i_1_n_0;
  wire tfirstclk_passed_prev;
  wire tfirstclk_passed_prev0;
  wire NLW_IBUFDS_adc_dco_O_UNCONNECTED;
  wire [3:0]\NLW_adc_data_neg_reg[14]_i_26_CO_UNCONNECTED ;
  wire [3:1]\NLW_adc_data_neg_reg[14]_i_26_O_UNCONNECTED ;
  wire [3:0]\NLW_adc_data_neg_reg[15]_i_33_CO_UNCONNECTED ;
  wire [3:1]\NLW_adc_data_neg_reg[15]_i_33_O_UNCONNECTED ;
  wire [0:0]\NLW_adc_data_neg_reg[15]_i_6_O_UNCONNECTED ;

  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_da
       (.I(adc_dax_p),
        .IB(adc_dax_n),
        .O(adc_da));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_db
       (.I(adc_dbx_p),
        .IB(adc_dbx_n),
        .O(adc_db));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_dco
       (.I(adc_dcox_p),
        .IB(adc_dcox_n),
        .O(NLW_IBUFDS_adc_dco_O_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_fpga_clk
       (.I(fpga_clkx_p),
        .IB(fpga_clkx_n),
        .O(fpga_clk));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_adc_clk
       (.I(adc_clk),
        .O(adc_clkx_p),
        .OB(adc_clkx_n));
  LUT3 #(
    .INIT(8'h80)) 
    OBUFDS_adc_clk_i_1
       (.I0(adc_reading_allowed_reg_reg_n_0),
        .I1(clk_adc),
        .I2(adc_reading_allowed_neg_reg_reg_n_0),
        .O(adc_clk));
  LUT4 #(
    .INIT(16'hB5AA)) 
    \adc_current_clk_num_reg[0]_i_1 
       (.I0(\adc_current_clk_num_reg_reg_n_0_[0] ),
        .I1(\adc_current_clk_num_reg_reg_n_0_[1] ),
        .I2(\adc_current_clk_num_reg_reg_n_0_[2] ),
        .I3(adc_reading_allowed_reg_reg_n_0),
        .O(\adc_current_clk_num_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hC6CC)) 
    \adc_current_clk_num_reg[1]_i_1 
       (.I0(\adc_current_clk_num_reg_reg_n_0_[0] ),
        .I1(\adc_current_clk_num_reg_reg_n_0_[1] ),
        .I2(\adc_current_clk_num_reg_reg_n_0_[2] ),
        .I3(adc_reading_allowed_reg_reg_n_0),
        .O(\adc_current_clk_num_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hD8F0)) 
    \adc_current_clk_num_reg[2]_i_1 
       (.I0(\adc_current_clk_num_reg_reg_n_0_[0] ),
        .I1(\adc_current_clk_num_reg_reg_n_0_[1] ),
        .I2(\adc_current_clk_num_reg_reg_n_0_[2] ),
        .I3(adc_reading_allowed_reg_reg_n_0),
        .O(\adc_current_clk_num_reg[2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \adc_current_clk_num_reg[2]_i_2 
       (.I0(clk_adc),
        .I1(clk_locked),
        .O(tfirstclk_passed_prev0));
  FDRE #(
    .INIT(1'b1)) 
    \adc_current_clk_num_reg_reg[0] 
       (.C(tfirstclk_passed_prev0),
        .CE(1'b1),
        .D(\adc_current_clk_num_reg[0]_i_1_n_0 ),
        .Q(\adc_current_clk_num_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_clk_num_reg_reg[1] 
       (.C(tfirstclk_passed_prev0),
        .CE(1'b1),
        .D(\adc_current_clk_num_reg[1]_i_1_n_0 ),
        .Q(\adc_current_clk_num_reg_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_clk_num_reg_reg[2] 
       (.C(tfirstclk_passed_prev0),
        .CE(1'b1),
        .D(\adc_current_clk_num_reg[2]_i_1_n_0 ),
        .Q(\adc_current_clk_num_reg_reg_n_0_[2] ),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \adc_current_strobe_num_reg[0]_i_1 
       (.I0(out_cnt[0]),
        .O(\adc_current_strobe_num_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \adc_current_strobe_num_reg[1]_i_1 
       (.I0(out_cnt[0]),
        .I1(out_cnt[1]),
        .O(adc_data_neg2[3]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \adc_current_strobe_num_reg[2]_i_1 
       (.I0(out_cnt[0]),
        .I1(out_cnt[1]),
        .I2(out_cnt[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \adc_current_strobe_num_reg[3]_i_1 
       (.I0(out_cnt[1]),
        .I1(out_cnt[2]),
        .I2(out_cnt[0]),
        .I3(out_cnt[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \adc_current_strobe_num_reg[4]_i_1 
       (.I0(out_cnt[3]),
        .I1(out_cnt[0]),
        .I2(out_cnt[2]),
        .I3(out_cnt[1]),
        .I4(out_cnt[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \adc_current_strobe_num_reg[5]_i_1 
       (.I0(out_cnt[1]),
        .I1(out_cnt[2]),
        .I2(out_cnt[0]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[5]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \adc_current_strobe_num_reg[6]_i_1 
       (.I0(\adc_current_strobe_num_reg[7]_i_2_n_0 ),
        .I1(out_cnt[6]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \adc_current_strobe_num_reg[7]_i_1 
       (.I0(out_cnt[6]),
        .I1(\adc_current_strobe_num_reg[7]_i_2_n_0 ),
        .I2(out_cnt[7]),
        .O(p_0_in[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \adc_current_strobe_num_reg[7]_i_2 
       (.I0(out_cnt[1]),
        .I1(out_cnt[2]),
        .I2(out_cnt[0]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[5]),
        .O(\adc_current_strobe_num_reg[7]_i_2_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \adc_current_strobe_num_reg_reg[0] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_current_strobe_num_reg[0]_i_1_n_0 ),
        .Q(out_cnt[0]),
        .S(is_it_first_strobe));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_strobe_num_reg_reg[1] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(adc_data_neg2[3]),
        .Q(out_cnt[1]),
        .R(is_it_first_strobe));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_strobe_num_reg_reg[2] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(out_cnt[2]),
        .R(is_it_first_strobe));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_strobe_num_reg_reg[3] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(out_cnt[3]),
        .R(is_it_first_strobe));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_strobe_num_reg_reg[4] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(out_cnt[4]),
        .R(is_it_first_strobe));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_strobe_num_reg_reg[5] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(out_cnt[5]),
        .R(is_it_first_strobe));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_strobe_num_reg_reg[6] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(out_cnt[6]),
        .R(is_it_first_strobe));
  FDRE #(
    .INIT(1'b0)) 
    \adc_current_strobe_num_reg_reg[7] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(out_cnt[7]),
        .R(is_it_first_strobe));
  LUT4 #(
    .INIT(16'hFB08)) 
    \adc_data_neg[10]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_neg[14]_i_2_n_0 ),
        .I2(\adc_data_neg[10]_i_2_n_0 ),
        .I3(adc_data[10]),
        .O(\adc_data_neg[10]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFDFF)) 
    \adc_data_neg[10]_i_2 
       (.I0(\adc_data_neg[14]_i_4_n_0 ),
        .I1(\adc_data_neg[14]_i_5_n_0 ),
        .I2(out_cnt[0]),
        .I3(adc_data_neg1[3]),
        .O(\adc_data_neg[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \adc_data_neg[11]_i_1 
       (.I0(adc_da),
        .I1(out_cnt[0]),
        .I2(out_cnt[1]),
        .I3(\adc_data_neg[15]_i_2_n_0 ),
        .I4(adc_data[11]),
        .O(\adc_data_neg[11]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \adc_data_neg[14]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_neg[14]_i_2_n_0 ),
        .I2(\adc_data_neg[14]_i_3_n_0 ),
        .I3(adc_data[14]),
        .O(\adc_data_neg[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \adc_data_neg[14]_i_10 
       (.I0(adc_data_neg1[26]),
        .I1(adc_data_neg1[27]),
        .I2(adc_data_neg1[28]),
        .I3(adc_data_neg1[29]),
        .I4(adc_data_neg1[31]),
        .I5(adc_data_neg1[30]),
        .O(\adc_data_neg[14]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \adc_data_neg[14]_i_13 
       (.I0(adc_data_neg1[8]),
        .I1(adc_data_neg1[11]),
        .I2(adc_data_neg1[5]),
        .I3(adc_data_neg1[9]),
        .O(\adc_data_neg[14]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \adc_data_neg[14]_i_14 
       (.I0(adc_data_neg1[12]),
        .I1(adc_data_neg1[15]),
        .I2(adc_data_neg1[10]),
        .I3(adc_data_neg1[13]),
        .O(\adc_data_neg[14]_i_14_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \adc_data_neg[14]_i_15 
       (.I0(out_cnt[3]),
        .I1(out_cnt[0]),
        .I2(out_cnt[1]),
        .I3(out_cnt[2]),
        .I4(out_cnt[4]),
        .O(\adc_data_neg[14]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'h01FE)) 
    \adc_data_neg[14]_i_16 
       (.I0(out_cnt[2]),
        .I1(out_cnt[1]),
        .I2(out_cnt[0]),
        .I3(out_cnt[3]),
        .O(\adc_data_neg[14]_i_16_n_0 ));
  LUT3 #(
    .INIT(8'hA9)) 
    \adc_data_neg[14]_i_17 
       (.I0(out_cnt[2]),
        .I1(out_cnt[1]),
        .I2(out_cnt[0]),
        .O(\adc_data_neg[14]_i_17_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \adc_data_neg[14]_i_18 
       (.I0(out_cnt[0]),
        .I1(out_cnt[1]),
        .O(\adc_data_neg[14]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_19 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h001F)) 
    \adc_data_neg[14]_i_2 
       (.I0(out_cnt[0]),
        .I1(out_cnt[1]),
        .I2(out_cnt[2]),
        .I3(data_ready_clk_INST_0_i_1_n_0),
        .O(\adc_data_neg[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_20 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_21 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_22 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_27 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \adc_data_neg[14]_i_28 
       (.I0(out_cnt[5]),
        .I1(out_cnt[3]),
        .I2(out_cnt[4]),
        .I3(out_cnt[6]),
        .I4(\adc_data_neg[15]_i_34_n_0 ),
        .I5(out_cnt[7]),
        .O(\adc_data_neg[14]_i_28_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \adc_data_neg[14]_i_29 
       (.I0(out_cnt[4]),
        .I1(out_cnt[3]),
        .I2(out_cnt[5]),
        .I3(\adc_data_neg[15]_i_34_n_0 ),
        .I4(out_cnt[6]),
        .O(\adc_data_neg[14]_i_29_n_0 ));
  LUT4 #(
    .INIT(16'hF7FF)) 
    \adc_data_neg[14]_i_3 
       (.I0(out_cnt[0]),
        .I1(\adc_data_neg[14]_i_4_n_0 ),
        .I2(\adc_data_neg[14]_i_5_n_0 ),
        .I3(adc_data_neg1[3]),
        .O(\adc_data_neg[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \adc_data_neg[14]_i_30 
       (.I0(out_cnt[3]),
        .I1(out_cnt[4]),
        .I2(out_cnt[0]),
        .I3(out_cnt[1]),
        .I4(out_cnt[2]),
        .I5(out_cnt[5]),
        .O(\adc_data_neg[14]_i_30_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_31 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_31_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_32 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_32_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_33 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_33_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_34 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_35 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_35_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_36 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_36_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_37 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_37_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_38 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_38_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_39 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_39_n_0 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \adc_data_neg[14]_i_4 
       (.I0(adc_data_neg1[17]),
        .I1(adc_data_neg1[16]),
        .I2(adc_data_neg1[4]),
        .I3(\adc_data_neg[14]_i_8_n_0 ),
        .I4(\adc_data_neg[14]_i_9_n_0 ),
        .I5(\adc_data_neg[14]_i_10_n_0 ),
        .O(\adc_data_neg[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_40 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_41 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_42 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_42_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_43 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_44 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_44_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_45 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_45_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_46 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_46_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[14]_i_47 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[14]_i_47_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \adc_data_neg[14]_i_5 
       (.I0(adc_data_neg1[7]),
        .I1(adc_data_neg1[6]),
        .I2(adc_data_neg1[14]),
        .I3(\adc_data_neg[14]_i_13_n_0 ),
        .I4(\adc_data_neg[14]_i_14_n_0 ),
        .O(\adc_data_neg[14]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \adc_data_neg[14]_i_8 
       (.I0(adc_data_neg1[21]),
        .I1(adc_data_neg1[20]),
        .I2(adc_data_neg1[19]),
        .I3(adc_data_neg1[18]),
        .O(\adc_data_neg[14]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \adc_data_neg[14]_i_9 
       (.I0(adc_data_neg1[25]),
        .I1(adc_data_neg1[24]),
        .I2(adc_data_neg1[23]),
        .I3(adc_data_neg1[22]),
        .O(\adc_data_neg[14]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hFBFF0800)) 
    \adc_data_neg[15]_i_1 
       (.I0(adc_da),
        .I1(out_cnt[0]),
        .I2(out_cnt[1]),
        .I3(\adc_data_neg[15]_i_2_n_0 ),
        .I4(adc_data[15]),
        .O(\adc_data_neg[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \adc_data_neg[15]_i_11 
       (.I0(adc_data_neg2[21]),
        .I1(adc_data_neg2[20]),
        .I2(adc_data_neg2[19]),
        .I3(adc_data_neg2[18]),
        .O(\adc_data_neg[15]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \adc_data_neg[15]_i_12 
       (.I0(adc_data_neg2[25]),
        .I1(adc_data_neg2[24]),
        .I2(adc_data_neg2[23]),
        .I3(adc_data_neg2[22]),
        .O(\adc_data_neg[15]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \adc_data_neg[15]_i_13 
       (.I0(adc_data_neg2[26]),
        .I1(adc_data_neg2[27]),
        .I2(adc_data_neg2[28]),
        .I3(adc_data_neg2[29]),
        .I4(adc_data_neg2[31]),
        .I5(adc_data_neg2[30]),
        .O(\adc_data_neg[15]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_14 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \adc_data_neg[15]_i_15 
       (.I0(out_cnt[5]),
        .I1(out_cnt[3]),
        .I2(out_cnt[4]),
        .I3(out_cnt[6]),
        .I4(\adc_data_neg[15]_i_34_n_0 ),
        .I5(out_cnt[7]),
        .O(\adc_data_neg[15]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \adc_data_neg[15]_i_16 
       (.I0(out_cnt[4]),
        .I1(out_cnt[3]),
        .I2(out_cnt[5]),
        .I3(\adc_data_neg[15]_i_34_n_0 ),
        .I4(out_cnt[6]),
        .O(\adc_data_neg[15]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \adc_data_neg[15]_i_17 
       (.I0(out_cnt[3]),
        .I1(out_cnt[4]),
        .I2(out_cnt[0]),
        .I3(out_cnt[1]),
        .I4(out_cnt[2]),
        .I5(out_cnt[5]),
        .O(\adc_data_neg[15]_i_17_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \adc_data_neg[15]_i_18 
       (.I0(out_cnt[3]),
        .I1(out_cnt[0]),
        .I2(out_cnt[1]),
        .I3(out_cnt[2]),
        .I4(out_cnt[4]),
        .O(\adc_data_neg[15]_i_18_n_0 ));
  LUT4 #(
    .INIT(16'h01FE)) 
    \adc_data_neg[15]_i_19 
       (.I0(out_cnt[2]),
        .I1(out_cnt[1]),
        .I2(out_cnt[0]),
        .I3(out_cnt[3]),
        .O(\adc_data_neg[15]_i_19_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    \adc_data_neg[15]_i_2 
       (.I0(\adc_data_neg[14]_i_2_n_0 ),
        .I1(\adc_data_neg[15]_i_3_n_0 ),
        .I2(\adc_data_neg[15]_i_4_n_0 ),
        .O(\adc_data_neg[15]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hA9)) 
    \adc_data_neg[15]_i_20 
       (.I0(out_cnt[2]),
        .I1(out_cnt[1]),
        .I2(out_cnt[0]),
        .O(\adc_data_neg[15]_i_20_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \adc_data_neg[15]_i_21 
       (.I0(out_cnt[0]),
        .I1(out_cnt[1]),
        .O(\adc_data_neg[15]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_22 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_23 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_24 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_25 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_26 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_27 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_28 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_29 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_29_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \adc_data_neg[15]_i_3 
       (.I0(adc_data_neg2[7]),
        .I1(adc_data_neg2[6]),
        .I2(adc_data_neg2[14]),
        .I3(\adc_data_neg[15]_i_8_n_0 ),
        .I4(\adc_data_neg[15]_i_9_n_0 ),
        .O(\adc_data_neg[15]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \adc_data_neg[15]_i_34 
       (.I0(out_cnt[0]),
        .I1(out_cnt[1]),
        .I2(out_cnt[2]),
        .O(\adc_data_neg[15]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_35 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_35_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_36 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_36_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_37 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_37_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_38 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_38_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_39 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_39_n_0 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \adc_data_neg[15]_i_4 
       (.I0(adc_data_neg2[17]),
        .I1(adc_data_neg2[16]),
        .I2(adc_data_neg2[4]),
        .I3(\adc_data_neg[15]_i_11_n_0 ),
        .I4(\adc_data_neg[15]_i_12_n_0 ),
        .I5(\adc_data_neg[15]_i_13_n_0 ),
        .O(\adc_data_neg[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_40 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_41 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_42 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_42_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_43 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_44 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_44_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_45 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_45_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_46 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_46_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \adc_data_neg[15]_i_47 
       (.I0(\adc_data_neg[15]_i_34_n_0 ),
        .I1(out_cnt[7]),
        .I2(out_cnt[5]),
        .I3(out_cnt[3]),
        .I4(out_cnt[4]),
        .I5(out_cnt[6]),
        .O(\adc_data_neg[15]_i_47_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \adc_data_neg[15]_i_8 
       (.I0(adc_data_neg2[8]),
        .I1(adc_data_neg2[11]),
        .I2(adc_data_neg2[5]),
        .I3(adc_data_neg2[9]),
        .O(\adc_data_neg[15]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \adc_data_neg[15]_i_9 
       (.I0(adc_data_neg2[12]),
        .I1(adc_data_neg2[15]),
        .I2(adc_data_neg2[10]),
        .I3(adc_data_neg2[13]),
        .O(\adc_data_neg[15]_i_9_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \adc_data_neg[2]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_neg[14]_i_2_n_0 ),
        .I2(\adc_data_neg[2]_i_2_n_0 ),
        .I3(adc_data[2]),
        .O(\adc_data_neg[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \adc_data_neg[2]_i_2 
       (.I0(\adc_data_neg[14]_i_4_n_0 ),
        .I1(\adc_data_neg[14]_i_5_n_0 ),
        .I2(out_cnt[0]),
        .I3(adc_data_neg1[3]),
        .O(\adc_data_neg[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFEFF0200)) 
    \adc_data_neg[3]_i_1 
       (.I0(adc_da),
        .I1(out_cnt[1]),
        .I2(out_cnt[0]),
        .I3(\adc_data_neg[15]_i_2_n_0 ),
        .I4(adc_data[3]),
        .O(\adc_data_neg[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \adc_data_neg[6]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_neg[14]_i_2_n_0 ),
        .I2(\adc_data_neg[6]_i_2_n_0 ),
        .I3(adc_data[6]),
        .O(\adc_data_neg[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFF7)) 
    \adc_data_neg[6]_i_2 
       (.I0(out_cnt[0]),
        .I1(\adc_data_neg[14]_i_4_n_0 ),
        .I2(\adc_data_neg[14]_i_5_n_0 ),
        .I3(adc_data_neg1[3]),
        .O(\adc_data_neg[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFF8000)) 
    \adc_data_neg[7]_i_1 
       (.I0(adc_da),
        .I1(out_cnt[0]),
        .I2(out_cnt[1]),
        .I3(\adc_data_neg[15]_i_2_n_0 ),
        .I4(adc_data[7]),
        .O(\adc_data_neg[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_neg_reg[10] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_neg[10]_i_1_n_0 ),
        .Q(adc_data[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_neg_reg[11] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_neg[11]_i_1_n_0 ),
        .Q(adc_data[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_neg_reg[14] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_neg[14]_i_1_n_0 ),
        .Q(adc_data[14]),
        .R(1'b0));
  CARRY4 \adc_data_neg_reg[14]_i_11 
       (.CI(\adc_data_neg_reg[14]_i_6_n_0 ),
        .CO({\adc_data_neg_reg[14]_i_11_n_0 ,\adc_data_neg_reg[14]_i_11_n_1 ,\adc_data_neg_reg[14]_i_11_n_2 ,\adc_data_neg_reg[14]_i_11_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg1[10:7]),
        .S({\adc_data_neg[14]_i_27_n_0 ,\adc_data_neg[14]_i_28_n_0 ,\adc_data_neg[14]_i_29_n_0 ,\adc_data_neg[14]_i_30_n_0 }));
  CARRY4 \adc_data_neg_reg[14]_i_12 
       (.CI(\adc_data_neg_reg[14]_i_11_n_0 ),
        .CO({\adc_data_neg_reg[14]_i_12_n_0 ,\adc_data_neg_reg[14]_i_12_n_1 ,\adc_data_neg_reg[14]_i_12_n_2 ,\adc_data_neg_reg[14]_i_12_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg1[14:11]),
        .S({\adc_data_neg[14]_i_31_n_0 ,\adc_data_neg[14]_i_32_n_0 ,\adc_data_neg[14]_i_33_n_0 ,\adc_data_neg[14]_i_34_n_0 }));
  CARRY4 \adc_data_neg_reg[14]_i_23 
       (.CI(\adc_data_neg_reg[14]_i_7_n_0 ),
        .CO({\adc_data_neg_reg[14]_i_23_n_0 ,\adc_data_neg_reg[14]_i_23_n_1 ,\adc_data_neg_reg[14]_i_23_n_2 ,\adc_data_neg_reg[14]_i_23_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg1[22:19]),
        .S({\adc_data_neg[14]_i_35_n_0 ,\adc_data_neg[14]_i_36_n_0 ,\adc_data_neg[14]_i_37_n_0 ,\adc_data_neg[14]_i_38_n_0 }));
  CARRY4 \adc_data_neg_reg[14]_i_24 
       (.CI(\adc_data_neg_reg[14]_i_23_n_0 ),
        .CO({\adc_data_neg_reg[14]_i_24_n_0 ,\adc_data_neg_reg[14]_i_24_n_1 ,\adc_data_neg_reg[14]_i_24_n_2 ,\adc_data_neg_reg[14]_i_24_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg1[26:23]),
        .S({\adc_data_neg[14]_i_39_n_0 ,\adc_data_neg[14]_i_40_n_0 ,\adc_data_neg[14]_i_41_n_0 ,\adc_data_neg[14]_i_42_n_0 }));
  CARRY4 \adc_data_neg_reg[14]_i_25 
       (.CI(\adc_data_neg_reg[14]_i_24_n_0 ),
        .CO({\adc_data_neg_reg[14]_i_25_n_0 ,\adc_data_neg_reg[14]_i_25_n_1 ,\adc_data_neg_reg[14]_i_25_n_2 ,\adc_data_neg_reg[14]_i_25_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg1[30:27]),
        .S({\adc_data_neg[14]_i_43_n_0 ,\adc_data_neg[14]_i_44_n_0 ,\adc_data_neg[14]_i_45_n_0 ,\adc_data_neg[14]_i_46_n_0 }));
  CARRY4 \adc_data_neg_reg[14]_i_26 
       (.CI(\adc_data_neg_reg[14]_i_25_n_0 ),
        .CO(\NLW_adc_data_neg_reg[14]_i_26_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_adc_data_neg_reg[14]_i_26_O_UNCONNECTED [3:1],adc_data_neg1[31]}),
        .S({1'b0,1'b0,1'b0,\adc_data_neg[14]_i_47_n_0 }));
  CARRY4 \adc_data_neg_reg[14]_i_6 
       (.CI(1'b0),
        .CO({\adc_data_neg_reg[14]_i_6_n_0 ,\adc_data_neg_reg[14]_i_6_n_1 ,\adc_data_neg_reg[14]_i_6_n_2 ,\adc_data_neg_reg[14]_i_6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O(adc_data_neg1[6:3]),
        .S({\adc_data_neg[14]_i_15_n_0 ,\adc_data_neg[14]_i_16_n_0 ,\adc_data_neg[14]_i_17_n_0 ,\adc_data_neg[14]_i_18_n_0 }));
  CARRY4 \adc_data_neg_reg[14]_i_7 
       (.CI(\adc_data_neg_reg[14]_i_12_n_0 ),
        .CO({\adc_data_neg_reg[14]_i_7_n_0 ,\adc_data_neg_reg[14]_i_7_n_1 ,\adc_data_neg_reg[14]_i_7_n_2 ,\adc_data_neg_reg[14]_i_7_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg1[18:15]),
        .S({\adc_data_neg[14]_i_19_n_0 ,\adc_data_neg[14]_i_20_n_0 ,\adc_data_neg[14]_i_21_n_0 ,\adc_data_neg[14]_i_22_n_0 }));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_neg_reg[15] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_neg[15]_i_1_n_0 ),
        .Q(adc_data[15]),
        .R(1'b0));
  CARRY4 \adc_data_neg_reg[15]_i_10 
       (.CI(\adc_data_neg_reg[15]_i_7_n_0 ),
        .CO({\adc_data_neg_reg[15]_i_10_n_0 ,\adc_data_neg_reg[15]_i_10_n_1 ,\adc_data_neg_reg[15]_i_10_n_2 ,\adc_data_neg_reg[15]_i_10_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg2[18:15]),
        .S({\adc_data_neg[15]_i_26_n_0 ,\adc_data_neg[15]_i_27_n_0 ,\adc_data_neg[15]_i_28_n_0 ,\adc_data_neg[15]_i_29_n_0 }));
  CARRY4 \adc_data_neg_reg[15]_i_30 
       (.CI(\adc_data_neg_reg[15]_i_10_n_0 ),
        .CO({\adc_data_neg_reg[15]_i_30_n_0 ,\adc_data_neg_reg[15]_i_30_n_1 ,\adc_data_neg_reg[15]_i_30_n_2 ,\adc_data_neg_reg[15]_i_30_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg2[22:19]),
        .S({\adc_data_neg[15]_i_35_n_0 ,\adc_data_neg[15]_i_36_n_0 ,\adc_data_neg[15]_i_37_n_0 ,\adc_data_neg[15]_i_38_n_0 }));
  CARRY4 \adc_data_neg_reg[15]_i_31 
       (.CI(\adc_data_neg_reg[15]_i_30_n_0 ),
        .CO({\adc_data_neg_reg[15]_i_31_n_0 ,\adc_data_neg_reg[15]_i_31_n_1 ,\adc_data_neg_reg[15]_i_31_n_2 ,\adc_data_neg_reg[15]_i_31_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg2[26:23]),
        .S({\adc_data_neg[15]_i_39_n_0 ,\adc_data_neg[15]_i_40_n_0 ,\adc_data_neg[15]_i_41_n_0 ,\adc_data_neg[15]_i_42_n_0 }));
  CARRY4 \adc_data_neg_reg[15]_i_32 
       (.CI(\adc_data_neg_reg[15]_i_31_n_0 ),
        .CO({\adc_data_neg_reg[15]_i_32_n_0 ,\adc_data_neg_reg[15]_i_32_n_1 ,\adc_data_neg_reg[15]_i_32_n_2 ,\adc_data_neg_reg[15]_i_32_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg2[30:27]),
        .S({\adc_data_neg[15]_i_43_n_0 ,\adc_data_neg[15]_i_44_n_0 ,\adc_data_neg[15]_i_45_n_0 ,\adc_data_neg[15]_i_46_n_0 }));
  CARRY4 \adc_data_neg_reg[15]_i_33 
       (.CI(\adc_data_neg_reg[15]_i_32_n_0 ),
        .CO(\NLW_adc_data_neg_reg[15]_i_33_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_adc_data_neg_reg[15]_i_33_O_UNCONNECTED [3:1],adc_data_neg2[31]}),
        .S({1'b0,1'b0,1'b0,\adc_data_neg[15]_i_47_n_0 }));
  CARRY4 \adc_data_neg_reg[15]_i_5 
       (.CI(\adc_data_neg_reg[15]_i_6_n_0 ),
        .CO({\adc_data_neg_reg[15]_i_5_n_0 ,\adc_data_neg_reg[15]_i_5_n_1 ,\adc_data_neg_reg[15]_i_5_n_2 ,\adc_data_neg_reg[15]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg2[10:7]),
        .S({\adc_data_neg[15]_i_14_n_0 ,\adc_data_neg[15]_i_15_n_0 ,\adc_data_neg[15]_i_16_n_0 ,\adc_data_neg[15]_i_17_n_0 }));
  CARRY4 \adc_data_neg_reg[15]_i_6 
       (.CI(1'b0),
        .CO({\adc_data_neg_reg[15]_i_6_n_0 ,\adc_data_neg_reg[15]_i_6_n_1 ,\adc_data_neg_reg[15]_i_6_n_2 ,\adc_data_neg_reg[15]_i_6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b0}),
        .O({adc_data_neg2[6:4],\NLW_adc_data_neg_reg[15]_i_6_O_UNCONNECTED [0]}),
        .S({\adc_data_neg[15]_i_18_n_0 ,\adc_data_neg[15]_i_19_n_0 ,\adc_data_neg[15]_i_20_n_0 ,\adc_data_neg[15]_i_21_n_0 }));
  CARRY4 \adc_data_neg_reg[15]_i_7 
       (.CI(\adc_data_neg_reg[15]_i_5_n_0 ),
        .CO({\adc_data_neg_reg[15]_i_7_n_0 ,\adc_data_neg_reg[15]_i_7_n_1 ,\adc_data_neg_reg[15]_i_7_n_2 ,\adc_data_neg_reg[15]_i_7_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(adc_data_neg2[14:11]),
        .S({\adc_data_neg[15]_i_22_n_0 ,\adc_data_neg[15]_i_23_n_0 ,\adc_data_neg[15]_i_24_n_0 ,\adc_data_neg[15]_i_25_n_0 }));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_neg_reg[2] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_neg[2]_i_1_n_0 ),
        .Q(adc_data[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_neg_reg[3] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_neg[3]_i_1_n_0 ),
        .Q(adc_data[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_neg_reg[6] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_neg[6]_i_1_n_0 ),
        .Q(adc_data[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_neg_reg[7] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_neg[7]_i_1_n_0 ),
        .Q(adc_data[7]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \adc_data_pos[0]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_pos[16]_i_2_n_0 ),
        .I2(is_it_first_strobe),
        .I3(\adc_data_pos[0]_i_2_n_0 ),
        .I4(adc_data[0]),
        .O(\adc_data_pos[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \adc_data_pos[0]_i_2 
       (.I0(out_cnt[0]),
        .I1(out_cnt[1]),
        .I2(out_cnt[2]),
        .I3(\adc_data_pos[16]_i_2_n_0 ),
        .O(\adc_data_pos[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \adc_data_pos[12]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_pos[16]_i_2_n_0 ),
        .I2(is_it_first_strobe),
        .I3(\adc_data_pos[12]_i_2_n_0 ),
        .I4(adc_data[12]),
        .O(\adc_data_pos[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFEFF)) 
    \adc_data_pos[12]_i_2 
       (.I0(\adc_data_pos[16]_i_2_n_0 ),
        .I1(out_cnt[2]),
        .I2(out_cnt[1]),
        .I3(out_cnt[0]),
        .O(\adc_data_pos[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \adc_data_pos[13]_i_1 
       (.I0(adc_da),
        .I1(\adc_data_pos[13]_i_2_n_0 ),
        .I2(adc_data[13]),
        .O(\adc_data_pos[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000004)) 
    \adc_data_pos[13]_i_2 
       (.I0(is_it_first_strobe),
        .I1(out_cnt[0]),
        .I2(out_cnt[1]),
        .I3(out_cnt[2]),
        .I4(\adc_data_pos[16]_i_2_n_0 ),
        .O(\adc_data_pos[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \adc_data_pos[16]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_pos[16]_i_2_n_0 ),
        .I2(\adc_data_pos[16]_i_3_n_0 ),
        .I3(is_it_first_strobe),
        .I4(adc_data[16]),
        .O(\adc_data_pos[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hEEEA)) 
    \adc_data_pos[16]_i_2 
       (.I0(data_ready_clk_INST_0_i_1_n_0),
        .I1(out_cnt[2]),
        .I2(out_cnt[1]),
        .I3(out_cnt[0]),
        .O(\adc_data_pos[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \adc_data_pos[16]_i_3 
       (.I0(\adc_data_pos[16]_i_2_n_0 ),
        .I1(out_cnt[2]),
        .I2(out_cnt[0]),
        .I3(out_cnt[1]),
        .O(\adc_data_pos[16]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \adc_data_pos[17]_i_1 
       (.I0(adc_da),
        .I1(\adc_data_pos[17]_i_3_n_0 ),
        .I2(adc_data[17]),
        .O(\adc_data_pos[17]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \adc_data_pos[17]_i_2 
       (.I0(is_strobe_clk_active_neg),
        .I1(clk_adc_delay),
        .I2(clk_locked),
        .I3(is_strobe_clk_active),
        .O(adc_data_pos0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \adc_data_pos[17]_i_3 
       (.I0(\adc_data_pos[16]_i_2_n_0 ),
        .I1(out_cnt[2]),
        .I2(out_cnt[0]),
        .I3(out_cnt[1]),
        .I4(is_it_first_strobe),
        .O(\adc_data_pos[17]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \adc_data_pos[1]_i_1 
       (.I0(adc_da),
        .I1(\adc_data_pos[1]_i_2_n_0 ),
        .I2(adc_data[1]),
        .O(\adc_data_pos[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \adc_data_pos[1]_i_2 
       (.I0(is_it_first_strobe),
        .I1(\adc_data_pos[16]_i_2_n_0 ),
        .I2(out_cnt[2]),
        .I3(out_cnt[1]),
        .I4(out_cnt[0]),
        .O(\adc_data_pos[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \adc_data_pos[4]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_pos[16]_i_2_n_0 ),
        .I2(is_it_first_strobe),
        .I3(\adc_data_pos[4]_i_2_n_0 ),
        .I4(adc_data[4]),
        .O(\adc_data_pos[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hEFFF)) 
    \adc_data_pos[4]_i_2 
       (.I0(\adc_data_pos[16]_i_2_n_0 ),
        .I1(out_cnt[2]),
        .I2(out_cnt[1]),
        .I3(out_cnt[0]),
        .O(\adc_data_pos[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \adc_data_pos[5]_i_1 
       (.I0(adc_da),
        .I1(\adc_data_pos[5]_i_2_n_0 ),
        .I2(adc_data[5]),
        .O(\adc_data_pos[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000040)) 
    \adc_data_pos[5]_i_2 
       (.I0(is_it_first_strobe),
        .I1(out_cnt[0]),
        .I2(out_cnt[1]),
        .I3(out_cnt[2]),
        .I4(\adc_data_pos[16]_i_2_n_0 ),
        .O(\adc_data_pos[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \adc_data_pos[8]_i_1 
       (.I0(adc_db),
        .I1(\adc_data_pos[16]_i_2_n_0 ),
        .I2(is_it_first_strobe),
        .I3(\adc_data_pos[8]_i_2_n_0 ),
        .I4(adc_data[8]),
        .O(\adc_data_pos[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFFFB)) 
    \adc_data_pos[8]_i_2 
       (.I0(out_cnt[0]),
        .I1(out_cnt[1]),
        .I2(out_cnt[2]),
        .I3(\adc_data_pos[16]_i_2_n_0 ),
        .O(\adc_data_pos[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \adc_data_pos[9]_i_1 
       (.I0(adc_da),
        .I1(\adc_data_pos[9]_i_2_n_0 ),
        .I2(adc_data[9]),
        .O(\adc_data_pos[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h00000100)) 
    \adc_data_pos[9]_i_2 
       (.I0(is_it_first_strobe),
        .I1(\adc_data_pos[16]_i_2_n_0 ),
        .I2(out_cnt[2]),
        .I3(out_cnt[1]),
        .I4(out_cnt[0]),
        .O(\adc_data_pos[9]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[0] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[0]_i_1_n_0 ),
        .Q(adc_data[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[12] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[12]_i_1_n_0 ),
        .Q(adc_data[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[13] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[13]_i_1_n_0 ),
        .Q(adc_data[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[16] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[16]_i_1_n_0 ),
        .Q(adc_data[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[17] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[17]_i_1_n_0 ),
        .Q(adc_data[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[1] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[1]_i_1_n_0 ),
        .Q(adc_data[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[4] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[4]_i_1_n_0 ),
        .Q(adc_data[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[5] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[5]_i_1_n_0 ),
        .Q(adc_data[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[8] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[8]_i_1_n_0 ),
        .Q(adc_data[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \adc_data_pos_reg[9] 
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(\adc_data_pos[9]_i_1_n_0 ),
        .Q(adc_data[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hA2AE)) 
    adc_reading_allowed_neg_reg_i_1
       (.I0(adc_reading_allowed_neg_reg_reg_n_0),
        .I1(\adc_current_clk_num_reg_reg_n_0_[0] ),
        .I2(\adc_current_clk_num_reg_reg_n_0_[1] ),
        .I3(\adc_current_clk_num_reg_reg_n_0_[2] ),
        .O(adc_reading_allowed_neg_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    adc_reading_allowed_neg_reg_reg
       (.C(tfirstclk_passed_prev0),
        .CE(1'b1),
        .D(adc_reading_allowed_neg_reg_i_1_n_0),
        .Q(adc_reading_allowed_neg_reg_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDF00DFFFDF00DF00)) 
    adc_reading_allowed_reg_i_1
       (.I0(\adc_current_clk_num_reg_reg_n_0_[0] ),
        .I1(\adc_current_clk_num_reg_reg_n_0_[1] ),
        .I2(\adc_current_clk_num_reg_reg_n_0_[2] ),
        .I3(adc_reading_allowed_reg_reg_n_0),
        .I4(tfirstclk_passed_prev),
        .I5(tfirstclk_passed),
        .O(adc_reading_allowed_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    adc_reading_allowed_reg_reg
       (.C(tfirstclk_passed_prev0),
        .CE(1'b1),
        .D(adc_reading_allowed_reg_i_1_n_0),
        .Q(adc_reading_allowed_reg_reg_n_0),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1 clk_wizard_adc
       (.clk_in1(fpga_clk),
        .clk_out1(clk_adc),
        .clk_out2(clk_adc_delay),
        .locked(clk_locked));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    data_ready_clk_INST_0
       (.I0(data_ready_clk_INST_0_i_1_n_0),
        .I1(out_cnt[0]),
        .I2(out_cnt[2]),
        .I3(out_cnt[1]),
        .O(data_ready_clk));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    data_ready_clk_INST_0_i_1
       (.I0(out_cnt[6]),
        .I1(out_cnt[4]),
        .I2(out_cnt[3]),
        .I3(out_cnt[5]),
        .I4(out_cnt[7]),
        .O(data_ready_clk_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \fpga_clk_counter[0]_i_1 
       (.I0(fpga_clk_counter_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fpga_clk_counter[1]_i_1 
       (.I0(fpga_clk_counter_reg[0]),
        .I1(fpga_clk_counter_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fpga_clk_counter[2]_i_1 
       (.I0(fpga_clk_counter_reg[0]),
        .I1(fpga_clk_counter_reg[1]),
        .I2(fpga_clk_counter_reg[2]),
        .O(\fpga_clk_counter[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \fpga_clk_counter[3]_i_1 
       (.I0(fpga_clk_counter_reg[1]),
        .I1(fpga_clk_counter_reg[0]),
        .I2(fpga_clk_counter_reg[2]),
        .I3(fpga_clk_counter_reg[3]),
        .O(p_0_in__0[3]));
  FDRE #(
    .INIT(1'b0)) 
    \fpga_clk_counter_reg[0] 
       (.C(fpga_clk),
        .CE(1'b1),
        .D(p_0_in__0[0]),
        .Q(fpga_clk_counter_reg[0]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \fpga_clk_counter_reg[1] 
       (.C(fpga_clk),
        .CE(1'b1),
        .D(p_0_in__0[1]),
        .Q(fpga_clk_counter_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \fpga_clk_counter_reg[2] 
       (.C(fpga_clk),
        .CE(1'b1),
        .D(\fpga_clk_counter[2]_i_1_n_0 ),
        .Q(fpga_clk_counter_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \fpga_clk_counter_reg[3] 
       (.C(fpga_clk),
        .CE(1'b1),
        .D(p_0_in__0[3]),
        .Q(fpga_clk_counter_reg[3]),
        .R(clear));
  LUT4 #(
    .INIT(16'hAAA8)) 
    \inst/ 
       (.I0(fpga_clk_counter_reg[3]),
        .I1(fpga_clk_counter_reg[0]),
        .I2(fpga_clk_counter_reg[1]),
        .I3(fpga_clk_counter_reg[2]),
        .O(clear));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hAAAAA3AA)) 
    is_data_ready_i_1
       (.I0(data_ready),
        .I1(out_cnt[0]),
        .I2(out_cnt[1]),
        .I3(out_cnt[2]),
        .I4(data_ready_clk_INST_0_i_1_n_0),
        .O(is_data_ready_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    is_data_ready_reg
       (.C(adc_data_pos0),
        .CE(1'b1),
        .D(is_data_ready_i_1_n_0),
        .Q(data_ready),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h70707770)) 
    is_it_first_strobe_i_1
       (.I0(adc_reading_allowed_reg1__1),
        .I1(adc_reading_allowed_reg_reg_n_0),
        .I2(is_it_first_strobe),
        .I3(tfirstclk_passed),
        .I4(tfirstclk_passed_prev),
        .O(is_it_first_strobe_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h1F)) 
    is_it_first_strobe_i_2
       (.I0(\adc_current_clk_num_reg_reg_n_0_[0] ),
        .I1(\adc_current_clk_num_reg_reg_n_0_[1] ),
        .I2(\adc_current_clk_num_reg_reg_n_0_[2] ),
        .O(adc_reading_allowed_reg1__1));
  FDRE #(
    .INIT(1'b0)) 
    is_it_first_strobe_reg
       (.C(tfirstclk_passed_prev0),
        .CE(1'b1),
        .D(is_it_first_strobe_i_1_n_0),
        .Q(is_it_first_strobe),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    is_strobe_clk_active_neg_reg
       (.C(clk_adc_delay),
        .CE(1'b1),
        .D(adc_reading_allowed_neg_reg_reg_n_0),
        .Q(is_strobe_clk_active_neg),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    is_strobe_clk_active_reg
       (.C(clk_adc_delay),
        .CE(1'b1),
        .D(adc_reading_allowed_reg_reg_n_0),
        .Q(is_strobe_clk_active),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAAEA8AAA)) 
    tfirstclk_passed_i_1
       (.I0(tfirstclk_passed),
        .I1(fpga_clk_counter_reg[1]),
        .I2(fpga_clk_counter_reg[0]),
        .I3(fpga_clk_counter_reg[3]),
        .I4(fpga_clk_counter_reg[2]),
        .O(tfirstclk_passed_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tfirstclk_passed_prev_reg
       (.C(tfirstclk_passed_prev0),
        .CE(1'b1),
        .D(tfirstclk_passed),
        .Q(tfirstclk_passed_prev),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    tfirstclk_passed_reg
       (.C(fpga_clk),
        .CE(1'b1),
        .D(tfirstclk_passed_i_1_n_0),
        .Q(tfirstclk_passed),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1
   (clk_out1,
    clk_out2,
    locked,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output locked;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire locked;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .locked(locked));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_1_clk_wiz
   (clk_out1,
    clk_out2,
    locked,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output locked;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire clk_out1_clk_wiz_1;
  wire clk_out2;
  wire clk_out2_clk_wiz_1;
  wire clkfbout_buf_clk_wiz_1;
  wire clkfbout_clk_wiz_1;
  wire locked;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_1),
        .O(clkfbout_buf_clk_wiz_1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_1),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_1),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* OPT_MODIFIED = "MLO" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(9.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(9.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(9),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(160.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_1),
        .CLKFBOUT(clkfbout_clk_wiz_1),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_1),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC_GET_DATA_0_0,adc_get_data,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "adc_get_data,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (adc_data,
    data_ready,
    data_ready_clk,
    out_cnt,
    adc_dax_p,
    adc_dax_n,
    adc_dbx_p,
    adc_dbx_n,
    adc_dcox_p,
    adc_dcox_n,
    fpga_clkx_p,
    fpga_clkx_n,
    adc_clkx_p,
    adc_clkx_n);
  output [17:0]adc_data;
  output data_ready;
  output data_ready_clk;
  output [7:0]out_cnt;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax p" *) input adc_dax_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dax, SV_INTERFACE true" *) input adc_dax_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx p" *) input adc_dbx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true" *) input adc_dbx_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox p" *) input adc_dcox_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true" *) input adc_dcox_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 fpga_clkx p" *) input fpga_clkx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 fpga_clkx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME fpga_clkx, SV_INTERFACE true" *) input fpga_clkx_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx p" *) output adc_clkx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true" *) output adc_clkx_n;

  (* SLEW = "SLOW" *) wire adc_clkx_n;
  (* SLEW = "SLOW" *) wire adc_clkx_p;
  wire [17:0]adc_data;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dax_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dax_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dbx_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dbx_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dcox_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dcox_p;
  wire data_ready;
  wire data_ready_clk;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire fpga_clkx_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire fpga_clkx_p;
  wire [7:0]out_cnt;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data inst
       (.adc_clkx_n(adc_clkx_n),
        .adc_clkx_p(adc_clkx_p),
        .adc_data(adc_data),
        .adc_dax_n(adc_dax_n),
        .adc_dax_p(adc_dax_p),
        .adc_dbx_n(adc_dbx_n),
        .adc_dbx_p(adc_dbx_p),
        .adc_dcox_n(adc_dcox_n),
        .adc_dcox_p(adc_dcox_p),
        .data_ready(data_ready),
        .data_ready_clk(data_ready_clk),
        .fpga_clkx_n(fpga_clkx_n),
        .fpga_clkx_p(fpga_clkx_p),
        .out_cnt(out_cnt));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
