// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun 23 11:10:01 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_init_clk_0_0_sim_netlist.v
// Design      : design_1_init_clk_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
   (clk_out1,
    clk_out2,
    clk_out3,
    clk_out4,
    locked,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  output clk_out4;
  output locked;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire clk_out3;
  wire clk_out4;
  wire locked;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .clk_out3(clk_out3),
        .clk_out4(clk_out4),
        .locked(locked));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
   (clk_out1,
    clk_out2,
    clk_out3,
    clk_out4,
    locked,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  output clk_out4;
  output locked;
  input clk_in1;

  wire clk_in1;
  wire clk_in1_clk_wiz_0;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out2;
  wire clk_out2_clk_wiz_0;
  wire clk_out3;
  wire clk_out3_clk_wiz_0;
  wire clk_out4;
  wire clk_out4_clk_wiz_0;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire locked;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufg
       (.I(clk_in1),
        .O(clk_in1_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_0),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout3_buf
       (.I(clk_out3_clk_wiz_0),
        .O(clk_out3));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout4_buf
       (.I(clk_out4_clk_wiz_0),
        .O(clk_out4));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(10.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(125),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(50),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(100),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1_clk_wiz_0),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_0),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(clk_out3_clk_wiz_0),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(clk_out4_clk_wiz_0),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_init_clk_0_0,init_clk,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "init_clk,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    start_ext,
    clk_sclk,
    clk_mosi,
    clk_cs,
    clk_sync);
  input clk;
  input start_ext;
  output clk_sclk;
  output clk_mosi;
  output clk_cs;
  output clk_sync;

  (* IBUF_LOW_PWR *) wire clk;
  wire clk_cs;
  wire clk_mosi;
  wire clk_sclk;
  wire clk_sync;
  wire start_ext;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_init_clk inst
       (.clk(clk),
        .clk_cs(clk_cs),
        .clk_mosi(clk_mosi),
        .clk_sclk(clk_sclk),
        .clk_start_init_reg_0(clk_sync),
        .start_ext(start_ext));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_init_clk
   (clk_start_init_reg_0,
    clk_cs,
    clk_sclk,
    clk_mosi,
    start_ext,
    clk);
  output clk_start_init_reg_0;
  output clk_cs;
  output clk_sclk;
  output clk_mosi;
  input start_ext;
  input clk;

  wire \bits_left[6]_i_1_n_0 ;
  wire \bits_left[6]_i_2_n_0 ;
  wire \bits_left[6]_i_4_n_0 ;
  wire [6:0]bits_left_reg;
  wire clk;
  wire clk_8MHz;
  wire clk_cs;
  wire clk_cs_reg_i_1_n_0;
  wire clk_cs_reg_i_2_n_0;
  wire clk_cs_reg_i_3_n_0;
  wire clk_cs_reg_i_4_n_0;
  wire clk_mosi;
  wire clk_mosi_reg_i_1_n_0;
  wire clk_mosi_reg_i_2_n_0;
  wire clk_mosi_reg_i_3_n_0;
  wire clk_mosi_reg_i_4_n_0;
  wire clk_sclk;
  wire clk_sclk_INST_0_i_1_n_0;
  wire clk_start_init_i_10_n_0;
  wire clk_start_init_i_11_n_0;
  wire clk_start_init_i_12_n_0;
  wire clk_start_init_i_13_n_0;
  wire clk_start_init_i_14_n_0;
  wire clk_start_init_i_15_n_0;
  wire clk_start_init_i_16_n_0;
  wire clk_start_init_i_17_n_0;
  wire clk_start_init_i_18_n_0;
  wire clk_start_init_i_1_n_0;
  wire clk_start_init_i_3_n_0;
  wire clk_start_init_i_4_n_0;
  wire clk_start_init_i_5_n_0;
  wire clk_start_init_i_6_n_0;
  wire clk_start_init_i_7_n_0;
  wire clk_start_init_i_8_n_0;
  wire clk_start_init_i_9_n_0;
  wire clk_start_init_reg_0;
  wire clk_sync_counter_reg;
  wire [31:1]clk_sync_counter_reg0;
  wire clk_sync_counter_reg0_0;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__0_n_0 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__0_n_1 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__0_n_2 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__0_n_3 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__1_n_0 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__1_n_1 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__1_n_2 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__1_n_3 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__2_n_0 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__2_n_1 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__2_n_2 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__2_n_3 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__3_n_0 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__3_n_1 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__3_n_2 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__3_n_3 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__4_n_0 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__4_n_1 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__4_n_2 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__4_n_3 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__5_n_0 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__5_n_1 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__5_n_2 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__5_n_3 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__6_n_2 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry__6_n_3 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry_n_0 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry_n_1 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry_n_2 ;
  wire \clk_sync_counter_reg0_inferred__0/i__carry_n_3 ;
  wire \clk_sync_counter_reg[0]_i_10_n_0 ;
  wire \clk_sync_counter_reg[0]_i_11_n_0 ;
  wire \clk_sync_counter_reg[0]_i_12_n_0 ;
  wire \clk_sync_counter_reg[0]_i_13_n_0 ;
  wire \clk_sync_counter_reg[0]_i_3_n_0 ;
  wire \clk_sync_counter_reg[0]_i_4_n_0 ;
  wire \clk_sync_counter_reg[0]_i_5_n_0 ;
  wire \clk_sync_counter_reg[0]_i_6_n_0 ;
  wire \clk_sync_counter_reg[0]_i_7_n_0 ;
  wire \clk_sync_counter_reg[0]_i_8_n_0 ;
  wire \clk_sync_counter_reg[0]_i_9_n_0 ;
  wire \clk_sync_counter_reg[12]_i_2_n_0 ;
  wire \clk_sync_counter_reg[12]_i_3_n_0 ;
  wire \clk_sync_counter_reg[12]_i_4_n_0 ;
  wire \clk_sync_counter_reg[12]_i_5_n_0 ;
  wire \clk_sync_counter_reg[16]_i_2_n_0 ;
  wire \clk_sync_counter_reg[16]_i_3_n_0 ;
  wire \clk_sync_counter_reg[16]_i_4_n_0 ;
  wire \clk_sync_counter_reg[16]_i_5_n_0 ;
  wire \clk_sync_counter_reg[20]_i_2_n_0 ;
  wire \clk_sync_counter_reg[20]_i_3_n_0 ;
  wire \clk_sync_counter_reg[20]_i_4_n_0 ;
  wire \clk_sync_counter_reg[20]_i_5_n_0 ;
  wire \clk_sync_counter_reg[24]_i_2_n_0 ;
  wire \clk_sync_counter_reg[24]_i_3_n_0 ;
  wire \clk_sync_counter_reg[24]_i_4_n_0 ;
  wire \clk_sync_counter_reg[24]_i_5_n_0 ;
  wire \clk_sync_counter_reg[28]_i_2_n_0 ;
  wire \clk_sync_counter_reg[28]_i_3_n_0 ;
  wire \clk_sync_counter_reg[28]_i_4_n_0 ;
  wire \clk_sync_counter_reg[28]_i_5_n_0 ;
  wire \clk_sync_counter_reg[4]_i_10_n_0 ;
  wire \clk_sync_counter_reg[4]_i_2_n_0 ;
  wire \clk_sync_counter_reg[4]_i_3_n_0 ;
  wire \clk_sync_counter_reg[4]_i_4_n_0 ;
  wire \clk_sync_counter_reg[4]_i_5_n_0 ;
  wire \clk_sync_counter_reg[4]_i_6_n_0 ;
  wire \clk_sync_counter_reg[4]_i_7_n_0 ;
  wire \clk_sync_counter_reg[4]_i_8_n_0 ;
  wire \clk_sync_counter_reg[4]_i_9_n_0 ;
  wire \clk_sync_counter_reg[8]_i_10_n_0 ;
  wire \clk_sync_counter_reg[8]_i_11_n_0 ;
  wire \clk_sync_counter_reg[8]_i_12_n_0 ;
  wire \clk_sync_counter_reg[8]_i_13_n_0 ;
  wire \clk_sync_counter_reg[8]_i_14_n_0 ;
  wire \clk_sync_counter_reg[8]_i_15_n_0 ;
  wire \clk_sync_counter_reg[8]_i_16_n_0 ;
  wire \clk_sync_counter_reg[8]_i_17_n_0 ;
  wire \clk_sync_counter_reg[8]_i_18_n_0 ;
  wire \clk_sync_counter_reg[8]_i_2_n_0 ;
  wire \clk_sync_counter_reg[8]_i_3_n_0 ;
  wire \clk_sync_counter_reg[8]_i_4_n_0 ;
  wire \clk_sync_counter_reg[8]_i_5_n_0 ;
  wire \clk_sync_counter_reg[8]_i_6_n_0 ;
  wire \clk_sync_counter_reg[8]_i_7_n_0 ;
  wire \clk_sync_counter_reg[8]_i_8_n_0 ;
  wire \clk_sync_counter_reg[8]_i_9_n_0 ;
  wire [31:0]clk_sync_counter_reg_reg;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_0 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_1 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_2 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_3 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_4 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_5 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_6 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_7 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_7 ;
  wire is_clk_initialized_reg;
  wire is_clk_initialized_reg_i_1_n_0;
  wire [6:0]p_0_in;
  wire start_ext;
  wire start_prev;
  wire start_prev_i_1_n_0;
  wire sys_clk_locked;
  wire [3:2]\NLW_clk_sync_counter_reg0_inferred__0/i__carry__6_CO_UNCONNECTED ;
  wire [3:3]\NLW_clk_sync_counter_reg0_inferred__0/i__carry__6_O_UNCONNECTED ;
  wire [3:3]\NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED ;
  wire NLW_clk_wizard_main_clk_out1_UNCONNECTED;
  wire NLW_clk_wizard_main_clk_out3_UNCONNECTED;
  wire NLW_clk_wizard_main_clk_out4_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bits_left[0]_i_1 
       (.I0(clk_sclk_INST_0_i_1_n_0),
        .I1(bits_left_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h82)) 
    \bits_left[1]_i_1 
       (.I0(clk_sclk_INST_0_i_1_n_0),
        .I1(bits_left_reg[1]),
        .I2(bits_left_reg[0]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hA802)) 
    \bits_left[2]_i_1 
       (.I0(clk_sclk_INST_0_i_1_n_0),
        .I1(bits_left_reg[0]),
        .I2(bits_left_reg[1]),
        .I3(bits_left_reg[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAAA80002)) 
    \bits_left[3]_i_1 
       (.I0(clk_sclk_INST_0_i_1_n_0),
        .I1(bits_left_reg[1]),
        .I2(bits_left_reg[0]),
        .I3(bits_left_reg[2]),
        .I4(bits_left_reg[3]),
        .O(p_0_in[3]));
  LUT6 #(
    .INIT(64'hAAAAAAA800000002)) 
    \bits_left[4]_i_1 
       (.I0(clk_sclk_INST_0_i_1_n_0),
        .I1(bits_left_reg[1]),
        .I2(bits_left_reg[0]),
        .I3(bits_left_reg[3]),
        .I4(bits_left_reg[2]),
        .I5(bits_left_reg[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFC00000002)) 
    \bits_left[5]_i_1 
       (.I0(bits_left_reg[6]),
        .I1(\bits_left[6]_i_4_n_0 ),
        .I2(bits_left_reg[0]),
        .I3(bits_left_reg[1]),
        .I4(bits_left_reg[4]),
        .I5(bits_left_reg[5]),
        .O(p_0_in[5]));
  LUT3 #(
    .INIT(8'h40)) 
    \bits_left[6]_i_1 
       (.I0(start_prev),
        .I1(clk_start_init_reg_0),
        .I2(start_ext),
        .O(\bits_left[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8880)) 
    \bits_left[6]_i_2 
       (.I0(clk_start_init_reg_0),
        .I1(start_ext),
        .I2(clk_sclk_INST_0_i_1_n_0),
        .I3(bits_left_reg[0]),
        .O(\bits_left[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    \bits_left[6]_i_3 
       (.I0(bits_left_reg[6]),
        .I1(bits_left_reg[5]),
        .I2(\bits_left[6]_i_4_n_0 ),
        .I3(bits_left_reg[0]),
        .I4(bits_left_reg[1]),
        .I5(bits_left_reg[4]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \bits_left[6]_i_4 
       (.I0(bits_left_reg[2]),
        .I1(bits_left_reg[3]),
        .O(\bits_left[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[0] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[0]),
        .Q(bits_left_reg[0]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[1] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[1]),
        .Q(bits_left_reg[1]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[2] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[2]),
        .Q(bits_left_reg[2]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[3] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[3]),
        .Q(bits_left_reg[3]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[4] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[4]),
        .Q(bits_left_reg[4]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[5] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[5]),
        .Q(bits_left_reg[5]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[6] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[6]),
        .Q(bits_left_reg[6]),
        .S(\bits_left[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAA00AAAAAA)) 
    clk_cs_reg_i_1
       (.I0(clk_cs),
        .I1(clk_cs_reg_i_2_n_0),
        .I2(clk_cs_reg_i_3_n_0),
        .I3(start_ext),
        .I4(clk_start_init_reg_0),
        .I5(start_prev),
        .O(clk_cs_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    clk_cs_reg_i_2
       (.I0(bits_left_reg[4]),
        .I1(bits_left_reg[1]),
        .I2(bits_left_reg[0]),
        .I3(bits_left_reg[3]),
        .I4(bits_left_reg[2]),
        .I5(bits_left_reg[5]),
        .O(clk_cs_reg_i_2_n_0));
  LUT5 #(
    .INIT(32'hFEEEAAAA)) 
    clk_cs_reg_i_3
       (.I0(bits_left_reg[6]),
        .I1(bits_left_reg[2]),
        .I2(bits_left_reg[1]),
        .I3(bits_left_reg[0]),
        .I4(clk_cs_reg_i_4_n_0),
        .O(clk_cs_reg_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h80)) 
    clk_cs_reg_i_4
       (.I0(bits_left_reg[3]),
        .I1(bits_left_reg[4]),
        .I2(bits_left_reg[5]),
        .O(clk_cs_reg_i_4_n_0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    clk_cs_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(clk_cs_reg_i_1_n_0),
        .Q(clk_cs),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAFFBFAAAA0080)) 
    clk_mosi_reg_i_1
       (.I0(clk_mosi_reg_i_2_n_0),
        .I1(start_ext),
        .I2(clk_start_init_reg_0),
        .I3(start_prev),
        .I4(\bits_left[6]_i_2_n_0 ),
        .I5(clk_mosi),
        .O(clk_mosi_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h000000000DFFFFFF)) 
    clk_mosi_reg_i_2
       (.I0(bits_left_reg[0]),
        .I1(clk_sclk_INST_0_i_1_n_0),
        .I2(clk_mosi_reg_i_3_n_0),
        .I3(start_ext),
        .I4(clk_start_init_reg_0),
        .I5(clk_mosi_reg_i_4_n_0),
        .O(clk_mosi_reg_i_2_n_0));
  LUT5 #(
    .INIT(32'hFBFBFBBB)) 
    clk_mosi_reg_i_3
       (.I0(bits_left_reg[6]),
        .I1(start_prev),
        .I2(clk_cs_reg_i_4_n_0),
        .I3(bits_left_reg[1]),
        .I4(bits_left_reg[2]),
        .O(clk_mosi_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'hFF74FCF1FF70FFFD)) 
    clk_mosi_reg_i_4
       (.I0(bits_left_reg[5]),
        .I1(bits_left_reg[1]),
        .I2(bits_left_reg[3]),
        .I3(bits_left_reg[2]),
        .I4(bits_left_reg[4]),
        .I5(bits_left_reg[0]),
        .O(clk_mosi_reg_i_4_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    clk_mosi_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(clk_mosi_reg_i_1_n_0),
        .Q(clk_mosi),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA8)) 
    clk_sclk_INST_0
       (.I0(clk),
        .I1(clk_sclk_INST_0_i_1_n_0),
        .I2(bits_left_reg[0]),
        .O(clk_sclk));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    clk_sclk_INST_0_i_1
       (.I0(bits_left_reg[6]),
        .I1(bits_left_reg[4]),
        .I2(bits_left_reg[2]),
        .I3(bits_left_reg[3]),
        .I4(bits_left_reg[1]),
        .I5(bits_left_reg[5]),
        .O(clk_sclk_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'hCCCCCCCC8FCCCCCC)) 
    clk_start_init_i_1
       (.I0(clk_start_init_i_3_n_0),
        .I1(clk_start_init_reg_0),
        .I2(clk_start_init_i_4_n_0),
        .I3(clk_start_init_i_5_n_0),
        .I4(start_ext),
        .I5(is_clk_initialized_reg),
        .O(clk_start_init_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    clk_start_init_i_10
       (.I0(clk_sync_counter_reg_reg[17]),
        .I1(clk_sync_counter_reg_reg[16]),
        .I2(clk_start_init_i_18_n_0),
        .I3(clk_sync_counter_reg_reg[6]),
        .I4(clk_sync_counter_reg_reg[4]),
        .I5(clk_start_init_i_16_n_0),
        .O(clk_start_init_i_10_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    clk_start_init_i_11
       (.I0(clk_sync_counter_reg_reg[21]),
        .I1(clk_sync_counter_reg_reg[22]),
        .I2(clk_sync_counter_reg_reg[19]),
        .I3(clk_sync_counter_reg_reg[20]),
        .I4(clk_sync_counter_reg_reg[18]),
        .I5(clk_sync_counter_reg_reg[23]),
        .O(clk_start_init_i_11_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    clk_start_init_i_12
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg_reg[12]),
        .I3(clk_sync_counter_reg_reg[11]),
        .O(clk_start_init_i_12_n_0));
  LUT6 #(
    .INIT(64'hFAF8000000000000)) 
    clk_start_init_i_13
       (.I0(clk_sync_counter_reg_reg[7]),
        .I1(clk_sync_counter_reg_reg[6]),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(clk_sync_counter_reg_reg[5]),
        .I4(clk_sync_counter_reg_reg[9]),
        .I5(clk_sync_counter_reg_reg[10]),
        .O(clk_start_init_i_13_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    clk_start_init_i_14
       (.I0(clk_sync_counter_reg_reg[29]),
        .I1(clk_sync_counter_reg_reg[31]),
        .I2(clk_sync_counter_reg_reg[26]),
        .I3(clk_sync_counter_reg_reg[30]),
        .O(clk_start_init_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    clk_start_init_i_15
       (.I0(clk_sync_counter_reg_reg[6]),
        .I1(clk_sync_counter_reg_reg[5]),
        .I2(clk_sync_counter_reg_reg[7]),
        .O(clk_start_init_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h7)) 
    clk_start_init_i_16
       (.I0(clk_sync_counter_reg_reg[10]),
        .I1(clk_sync_counter_reg_reg[9]),
        .O(clk_start_init_i_16_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    clk_start_init_i_17
       (.I0(clk_sync_counter_reg_reg[1]),
        .I1(clk_sync_counter_reg_reg[0]),
        .I2(clk_sync_counter_reg_reg[2]),
        .I3(clk_sync_counter_reg_reg[3]),
        .O(clk_start_init_i_17_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    clk_start_init_i_18
       (.I0(clk_sync_counter_reg_reg[7]),
        .I1(clk_sync_counter_reg_reg[5]),
        .O(clk_start_init_i_18_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    clk_start_init_i_2
       (.I0(clk_8MHz),
        .I1(sys_clk_locked),
        .O(clk_sync_counter_reg0_0));
  LUT6 #(
    .INIT(64'h222A222A222A22AA)) 
    clk_start_init_i_3
       (.I0(clk_start_init_i_6_n_0),
        .I1(clk_sync_counter_reg_reg[17]),
        .I2(clk_sync_counter_reg_reg[15]),
        .I3(clk_sync_counter_reg_reg[16]),
        .I4(clk_start_init_i_7_n_0),
        .I5(clk_sync_counter_reg_reg[14]),
        .O(clk_start_init_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    clk_start_init_i_4
       (.I0(clk_start_init_i_8_n_0),
        .I1(clk_start_init_i_9_n_0),
        .I2(clk_start_init_i_10_n_0),
        .I3(clk_start_init_i_11_n_0),
        .O(clk_start_init_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFDDDDDDDDDDDDD)) 
    clk_start_init_i_5
       (.I0(clk_start_init_i_6_n_0),
        .I1(clk_sync_counter_reg_reg[17]),
        .I2(clk_start_init_i_12_n_0),
        .I3(clk_start_init_i_13_n_0),
        .I4(clk_sync_counter_reg_reg[16]),
        .I5(clk_sync_counter_reg_reg[15]),
        .O(clk_start_init_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    clk_start_init_i_6
       (.I0(clk_start_init_i_11_n_0),
        .I1(clk_sync_counter_reg_reg[28]),
        .I2(clk_sync_counter_reg_reg[24]),
        .I3(clk_sync_counter_reg_reg[25]),
        .I4(clk_start_init_i_14_n_0),
        .I5(clk_sync_counter_reg_reg[27]),
        .O(clk_start_init_i_6_n_0));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    clk_start_init_i_7
       (.I0(clk_start_init_i_15_n_0),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[13]),
        .I3(clk_sync_counter_reg_reg[8]),
        .I4(clk_sync_counter_reg_reg[11]),
        .I5(clk_start_init_i_16_n_0),
        .O(clk_start_init_i_7_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    clk_start_init_i_8
       (.I0(clk_start_init_i_17_n_0),
        .I1(clk_sync_counter_reg_reg[8]),
        .I2(clk_sync_counter_reg_reg[15]),
        .I3(clk_sync_counter_reg_reg[27]),
        .I4(clk_start_init_i_12_n_0),
        .O(clk_start_init_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    clk_start_init_i_9
       (.I0(clk_sync_counter_reg_reg[28]),
        .I1(clk_sync_counter_reg_reg[24]),
        .I2(clk_sync_counter_reg_reg[25]),
        .I3(clk_start_init_i_14_n_0),
        .O(clk_start_init_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    clk_start_init_reg
       (.C(clk_sync_counter_reg0_0),
        .CE(1'b1),
        .D(clk_start_init_i_1_n_0),
        .Q(clk_start_init_reg_0),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\clk_sync_counter_reg0_inferred__0/i__carry_n_0 ,\clk_sync_counter_reg0_inferred__0/i__carry_n_1 ,\clk_sync_counter_reg0_inferred__0/i__carry_n_2 ,\clk_sync_counter_reg0_inferred__0/i__carry_n_3 }),
        .CYINIT(clk_sync_counter_reg_reg[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[4:1]),
        .S(clk_sync_counter_reg_reg[4:1]));
  CARRY4 \clk_sync_counter_reg0_inferred__0/i__carry__0 
       (.CI(\clk_sync_counter_reg0_inferred__0/i__carry_n_0 ),
        .CO({\clk_sync_counter_reg0_inferred__0/i__carry__0_n_0 ,\clk_sync_counter_reg0_inferred__0/i__carry__0_n_1 ,\clk_sync_counter_reg0_inferred__0/i__carry__0_n_2 ,\clk_sync_counter_reg0_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[8:5]),
        .S(clk_sync_counter_reg_reg[8:5]));
  CARRY4 \clk_sync_counter_reg0_inferred__0/i__carry__1 
       (.CI(\clk_sync_counter_reg0_inferred__0/i__carry__0_n_0 ),
        .CO({\clk_sync_counter_reg0_inferred__0/i__carry__1_n_0 ,\clk_sync_counter_reg0_inferred__0/i__carry__1_n_1 ,\clk_sync_counter_reg0_inferred__0/i__carry__1_n_2 ,\clk_sync_counter_reg0_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[12:9]),
        .S(clk_sync_counter_reg_reg[12:9]));
  CARRY4 \clk_sync_counter_reg0_inferred__0/i__carry__2 
       (.CI(\clk_sync_counter_reg0_inferred__0/i__carry__1_n_0 ),
        .CO({\clk_sync_counter_reg0_inferred__0/i__carry__2_n_0 ,\clk_sync_counter_reg0_inferred__0/i__carry__2_n_1 ,\clk_sync_counter_reg0_inferred__0/i__carry__2_n_2 ,\clk_sync_counter_reg0_inferred__0/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[16:13]),
        .S(clk_sync_counter_reg_reg[16:13]));
  CARRY4 \clk_sync_counter_reg0_inferred__0/i__carry__3 
       (.CI(\clk_sync_counter_reg0_inferred__0/i__carry__2_n_0 ),
        .CO({\clk_sync_counter_reg0_inferred__0/i__carry__3_n_0 ,\clk_sync_counter_reg0_inferred__0/i__carry__3_n_1 ,\clk_sync_counter_reg0_inferred__0/i__carry__3_n_2 ,\clk_sync_counter_reg0_inferred__0/i__carry__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[20:17]),
        .S(clk_sync_counter_reg_reg[20:17]));
  CARRY4 \clk_sync_counter_reg0_inferred__0/i__carry__4 
       (.CI(\clk_sync_counter_reg0_inferred__0/i__carry__3_n_0 ),
        .CO({\clk_sync_counter_reg0_inferred__0/i__carry__4_n_0 ,\clk_sync_counter_reg0_inferred__0/i__carry__4_n_1 ,\clk_sync_counter_reg0_inferred__0/i__carry__4_n_2 ,\clk_sync_counter_reg0_inferred__0/i__carry__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[24:21]),
        .S(clk_sync_counter_reg_reg[24:21]));
  CARRY4 \clk_sync_counter_reg0_inferred__0/i__carry__5 
       (.CI(\clk_sync_counter_reg0_inferred__0/i__carry__4_n_0 ),
        .CO({\clk_sync_counter_reg0_inferred__0/i__carry__5_n_0 ,\clk_sync_counter_reg0_inferred__0/i__carry__5_n_1 ,\clk_sync_counter_reg0_inferred__0/i__carry__5_n_2 ,\clk_sync_counter_reg0_inferred__0/i__carry__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[28:25]),
        .S(clk_sync_counter_reg_reg[28:25]));
  CARRY4 \clk_sync_counter_reg0_inferred__0/i__carry__6 
       (.CI(\clk_sync_counter_reg0_inferred__0/i__carry__5_n_0 ),
        .CO({\NLW_clk_sync_counter_reg0_inferred__0/i__carry__6_CO_UNCONNECTED [3:2],\clk_sync_counter_reg0_inferred__0/i__carry__6_n_2 ,\clk_sync_counter_reg0_inferred__0/i__carry__6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_clk_sync_counter_reg0_inferred__0/i__carry__6_O_UNCONNECTED [3],clk_sync_counter_reg0[31:29]}),
        .S({1'b0,clk_sync_counter_reg_reg[31:29]}));
  LUT3 #(
    .INIT(8'h08)) 
    \clk_sync_counter_reg[0]_i_1 
       (.I0(clk_start_init_i_3_n_0),
        .I1(start_ext),
        .I2(is_clk_initialized_reg),
        .O(clk_sync_counter_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFEB)) 
    \clk_sync_counter_reg[0]_i_10 
       (.I0(\clk_sync_counter_reg[8]_i_10_n_0 ),
        .I1(clk_sync_counter_reg_reg[16]),
        .I2(clk_sync_counter_reg_reg[17]),
        .I3(clk_sync_counter_reg_reg[11]),
        .I4(clk_sync_counter_reg_reg[4]),
        .I5(clk_sync_counter_reg_reg[6]),
        .O(\clk_sync_counter_reg[0]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFF9FF9)) 
    \clk_sync_counter_reg[0]_i_11 
       (.I0(clk_sync_counter_reg_reg[10]),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[16]),
        .I3(clk_sync_counter_reg_reg[15]),
        .I4(\clk_sync_counter_reg[0]_i_13_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h777F)) 
    \clk_sync_counter_reg[0]_i_12 
       (.I0(clk_sync_counter_reg_reg[15]),
        .I1(clk_sync_counter_reg_reg[16]),
        .I2(clk_start_init_i_13_n_0),
        .I3(clk_start_init_i_12_n_0),
        .O(\clk_sync_counter_reg[0]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hBFFD)) 
    \clk_sync_counter_reg[0]_i_13 
       (.I0(clk_sync_counter_reg_reg[8]),
        .I1(clk_sync_counter_reg_reg[9]),
        .I2(clk_sync_counter_reg_reg[7]),
        .I3(clk_sync_counter_reg_reg[5]),
        .O(\clk_sync_counter_reg[0]_i_13_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \clk_sync_counter_reg[0]_i_3 
       (.I0(clk_start_init_i_5_n_0),
        .O(\clk_sync_counter_reg[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \clk_sync_counter_reg[0]_i_4 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[3]),
        .I2(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I3(clk_sync_counter_reg0[3]),
        .O(\clk_sync_counter_reg[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \clk_sync_counter_reg[0]_i_5 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[2]),
        .I2(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I3(clk_sync_counter_reg0[2]),
        .O(\clk_sync_counter_reg[0]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \clk_sync_counter_reg[0]_i_6 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[1]),
        .I2(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I3(clk_sync_counter_reg0[1]),
        .O(\clk_sync_counter_reg[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \clk_sync_counter_reg[0]_i_7 
       (.I0(clk_sync_counter_reg_reg[0]),
        .O(\clk_sync_counter_reg[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0101FF0100000000)) 
    \clk_sync_counter_reg[0]_i_8 
       (.I0(\clk_sync_counter_reg[0]_i_9_n_0 ),
        .I1(\clk_sync_counter_reg[0]_i_10_n_0 ),
        .I2(\clk_sync_counter_reg[0]_i_11_n_0 ),
        .I3(\clk_sync_counter_reg[0]_i_12_n_0 ),
        .I4(clk_sync_counter_reg_reg[17]),
        .I5(clk_start_init_i_6_n_0),
        .O(\clk_sync_counter_reg[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFEBF)) 
    \clk_sync_counter_reg[0]_i_9 
       (.I0(clk_start_init_i_17_n_0),
        .I1(clk_sync_counter_reg_reg[13]),
        .I2(clk_sync_counter_reg_reg[14]),
        .I3(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[0]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF44F4)) 
    \clk_sync_counter_reg[12]_i_2 
       (.I0(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I1(clk_sync_counter_reg0[15]),
        .I2(clk_sync_counter_reg_reg[15]),
        .I3(clk_start_init_i_5_n_0),
        .I4(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .O(\clk_sync_counter_reg[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFF44444)) 
    \clk_sync_counter_reg[12]_i_3 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg0[14]),
        .I3(\clk_sync_counter_reg[8]_i_6_n_0 ),
        .I4(\clk_sync_counter_reg[8]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[12]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFF44444)) 
    \clk_sync_counter_reg[12]_i_4 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[13]),
        .I2(clk_sync_counter_reg0[13]),
        .I3(\clk_sync_counter_reg[8]_i_6_n_0 ),
        .I4(\clk_sync_counter_reg[8]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[12]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFF44444)) 
    \clk_sync_counter_reg[12]_i_5 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg0[12]),
        .I3(\clk_sync_counter_reg[8]_i_6_n_0 ),
        .I4(\clk_sync_counter_reg[8]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[16]_i_2 
       (.I0(clk_sync_counter_reg0[19]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[16]_i_3 
       (.I0(clk_sync_counter_reg0[18]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hA8)) 
    \clk_sync_counter_reg[16]_i_4 
       (.I0(\clk_sync_counter_reg[8]_i_7_n_0 ),
        .I1(\clk_sync_counter_reg[8]_i_6_n_0 ),
        .I2(clk_sync_counter_reg0[17]),
        .O(\clk_sync_counter_reg[16]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF44F4)) 
    \clk_sync_counter_reg[16]_i_5 
       (.I0(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I1(clk_sync_counter_reg0[16]),
        .I2(clk_sync_counter_reg_reg[16]),
        .I3(clk_start_init_i_5_n_0),
        .I4(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[20]_i_2 
       (.I0(clk_sync_counter_reg0[23]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[20]_i_3 
       (.I0(clk_sync_counter_reg0[22]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[20]_i_4 
       (.I0(clk_sync_counter_reg0[21]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[20]_i_5 
       (.I0(clk_sync_counter_reg0[20]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[24]_i_2 
       (.I0(clk_sync_counter_reg0[27]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[24]_i_3 
       (.I0(clk_sync_counter_reg0[26]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[24]_i_4 
       (.I0(clk_sync_counter_reg0[25]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[24]_i_5 
       (.I0(clk_sync_counter_reg0[24]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[28]_i_2 
       (.I0(clk_sync_counter_reg0[31]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[28]_i_3 
       (.I0(clk_sync_counter_reg0[30]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[28]_i_4 
       (.I0(clk_sync_counter_reg0[29]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \clk_sync_counter_reg[28]_i_5 
       (.I0(clk_sync_counter_reg0[28]),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \clk_sync_counter_reg[4]_i_10 
       (.I0(clk_sync_counter_reg_reg[11]),
        .I1(clk_sync_counter_reg_reg[4]),
        .I2(clk_sync_counter_reg_reg[6]),
        .O(\clk_sync_counter_reg[4]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hF4F4FFF4)) 
    \clk_sync_counter_reg[4]_i_2 
       (.I0(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I1(clk_sync_counter_reg0[7]),
        .I2(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I3(clk_sync_counter_reg_reg[7]),
        .I4(clk_start_init_i_5_n_0),
        .O(\clk_sync_counter_reg[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \clk_sync_counter_reg[4]_i_3 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[6]),
        .I2(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I3(clk_sync_counter_reg0[6]),
        .O(\clk_sync_counter_reg[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF44F4)) 
    \clk_sync_counter_reg[4]_i_4 
       (.I0(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I1(clk_sync_counter_reg0[5]),
        .I2(clk_sync_counter_reg_reg[5]),
        .I3(clk_start_init_i_5_n_0),
        .I4(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \clk_sync_counter_reg[4]_i_5 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[4]),
        .I2(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I3(clk_sync_counter_reg0[4]),
        .O(\clk_sync_counter_reg[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0100010000000100)) 
    \clk_sync_counter_reg[4]_i_6 
       (.I0(\clk_sync_counter_reg[0]_i_9_n_0 ),
        .I1(\clk_sync_counter_reg[0]_i_10_n_0 ),
        .I2(\clk_sync_counter_reg[0]_i_11_n_0 ),
        .I3(clk_start_init_i_6_n_0),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I5(\clk_sync_counter_reg[4]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000010000000000)) 
    \clk_sync_counter_reg[4]_i_7 
       (.I0(\clk_sync_counter_reg[4]_i_9_n_0 ),
        .I1(clk_sync_counter_reg_reg[15]),
        .I2(clk_sync_counter_reg_reg[16]),
        .I3(clk_sync_counter_reg_reg[17]),
        .I4(clk_sync_counter_reg_reg[10]),
        .I5(clk_sync_counter_reg_reg[12]),
        .O(\clk_sync_counter_reg[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    \clk_sync_counter_reg[4]_i_8 
       (.I0(\clk_sync_counter_reg[4]_i_10_n_0 ),
        .I1(clk_sync_counter_reg_reg[5]),
        .I2(clk_sync_counter_reg_reg[7]),
        .I3(clk_sync_counter_reg_reg[8]),
        .I4(clk_sync_counter_reg_reg[9]),
        .I5(clk_start_init_i_17_n_0),
        .O(\clk_sync_counter_reg[4]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \clk_sync_counter_reg[4]_i_9 
       (.I0(clk_sync_counter_reg_reg[14]),
        .I1(clk_sync_counter_reg_reg[13]),
        .O(\clk_sync_counter_reg[4]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h6FF6)) 
    \clk_sync_counter_reg[8]_i_10 
       (.I0(clk_sync_counter_reg_reg[12]),
        .I1(clk_sync_counter_reg_reg[13]),
        .I2(clk_sync_counter_reg_reg[9]),
        .I3(clk_sync_counter_reg_reg[10]),
        .O(\clk_sync_counter_reg[8]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF6FF6)) 
    \clk_sync_counter_reg[8]_i_11 
       (.I0(clk_sync_counter_reg_reg[15]),
        .I1(clk_sync_counter_reg_reg[16]),
        .I2(clk_sync_counter_reg_reg[13]),
        .I3(clk_sync_counter_reg_reg[14]),
        .I4(\clk_sync_counter_reg[4]_i_10_n_0 ),
        .I5(\clk_sync_counter_reg[8]_i_15_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \clk_sync_counter_reg[8]_i_12 
       (.I0(\clk_sync_counter_reg[8]_i_16_n_0 ),
        .I1(\clk_sync_counter_reg[4]_i_9_n_0 ),
        .I2(clk_sync_counter_reg_reg[16]),
        .I3(clk_sync_counter_reg_reg[15]),
        .I4(\clk_sync_counter_reg[8]_i_17_n_0 ),
        .I5(\clk_sync_counter_reg[8]_i_18_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \clk_sync_counter_reg[8]_i_13 
       (.I0(clk_sync_counter_reg_reg[10]),
        .I1(clk_sync_counter_reg_reg[12]),
        .O(\clk_sync_counter_reg[8]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'hF99F)) 
    \clk_sync_counter_reg[8]_i_14 
       (.I0(clk_sync_counter_reg_reg[17]),
        .I1(clk_sync_counter_reg_reg[16]),
        .I2(clk_sync_counter_reg_reg[14]),
        .I3(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[8]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    \clk_sync_counter_reg[8]_i_15 
       (.I0(clk_sync_counter_reg_reg[10]),
        .I1(clk_sync_counter_reg_reg[9]),
        .I2(clk_sync_counter_reg_reg[13]),
        .I3(clk_sync_counter_reg_reg[12]),
        .I4(clk_sync_counter_reg_reg[5]),
        .I5(clk_sync_counter_reg_reg[7]),
        .O(\clk_sync_counter_reg[8]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \clk_sync_counter_reg[8]_i_16 
       (.I0(clk_sync_counter_reg_reg[4]),
        .I1(clk_sync_counter_reg_reg[3]),
        .I2(clk_sync_counter_reg_reg[10]),
        .I3(clk_sync_counter_reg_reg[11]),
        .O(\clk_sync_counter_reg[8]_i_16_n_0 ));
  LUT4 #(
    .INIT(16'hDFFF)) 
    \clk_sync_counter_reg[8]_i_17 
       (.I0(clk_sync_counter_reg_reg[8]),
        .I1(clk_sync_counter_reg_reg[9]),
        .I2(clk_sync_counter_reg_reg[12]),
        .I3(clk_sync_counter_reg_reg[17]),
        .O(\clk_sync_counter_reg[8]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \clk_sync_counter_reg[8]_i_18 
       (.I0(clk_sync_counter_reg_reg[7]),
        .I1(clk_sync_counter_reg_reg[5]),
        .I2(clk_sync_counter_reg_reg[6]),
        .I3(clk_sync_counter_reg_reg[0]),
        .I4(clk_sync_counter_reg_reg[1]),
        .I5(clk_sync_counter_reg_reg[2]),
        .O(\clk_sync_counter_reg[8]_i_18_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \clk_sync_counter_reg[8]_i_2 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[11]),
        .I2(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I3(clk_sync_counter_reg0[11]),
        .O(\clk_sync_counter_reg[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF44F4)) 
    \clk_sync_counter_reg[8]_i_3 
       (.I0(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I1(clk_sync_counter_reg0[10]),
        .I2(clk_sync_counter_reg_reg[10]),
        .I3(clk_start_init_i_5_n_0),
        .I4(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF44F4)) 
    \clk_sync_counter_reg[8]_i_4 
       (.I0(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I1(clk_sync_counter_reg0[9]),
        .I2(clk_sync_counter_reg_reg[9]),
        .I3(clk_start_init_i_5_n_0),
        .I4(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFF44444)) 
    \clk_sync_counter_reg[8]_i_5 
       (.I0(clk_start_init_i_5_n_0),
        .I1(clk_sync_counter_reg_reg[8]),
        .I2(clk_sync_counter_reg0[8]),
        .I3(\clk_sync_counter_reg[8]_i_6_n_0 ),
        .I4(\clk_sync_counter_reg[8]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000002000002)) 
    \clk_sync_counter_reg[8]_i_6 
       (.I0(clk_start_init_i_6_n_0),
        .I1(\clk_sync_counter_reg[8]_i_8_n_0 ),
        .I2(\clk_sync_counter_reg[8]_i_9_n_0 ),
        .I3(clk_sync_counter_reg_reg[7]),
        .I4(clk_sync_counter_reg_reg[5]),
        .I5(\clk_sync_counter_reg[8]_i_10_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hDFDFDF0FDFDFDFDF)) 
    \clk_sync_counter_reg[8]_i_7 
       (.I0(\clk_sync_counter_reg[0]_i_12_n_0 ),
        .I1(clk_sync_counter_reg_reg[17]),
        .I2(clk_start_init_i_6_n_0),
        .I3(\clk_sync_counter_reg[8]_i_8_n_0 ),
        .I4(\clk_sync_counter_reg[8]_i_11_n_0 ),
        .I5(\clk_sync_counter_reg[8]_i_12_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFDFEFFF)) 
    \clk_sync_counter_reg[8]_i_8 
       (.I0(clk_sync_counter_reg_reg[9]),
        .I1(clk_start_init_i_17_n_0),
        .I2(\clk_sync_counter_reg[8]_i_13_n_0 ),
        .I3(clk_sync_counter_reg_reg[8]),
        .I4(clk_sync_counter_reg_reg[7]),
        .I5(\clk_sync_counter_reg[8]_i_14_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hBEFFFFBE)) 
    \clk_sync_counter_reg[8]_i_9 
       (.I0(\clk_sync_counter_reg[4]_i_10_n_0 ),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg_reg[13]),
        .I3(clk_sync_counter_reg_reg[16]),
        .I4(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[8]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[0] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_7 ),
        .Q(clk_sync_counter_reg_reg[0]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\clk_sync_counter_reg_reg[0]_i_2_n_0 ,\clk_sync_counter_reg_reg[0]_i_2_n_1 ,\clk_sync_counter_reg_reg[0]_i_2_n_2 ,\clk_sync_counter_reg_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\clk_sync_counter_reg[0]_i_3_n_0 }),
        .O({\clk_sync_counter_reg_reg[0]_i_2_n_4 ,\clk_sync_counter_reg_reg[0]_i_2_n_5 ,\clk_sync_counter_reg_reg[0]_i_2_n_6 ,\clk_sync_counter_reg_reg[0]_i_2_n_7 }),
        .S({\clk_sync_counter_reg[0]_i_4_n_0 ,\clk_sync_counter_reg[0]_i_5_n_0 ,\clk_sync_counter_reg[0]_i_6_n_0 ,\clk_sync_counter_reg[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[10] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[11] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[12] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[12]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[12]_i_1 
       (.CI(\clk_sync_counter_reg_reg[8]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[12]_i_1_n_0 ,\clk_sync_counter_reg_reg[12]_i_1_n_1 ,\clk_sync_counter_reg_reg[12]_i_1_n_2 ,\clk_sync_counter_reg_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[12]_i_1_n_4 ,\clk_sync_counter_reg_reg[12]_i_1_n_5 ,\clk_sync_counter_reg_reg[12]_i_1_n_6 ,\clk_sync_counter_reg_reg[12]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[12]_i_2_n_0 ,\clk_sync_counter_reg[12]_i_3_n_0 ,\clk_sync_counter_reg[12]_i_4_n_0 ,\clk_sync_counter_reg[12]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[13] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[14] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[15] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[16] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[16]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[16]_i_1 
       (.CI(\clk_sync_counter_reg_reg[12]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[16]_i_1_n_0 ,\clk_sync_counter_reg_reg[16]_i_1_n_1 ,\clk_sync_counter_reg_reg[16]_i_1_n_2 ,\clk_sync_counter_reg_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[16]_i_1_n_4 ,\clk_sync_counter_reg_reg[16]_i_1_n_5 ,\clk_sync_counter_reg_reg[16]_i_1_n_6 ,\clk_sync_counter_reg_reg[16]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[16]_i_2_n_0 ,\clk_sync_counter_reg[16]_i_3_n_0 ,\clk_sync_counter_reg[16]_i_4_n_0 ,\clk_sync_counter_reg[16]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[17] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[18] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[19] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[1] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_6 ),
        .Q(clk_sync_counter_reg_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[20] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[20]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[20]_i_1 
       (.CI(\clk_sync_counter_reg_reg[16]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[20]_i_1_n_0 ,\clk_sync_counter_reg_reg[20]_i_1_n_1 ,\clk_sync_counter_reg_reg[20]_i_1_n_2 ,\clk_sync_counter_reg_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[20]_i_1_n_4 ,\clk_sync_counter_reg_reg[20]_i_1_n_5 ,\clk_sync_counter_reg_reg[20]_i_1_n_6 ,\clk_sync_counter_reg_reg[20]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[20]_i_2_n_0 ,\clk_sync_counter_reg[20]_i_3_n_0 ,\clk_sync_counter_reg[20]_i_4_n_0 ,\clk_sync_counter_reg[20]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[21] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[22] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[23] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[24] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[24]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[24]_i_1 
       (.CI(\clk_sync_counter_reg_reg[20]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[24]_i_1_n_0 ,\clk_sync_counter_reg_reg[24]_i_1_n_1 ,\clk_sync_counter_reg_reg[24]_i_1_n_2 ,\clk_sync_counter_reg_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[24]_i_1_n_4 ,\clk_sync_counter_reg_reg[24]_i_1_n_5 ,\clk_sync_counter_reg_reg[24]_i_1_n_6 ,\clk_sync_counter_reg_reg[24]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[24]_i_2_n_0 ,\clk_sync_counter_reg[24]_i_3_n_0 ,\clk_sync_counter_reg[24]_i_4_n_0 ,\clk_sync_counter_reg[24]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[25] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[26] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[27] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[28] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[28]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[28]_i_1 
       (.CI(\clk_sync_counter_reg_reg[24]_i_1_n_0 ),
        .CO({\NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED [3],\clk_sync_counter_reg_reg[28]_i_1_n_1 ,\clk_sync_counter_reg_reg[28]_i_1_n_2 ,\clk_sync_counter_reg_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[28]_i_1_n_4 ,\clk_sync_counter_reg_reg[28]_i_1_n_5 ,\clk_sync_counter_reg_reg[28]_i_1_n_6 ,\clk_sync_counter_reg_reg[28]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[28]_i_2_n_0 ,\clk_sync_counter_reg[28]_i_3_n_0 ,\clk_sync_counter_reg[28]_i_4_n_0 ,\clk_sync_counter_reg[28]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[29] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[2] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_5 ),
        .Q(clk_sync_counter_reg_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[30] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[31] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[3] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_4 ),
        .Q(clk_sync_counter_reg_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[4] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[4]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[4]_i_1 
       (.CI(\clk_sync_counter_reg_reg[0]_i_2_n_0 ),
        .CO({\clk_sync_counter_reg_reg[4]_i_1_n_0 ,\clk_sync_counter_reg_reg[4]_i_1_n_1 ,\clk_sync_counter_reg_reg[4]_i_1_n_2 ,\clk_sync_counter_reg_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[4]_i_1_n_4 ,\clk_sync_counter_reg_reg[4]_i_1_n_5 ,\clk_sync_counter_reg_reg[4]_i_1_n_6 ,\clk_sync_counter_reg_reg[4]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[4]_i_2_n_0 ,\clk_sync_counter_reg[4]_i_3_n_0 ,\clk_sync_counter_reg[4]_i_4_n_0 ,\clk_sync_counter_reg[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[5] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[6] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[7] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[8] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[8]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[8]_i_1 
       (.CI(\clk_sync_counter_reg_reg[4]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[8]_i_1_n_0 ,\clk_sync_counter_reg_reg[8]_i_1_n_1 ,\clk_sync_counter_reg_reg[8]_i_1_n_2 ,\clk_sync_counter_reg_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[8]_i_1_n_4 ,\clk_sync_counter_reg_reg[8]_i_1_n_5 ,\clk_sync_counter_reg_reg[8]_i_1_n_6 ,\clk_sync_counter_reg_reg[8]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[8]_i_2_n_0 ,\clk_sync_counter_reg[8]_i_3_n_0 ,\clk_sync_counter_reg[8]_i_4_n_0 ,\clk_sync_counter_reg[8]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[9] 
       (.C(clk_sync_counter_reg0_0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[9]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 clk_wizard_main
       (.clk_in1(clk),
        .clk_out1(NLW_clk_wizard_main_clk_out1_UNCONNECTED),
        .clk_out2(clk_8MHz),
        .clk_out3(NLW_clk_wizard_main_clk_out3_UNCONNECTED),
        .clk_out4(NLW_clk_wizard_main_clk_out4_UNCONNECTED),
        .locked(sys_clk_locked));
  LUT4 #(
    .INIT(16'hAAEA)) 
    is_clk_initialized_reg_i_1
       (.I0(is_clk_initialized_reg),
        .I1(start_ext),
        .I2(clk_start_init_i_5_n_0),
        .I3(clk_start_init_i_3_n_0),
        .O(is_clk_initialized_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_clk_initialized_reg_reg
       (.C(clk_sync_counter_reg0_0),
        .CE(1'b1),
        .D(is_clk_initialized_reg_i_1_n_0),
        .Q(is_clk_initialized_reg),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    start_prev_i_1
       (.I0(clk_start_init_reg_0),
        .I1(start_ext),
        .I2(start_prev),
        .O(start_prev_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk),
        .CE(1'b1),
        .D(start_prev_i_1_n_0),
        .Q(start_prev),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
